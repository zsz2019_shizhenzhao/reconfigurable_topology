#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <math.h>
using namespace std;
int main()
{
	//为了简化程序，不进行交互输入
	//先考虑20个switch
	int D[20][20] = { 0 }, T[20][20] = { 0 };
	double W[20][20][20] = { 0 };
	double sum = 0;
	int i = 0, j = 0, k = 0, m = 0;
	int seed = 0;
	//设置每条线路上的流量，单向
	for (i = 0; i < 20; ++i)
	{
		for (j = 0; j < 20; ++j)
		{
			if (i == j) { D[i][j] = 0; }
			else {
				D[i][j] = rand() % 10000;
			}
		}
	}
	/*设置W的数值，第一个下标表示输出，
	第二个下标表示途径的节点，第三个下标表示输入节点，
	若第二第三下标一致则没有发生跳转*/
	for (i = 0; i < 20; ++i)
	{
		for (j = 0; j < 20; ++j)
		{
			sum = 0;
			for (k = 0; k < 20; ++k)
			{
				if (i == j) { W[i][j][k] = 0; }
				else {
					W[i][j][k] = ((rand() % 100) / 100.0) * (1 - sum);
					sum += W[i][j][k];
				}
				if (sum == 1) { break; }
			}
		}
	}
	//解线性方程
	return 0;
}
//这里是来创建高斯消去法的函数
void Gauss_eliminate(double(*Arr)[3], double* b, int N) {
	/* 这里的参数说明：
		1. Arr 是方程组的系数矩阵， 是一个二维方阵
		2. b 是方程组的右端项， 是一个一维数组
		3. N 是矩阵的阶数*/

		// 列主元高斯消去法，需要我们去首先找到列主元
	int max_val_index = 0;
	double effecd = 0.0;
	for (int i = 0; i < N; i++) {
		max_val_index = i;
		for (int row_i = i + 1; row_i < N; row_i++) {
			if (abs(Arr[row_i][i]) > abs(Arr[max_val_index][i])) {
				max_val_index = row_i;
			}
		}
		// 待循环结束，便是找到了主元所在列上的最大值所对应的索引
		// 将max_val_index所指示的行交换到主行上去
		double temp = 0.0;
		for (int d = 0; d < N; d++) {
			temp = Arr[max_val_index][d];
			Arr[max_val_index][d] = Arr[i][d];
			Arr[i][d] = temp;
		}
		// 交换结束后，需要对b中对应位置上的元素进行交换
		temp = b[max_val_index];
		b[max_val_index] = b[i];
		b[i] = temp;
		// 循环结束，交换也就结束了，
		for (int row_i = i + 1; row_i < N; row_i++) {
			// 列是从i列开始的.
			effecd = Arr[row_i][i] / Arr[i][i];
			for (int col_i = i; col_i < N; col_i++) {
				Arr[row_i][col_i] -= Arr[i][col_i] * effecd;
			}
			// 内部循环结束后，当前的行上的所有的元素便处理完成了，此时要处理b上的对应位置的元素
			b[row_i] -= b[i] * effecd;
		}
	}
}