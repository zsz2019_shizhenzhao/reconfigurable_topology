from ortools.linear_solver import pywraplp
import numpy as np
from utils.base import DcnBase
import math
import gurobipy as grb
from root_const import ROOT_PATH

class LinearProgrammingRouting(DcnBase):
    """
    Desc: 流量工程做routing，顺便计算link utilization
    """
 
    _w_routing = {}
    
    
    def __init__(self, topology_pods):
        """
        Desc: 做routing前，除了需要加载基类的配置读取，配置其他参数
              还需要添加前面的拓扑工程计算出来的pods间连接数
        Inputs:
            - topology_pods(2-dimension list): Number of s_i egress links connected to igress links of s_j.
        """
        self._topology_pods = topology_pods

    
    def traffic_engineering_grb(self, actual_traffic):
        """用gurobi来解
        """
        if not isinstance(actual_traffic, np.ndarray):
            traffic = np.array(actual_traffic)
        else:
            traffic = actual_traffic
    
        if not isinstance(self._bandwidth, np.ndarray):
            bandwidth = np.array(self._bandwidth)
        else:
            bandwidth = self._bandwidth

        pods_num = self._pods_num
        capacity = self._topology_pods * bandwidth

        m = grb.Model('traffic_engineering_grb')
        m.Params.LogToConsole = 0
        mlu = m.addVar(lb = 0, vtype = grb.GRB.CONTINUOUS, name='mlu')

        name_w_ikj = [
            f'w_{i}_{k}_{j}'
            for i in range(1, pods_num + 1)
            for k in range(pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != k and i != j and j != k
        ]
        w = m.addVars(name_w_ikj, lb = 0, ub = 1, vtype = grb.GRB.CONTINUOUS, name='w')

        # 没有链路的时候不能用流量
        m.addConstrs(
                grb.quicksum(
                    w[f'w_{k}_{i}_{j}'] + w[f'w_{i}_{j}_{k}']
                    if k != 0 else w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) == 0
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j and capacity[i - 1][j - 1] == 0 
            )
        
        # summation(w_ikj) = 1
        m.addConstrs(
            (grb.quicksum(
                w[f'w_{i}_{k}_{j}'] for k in range(0, pods_num + 1)
                if k != i and k != j
            ) == 1
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j),
            name = 'sumOneConstrs'
        )
        # summation(T_ij*w_) <= u * capacity
        if len(traffic.shape) == 2:
            # 单个流量矩阵下的约束
            m.addConstrs(
                grb.quicksum(
                    traffic[k - 1][j - 1] * w[f'w_{k}_{i}_{j}']
                    + traffic[i - 1][k - 1] * w[f'w_{i}_{j}_{k}']
                    if k != 0 else traffic[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) <= mlu * capacity[i - 1][j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j
            )
        elif len(traffic.shape) == 3:
            # 用多个流量矩阵来约束
            for t in traffic:
                m.addConstrs(
                    grb.quicksum(
                        t[k - 1][j - 1] * w[f'w_{k}_{i}_{j}']
                        + t[i - 1][k - 1] * w[f'w_{i}_{j}_{k}']
                        if k != 0 else t[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                        for k in range(0, pods_num + 1)
                        if k != i and k != j
                    ) <= mlu * capacity[i - 1][j - 1]
                    for i in range(1, pods_num + 1)
                    for j in range(1, pods_num + 1)
                    if i != j
                )

        m.setObjective(mlu, grb.GRB.MINIMIZE)
        # m.write('debug.lp')
        m.optimize()
        if m.status == grb.GRB.Status.OPTIMAL:
            # print(m.objVal)
            solution = m.getAttr('X', w)
            w_routing = {}
            for w_name in name_w_ikj:
                w_routing[w_name] = solution[w_name]
            self._w_routing = w_routing
            return w_routing, m.objVal
        else:
            print('No solution')


    def again_routing(self, actual_traffic, max_link_utilization, scale_down_factor = 1, start_index = 0):
        """利用上一步解出来的u来重新优化routing结果
        """
        if not isinstance(actual_traffic, np.ndarray):
            traffic = np.array(actual_traffic)
        else:
            traffic = actual_traffic
    
        if not isinstance(self._bandwidth, np.ndarray):
            bandwidth = np.array(self._bandwidth)
        else:
            bandwidth = self._bandwidth

        pods_num = self._pods_num
        capacity = self._topology_pods * bandwidth
        # capacity = capacity.tolist()

        m = grb.Model('again_routing')
        m.Params.LogToConsole = 0

        name_w_ikj = [
            f'w_{i}_{k}_{j}'
            for i in range(1, pods_num + 1)
            for k in range(pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != k and i != j and j != k
        ]
        w = m.addVars(name_w_ikj, lb = 0, ub = 1, vtype = grb.GRB.CONTINUOUS)


        # 没有链路的时候不能用流量
        m.addConstrs(
                grb.quicksum(
                    w[f'w_{k}_{i}_{j}'] + w[f'w_{i}_{j}_{k}']
                    if k != 0 else w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) == 0
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j and capacity[i - 1][j - 1] == 0 
            )

            
        # summation(w_ikj) = 1
        m.addConstrs(
            grb.quicksum(
                w[f'w_{i}_{k}_{j}'] for k in range(0, pods_num + 1)
                if k != i and k != j
            ) == 1
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j
        )
        # summation(T_ij*w_) <= u * capacity
        if len(traffic.shape) == 2:
            # 单个流量矩阵下的约束
            m.addConstrs(
                grb.quicksum(
                    traffic[k - 1][j - 1] * w[f'w_{k}_{i}_{j}']
                    + traffic[i - 1][k - 1] * w[f'w_{i}_{j}_{k}']
                    if k != 0 else traffic[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) <= max_link_utilization * capacity[i - 1][j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j
            )

        elif len(traffic.shape) == 3:
            # 用多个流量矩阵来约束
            for t in traffic:
                m.addConstrs(
                    grb.quicksum(
                        t[k - 1][j - 1] * w[f'w_{k}_{i}_{j}']
                        + t[i - 1][k - 1] * w[f'w_{i}_{j}_{k}']
                        if k != 0 else t[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                        for k in range(0, pods_num + 1)
                        if k != i and k != j
                    ) <= max_link_utilization * capacity[i - 1][j - 1]
                    for i in range(1, pods_num + 1)
                    for j in range(1, pods_num + 1)
                    if i != j
                )

        import caopeirui.LFToE_simulation.preprocess as pp
        traffic_seq = pp.get_ori_traffic_seq(pp.TRAFFIC_FILE)
        before_var = pp.get_place_var(traffic_seq[start_index : start_index + pp.WINDOW_SIZE])
        before_std = np.sqrt(before_var)
        traffic_std = before_std.reshape(self._pods_num, self._pods_num)
             
        sen = m.addVars(pods_num, pods_num, vtype = grb.GRB.CONTINUOUS, name = 'sen')


        m.addConstrs(
            grb.quicksum(
                traffic_std[k - 1][j - 1] * w[f'w_{k}_{i}_{j}']
                + traffic_std[i - 1][k - 1] * w[f'w_{i}_{j}_{k}']
                if k != 0 else traffic_std[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                for k in range(0, pods_num + 1)
                if k != i and k != j
            ) <= sen[i - 1, j - 1] * capacity[i - 1][j - 1]
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j
        )

        m.setObjective(grb.quicksum(
            sen[i - 1, j - 1] * sen[i - 1, j - 1]
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j
        ), grb.GRB.MINIMIZE)

        m.optimize()
        if m.status == grb.GRB.Status.OPTIMAL:
            # print('agaim routing, min_direct_hop_traffic:', m.objVal)
            # self._min_direct_hop_traffic = m.objVal
            solution = m.getAttr('X', w)
            w_routing = {}  
            for w_name in name_w_ikj:
                w_routing[w_name] = solution[w_name]
            self._w_routing = w_routing
            return w_routing
        else:
            print('No solution')

    def routing(self, actual_traffic, start_index = 0, scale_down_factor = 1):
        _, mlu = self.traffic_engineering_grb(actual_traffic)
        self.again_routing(actual_traffic, mlu, scale_down_factor, start_index)


if __name__ == "__main__":
    test = LinearProgrammingRouting([1])
    conf_file = f'{ROOT_PATH}/config/pods_config.csv'
    test.load_init_config(conf_file)
    traffic_TE = np.load(f"{ROOT_PATH}/data/data_properties_100/out_pod{8}.npy")

    test.traffic_engineering_grb(traffic_TE)
    lu, mlu, alu  = test.calc_link_utilization(traffic_TE)
    # print(lu, mlu, alu)
    test.again_routing(traffic_TE, mlu)
    lu, mlu, alu  = test.calc_link_utilization(traffic_TE)

    print(lu, mlu, alu)
