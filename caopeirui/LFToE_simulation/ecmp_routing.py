from ortools.linear_solver import pywraplp
import numpy as np
from utils.base import DcnBase
import math
import gurobipy as grb
from root_const import ROOT_PATH

class EcmpRouting(DcnBase):
    """
    Desc: 流量工程做routing，顺便计算link utilization
    """
 
    _w_routing = {}
    
    
    def __init__(self, topology_pods):
        """
        Desc: 做routing前，除了需要加载基类的配置读取，配置其他参数
              还需要添加前面的拓扑工程计算出来的pods间连接数
        Inputs:
            - topology_pods(2-dimension list): Number of s_i egress links connected to igress links of s_j.
        """
        self._topology_pods = topology_pods


    def traffic_engineering(self, actual_traffic):
        """
        Desc: Using the linear programming to obtain the optimal routing(w_i_k_j)

        Inputs:
            - actual_traffic(2-dimension list): Actual traffic in the datacenter.
        Returns: 
            - w: The result of routing.
        """
        if not isinstance(actual_traffic, np.ndarray):
            traffic = np.array(actual_traffic)
        else:
            traffic = actual_traffic
    
        if not isinstance(self._bandwidth, np.ndarray):
            bandwidth = np.array(self._bandwidth)
        else:
            bandwidth = self._bandwidth

        pods_num = self._pods_num
        capacity = self._topology_pods * bandwidth
        capacity = capacity.tolist()

        solver = pywraplp.Solver('EcmpRouting',
                                pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)
        max_link_utilization = solver.NumVar(0, solver.infinity(), 'max_link_utilization')

        # w_ikj denotes the actual traffic routed from pod s_i to pod s_j though s_k.
        # If k=0, w_i0j denotes the traffic routed from pod s_i to pod s_j directly.
        name_w_ikj = [
            f'w_{i}_{k}_{j}'
            for i in range(1, pods_num + 1)
            for k in range(pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != k and i != j and j != k
        ]
        variables_w_ikj = {}
        for w in name_w_ikj:
            variables_w_ikj[w] = solver.NumVar(0, 1, w)
            # variables_w_ikj[w] = solver.NumVar(0, solver.infinity(), w)

        for i in range(1, pods_num + 1):
            for j in range(1, pods_num + 1):
                if i != j:
                    # summation(w_ikj) = 1
                    constraint_w_ikj = solver.Constraint(1, 1)
                    # summation(T_ij*w_) <= u * capacity
                    # => 0 <= u * capacity - summation(T_ij*w_)
                    constraint_w_capacity = solver.Constraint(0, solver.infinity())
                    constraint_w_capacity.SetCoefficient(max_link_utilization, capacity[i - 1][j - 1])
                    for k in range(0, pods_num + 1):
                        if k != i and k != j:
                            constraint_w_ikj.SetCoefficient(variables_w_ikj[f'w_{i}_{k}_{j}'], 1)
                            if k != 0:
                                # In the physical sense, the routing traffic ratio w_ should multiply the actual traffic from source to destination
                                constraint_w_capacity.SetCoefficient(variables_w_ikj[f'w_{k}_{i}_{j}'], -traffic[k - 1][j - 1])
                                constraint_w_capacity.SetCoefficient(variables_w_ikj[f'w_{i}_{j}_{k}'], -traffic[i - 1][k - 1])
                            else:
                                constraint_w_capacity.SetCoefficient(variables_w_ikj[f'w_{i}_{0}_{j}'], -traffic[i - 1][j - 1])

        objective = solver.Objective()
        objective.SetCoefficient(max_link_utilization, 1)
        objective.SetMinimization()
        # Call the solver and display the results.
        solver.Solve()
        # print('LP->max_link_utilization = ', max_link_utilization.solution_value())

        # The result of routing w_***
        """ 不用数组写法
        w_ikj = [[0] * self._pods_num for _ in range(self._pods_num)]

        for i in range(1, self._pods_num + 1):
            for j in range(1, self._pods_num + 1):
                if i != j:
                    w_ikj[i - 1][j - 1] = []
                    for k in range(self._pods_num + 1):
                        if k != i and k != j:
                            # print(f'w_{i}_{k}_{j}', end=' ')
                            w_ikj[i - 1][j - 1].append(variables_w_ikj[f'w_{i}_{k}_{j}'].solution_value())
                        else:
                            # 不存在路由的位置，需要占位，方便后面计算取值
                            w_ikj[i - 1][j - 1].append(None)
                    # print(f'↑{i}_{j}')
        """
        # 为了可读性，不用array或者list存储routing信息
        # 可利用字典直接读取routing值如: w_routing['w_1_3_2'] 即为pod1经过pod3传到pod2的routing值
        w_routing = {}    
        for w_name in name_w_ikj:
            w_routing[w_name] = variables_w_ikj[w_name].solution_value()

        self._w_routing = w_routing
        return w_routing

    
    def traffic_engineering_grb(self, actual_traffic):
        """用gurobi来解
        """
        if not isinstance(actual_traffic, np.ndarray):
            traffic = np.array(actual_traffic)
        else:
            traffic = actual_traffic
    
        if not isinstance(self._bandwidth, np.ndarray):
            bandwidth = np.array(self._bandwidth)
        else:
            bandwidth = self._bandwidth

        pods_num = self._pods_num
        capacity = self._topology_pods * bandwidth

        m = grb.Model('traffic_engineering_grb')
        m.Params.LogToConsole = 0
        mlu = m.addVar(lb = 0, vtype = grb.GRB.CONTINUOUS, name='mlu')

        name_w_ikj = [
            f'w_{i}_{k}_{j}'
            for i in range(1, pods_num + 1)
            for k in range(pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != k and i != j and j != k
        ]
        w = m.addVars(name_w_ikj, lb = 0, ub = 1, vtype = grb.GRB.CONTINUOUS, name='w')

        # summation(w_ikj) = 1
        m.addConstrs(
            (grb.quicksum(
                w[f'w_{i}_{k}_{j}'] for k in range(0, pods_num + 1)
                if k != i and k != j
            ) == 1
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j),
            name = 'sumOneConstrs'
        )
        # summation(T_ij*w_) <= u * capacity
        m.addConstrs(
            grb.quicksum(
                traffic[k - 1][j - 1] * w[f'w_{k}_{i}_{j}']
                + traffic[i - 1][k - 1] * w[f'w_{i}_{j}_{k}']
                if k != 0 else traffic[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                for k in range(0, pods_num + 1)
                if k != i and k != j
            ) <= mlu * capacity[i - 1][j - 1]
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j
        )
        
        m.setObjective(mlu, grb.GRB.MINIMIZE)
        # m.write('debug.lp')
        m.optimize()
        if m.status == grb.GRB.Status.OPTIMAL:
            # print(m.objVal)
            solution = m.getAttr('X', w)
            w_routing = {}  
            for w_name in name_w_ikj:
                w_routing[w_name] = solution[w_name]
            self._w_routing = w_routing
            return w_routing
        else:
            print('No solution')

    def again_routing(self, actual_traffic, max_link_utilization):
        """利用上一步解出来的u来重新优化routing结果
        """
        if not isinstance(actual_traffic, np.ndarray):
            traffic = np.array(actual_traffic)
        else:
            traffic = actual_traffic
    
        if not isinstance(self._bandwidth, np.ndarray):
            bandwidth = np.array(self._bandwidth)
        else:
            bandwidth = self._bandwidth

        pods_num = self._pods_num
        capacity = self._topology_pods * bandwidth
        # capacity = capacity.tolist()

        m = grb.Model('again_routing')
        m.Params.LogToConsole = 0

        name_w_ikj = [
            f'w_{i}_{k}_{j}'
            for i in range(1, pods_num + 1)
            for k in range(pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != k and i != j and j != k
        ]
        w = m.addVars(name_w_ikj, lb = 0, ub = 1, vtype = grb.GRB.CONTINUOUS)

        # summation(w_ikj) = 1
        m.addConstrs(
            grb.quicksum(
                w[f'w_{i}_{k}_{j}'] for k in range(0, pods_num + 1)
                if k != i and k != j
            ) == 1
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j
        )
        # summation(T_ij*w_) <= u * capacity
        m.addConstrs(
            grb.quicksum(
                traffic[k - 1][j - 1] * w[f'w_{k}_{i}_{j}']
                + traffic[i - 1][k - 1] * w[f'w_{i}_{j}_{k}']
                if k != 0 else traffic[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                for k in range(0, pods_num + 1)
                if k != i and k != j
            ) <= max_link_utilization * capacity[i - 1][j - 1]
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j
        )
        
        max_w = m.addVar(lb = 0, ub = 1, vtype = grb.GRB.CONTINUOUS)
        # 此处如果不加capacity可以看成是ECMP
        m.addConstrs(w[w_ikj] <= max_w for w_ikj in name_w_ikj)
        
        m.setObjective(max_w, grb.GRB.MINIMIZE)

        m.optimize()
        if m.status == grb.GRB.Status.OPTIMAL:
            print('agaim routing, max w:', m.objVal)
            solution = m.getAttr('X', w)
            w_routing = {}  
            for w_name in name_w_ikj:
                w_routing[w_name] = solution[w_name]
            self._w_routing = w_routing
            return w_routing
        else:
            print('No solution')


if __name__ == "__main__":
    test = EcmpRouting([1])
    conf_file = f'{ROOT_PATH}/config/pods_config.csv'
    test.load_init_config(conf_file)
    traffic_TE = np.load(f"{ROOT_PATH}/data/data_properties_100/out_pod{8}.npy")

    test.traffic_engineering_grb(traffic_TE)
    lu, mlu, alu  = test.calc_link_utilization(traffic_TE)
    # print(lu, mlu, alu)
    test.again_routing(traffic_TE, mlu)
    lu, mlu, alu  = test.calc_link_utilization(traffic_TE)

    print(lu, mlu, alu)