from ortools.linear_solver import pywraplp
from utils.base import DcnBase
import numpy as np
import gurobipy as grb
import math
import caopeirui.LFToE_simulation.preprocess as pp


class FindFractionalTopology(DcnBase):
    def get_fractional_topology(self):
        """
        Desc: According to the number of pods and pods' ingress/egress, and bandwidth,
              find the topology of all pods' connection by LP.
        
        Returns: (u.solution_value(), d_ij)
            - u.solution_value(): 1/(max link utilization)
            - d_ij: Denote the number of s_i egress links connected to ingress links of s_j.
        """
        if self._traffic_count == 0:
            raise ValueError('''
                No traffic input has been set yet, 
                please call add_traffic() firstly.
            ''')
        solver = pywraplp.Solver('FindFractionalTopology',
                                pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)

        u = solver.NumVar(0, solver.infinity(), 'u')

        # d_ij denotes the number of links which s_i connected to s_j
        name_d_ij = [
            f'd_{i}_{j}'
            for i in range(1, self._pods_num + 1)
            for j in range(1, self._pods_num + 1)
            if i != j
        ]
        variables_d_ij = {}
        for d in name_d_ij:
            variables_d_ij[d] = solver.NumVar(0, solver.infinity(), d)

        # Add all constraints for every input traffic matrix
        for multiple in range(0, self._traffic_count):
            a_traffic = self._traffic_sequence[multiple]
            # w_ikj denotes the actual traffic routed from pod s_i to pod s_j though s_k.
            # If k=0, w_i0j denotes the traffic routed from pod s_i to pod s_j directly.
            name_w_ikj = [
                f'{multiple}_w_{i}_{k}_{j}'
                for i in range(1, self._pods_num + 1)
                for k in range(self._pods_num + 1)
                for j in range(1, self._pods_num + 1)
                if i != k and i != j and j != k
            ]
            variables_w_ikj = {}
            for w in name_w_ikj:
                variables_w_ikj[w] = solver.NumVar(0, solver.infinity(), w)

            for i in range(1, self._pods_num + 1):
                # Corresponding to the paper formula(1)
                # summation(d_ij) <= r_i_(e/in)gress
                if multiple == 0:
                    constraint_dr_egress = solver.Constraint(-solver.infinity(), int(self._r_egress[i - 1]))
                    constraint_dr_ingress = solver.Constraint(-solver.infinity(), int(self._r_ingress[i - 1]))

                for j in range(1, self._pods_num + 1):
                    if i != j:
                        # Corresponding to the paper formula(1)
                        if multiple == 0:
                            constraint_dr_egress.SetCoefficient(variables_d_ij[f'd_{i}_{j}'], 1)
                            constraint_dr_ingress.SetCoefficient(variables_d_ij[f'd_{j}_{i}'], 1)

                        # Corresponding to the paper formula(2)->1)
                        # summation(w_p_ij) = u * t_ij  =>  summation(w_p) - u * t_ij
                        # constraint =  solver.Constraint(0, 0) 这样某些情况会无解
                        constraint =  solver.Constraint(0, solver.infinity())
                        constraint.SetCoefficient(u, -a_traffic[i - 1][j - 1])

                        # Corresponding to the paper formula(2)->2)
                        # summation(w_p_sisj) ≤ d_ij * b_ij  =>  d_ij * b_ij - summation(w_p) ≥ 0
                        constraint_db = solver.Constraint(0, solver.infinity())
                        constraint_db.SetCoefficient(variables_d_ij[f'd_{i}_{j}'], int(self._bandwidth[i - 1][j - 1]))   
                        for k in range(0, self._pods_num + 1):
                            if k != i and k != j:
                                # Corresponding to the paper formula(2)->1) 
                                constraint.SetCoefficient(variables_w_ikj[f'{multiple}_w_{i}_{k}_{j}'], 1)

                                # Corresponding to the paper formula(2)->2)
                                if k != 0:
                                    constraint_db.SetCoefficient(variables_w_ikj[f'{multiple}_w_{k}_{i}_{j}'], -1)
                                    constraint_db.SetCoefficient(variables_w_ikj[f'{multiple}_w_{i}_{j}_{k}'], -1)
                                else:
                                    constraint_db.SetCoefficient(variables_w_ikj[f'{multiple}_w_{i}_{0}_{j}'], -1)


        # Create the objective function, maximize u.
        objective = solver.Objective()
        objective.SetCoefficient(u, 1)
        objective.SetMaximization()
        # Call the solver and display the results.
        solver.Solve()
        # print('Solution:')
        # print('u = ', u.solution_value())

        d_ij = np.zeros((self._pods_num, self._pods_num), dtype=np.float)
        for i in range(1, self._pods_num + 1):
            for j in range(1, self._pods_num + 1):
                if i != j:
                    d_ij[i - 1][j - 1] = variables_d_ij[f'd_{i}_{j}'].solution_value()

        # 这里解出来的u，可视为 max link utilization 的 倒数
        self._d_ij = d_ij
        return u.solution_value(), d_ij


    def third_ToE(self, max_link_utilization, scale_up_factor = 1.0, start_index = 0):
        """利用上次的max link utilization重新解d_ij
        objective: maxmize(\min_k summation(t^(k)_ij w_ij))
        """
        pods_num = self._pods_num
        bandwidth = self._bandwidth

        m = grb.Model('again_ToE')
        m.Params.LogToConsole = 0
        
        name_w_ikj = [
            f'w_{i}_{k}_{j}'
            for i in range(1, pods_num + 1)
            for k in range(pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != k and i != j and j != k
        ]
        # 此处w变量视为分配routing比例，充当约束，但解ToE时无需知道结果
        w = m.addVars(name_w_ikj, lb = 0, ub = 1, vtype = grb.GRB.CONTINUOUS, name = 'w')

        # summation(w_ikj) = 1
        m.addConstrs(
            grb.quicksum(
                w[f'w_{i}_{k}_{j}'] for k in range(0, pods_num + 1)
                if k != i and k != j
            ) == 1
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j 
        )

        topology_pods = m.addVars(pods_num, pods_num, vtype = grb.GRB.CONTINUOUS, name = 'x')
        # topology_pods = m.addVars(pods_num, pods_num, vtype = grb.GRB.INTEGER, name = 'x')
        # summation(d_ij) <= r_i_(e/in)gress
        m.addConstrs(
            grb.quicksum(
                topology_pods[i, j] for j in range(pods_num)
            ) <= self._r_egress[i]
            for i in range(pods_num)
        )
        m.addConstrs(
            grb.quicksum(
                topology_pods[j, i] for j in range(pods_num)
            ) <= self._r_ingress[i]
            for i in range(pods_num)
        )

        #min_direct_hop_traffic = m.addVar(lb = 0, vtype = grb.GRB.CONTINUOUS, name='min_direct_hop_traffic')
        for traffic in self._traffic_sequence:
            # summation(T_ij*w_) <= u * capacity
            # w下标直接对应pods编号，bandwidth和topology_pods下标只有二维所以从0开始，对应w的下标减1
            m.addConstrs(
                grb.quicksum(
                    traffic[k - 1][j - 1] * w[f'w_{k}_{i}_{j}']
                    + traffic[i - 1][k - 1] * w[f'w_{i}_{j}_{k}']
                    if k != 0 else traffic[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) <= max_link_utilization * bandwidth[i - 1][j - 1] * topology_pods[i - 1, j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j
            )
            """m.addConstr(
                grb.quicksum(
                    traffic[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                    for i in range(1, pods_num + 1)
                    for j in range(1, pods_num + 1)
                    if i != j
                ) >= min_direct_hop_traffic
            )"""

        m.addConstrs(
            topology_pods[i, j] == 0
            for i in range(pods_num)
            for j in range(pods_num)
            if i == j
        )

        traffic_seq = pp.get_ori_traffic_seq(pp.TRAFFIC_FILE)
        before_var = pp.get_place_var(traffic_seq[start_index : start_index + pp.WINDOW_SIZE])
        before_std = np.sqrt(before_var)
        max_std = max(before_std)
        norm_before_std = np.array([std / max_std for std in before_std])
        traffic_std = norm_before_std.reshape(self._pods_num, self._pods_num)

        # 对抗uncertainty
        y = self._y * scale_up_factor
        m.addConstrs(
            traffic_std[i - 1][j - 1] * w[f'w_{i}_{0}_{j}'] <= y * bandwidth[i - 1][j - 1] * topology_pods[i - 1, j - 1]
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j
        )
        m.addConstrs(
            traffic_std[i - 1][j - 1] * w[f'w_{i}_{k}_{j}'] <= y * bandwidth[k - 1][j - 1] * topology_pods[k - 1, j - 1]
            for k in range(1, pods_num + 1)
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j and i != k and k != j
        )
        m.addConstrs(
            traffic_std[i - 1][j - 1] * w[f'w_{i}_{k}_{j}'] <= y * bandwidth[i - 1][k - 1] * topology_pods[i - 1, k - 1]
            for k in range(1, pods_num + 1)
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j and i != k and k != j
        )

        # 此约束效果明显，让两跳routing尽量少，单跳links数随之增加
        m.setObjective(grb.quicksum(
            w[f'w_{i}_{k}_{j}'] * w[f'w_{i}_{k}_{j}']
            for k in range(1, pods_num + 1)
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j and i != k and j != k
        ), grb.GRB.MINIMIZE)

        m.optimize()
        # m.write('againToE.lp')
        if m.status == grb.GRB.Status.OPTIMAL:
            solution = m.getAttr('X', topology_pods)

            # 提供给第三次ToE
            # self._min_direct_hop_traffic = m.objVal
            # self._w_routing = m.getAttr('X', w)

            x_ij = np.zeros((self._pods_num, self._pods_num), dtype=np.float)
            for i in range(pods_num):
                for j in range(pods_num):
                    x_ij[i][j] = solution[i, j]
            return x_ij
        else:
            print('No solution')
            exit(0)


    def again_ToE(self, max_link_utilization, start_index):
        """利用上次的max link utilization重新解d_ij
           二分迭代求解，直到 y 不能再减小
        """
        upper_bound = 1 / self._bandwidth.min()
        lower_bound = 0
        gap = upper_bound - lower_bound
        res = None
        y = upper_bound
        while upper_bound - lower_bound > gap * 0.01:
            # y = 1
            state, tmp_res = self._again_LP(max_link_utilization, y, start_index)
            self._d_ij = tmp_res
            if state == 'No solution':
                lower_bound = y
                print('lower_bound', y)
            else:
                upper_bound = y
                self._y = y
                res = tmp_res
                print('upper_bound', y)
            y = (upper_bound + lower_bound) / 2
        return res


    def _again_LP(self, max_link_utilization, y, start_index):
        """利用上次的max link utilization重新解d_ij
        objective: maxmize(summation(topology_pods[i, j]))
        """
        pods_num = self._pods_num
        bandwidth = self._bandwidth

        m = grb.Model('again_ToE')
        m.Params.LogToConsole = 0
        
        name_w_ikj = [
            f'w_{i}_{k}_{j}'
            for i in range(1, pods_num + 1)
            for k in range(pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != k and i != j and j != k
        ]
        # 此处w变量视为分配routing比例，充当约束，但解ToE时无需知道结果
        w = m.addVars(name_w_ikj, lb = 0, ub = 1, vtype = grb.GRB.CONTINUOUS, name = 'w')

        # summation(w_ikj) = 1
        m.addConstrs(
            grb.quicksum(
                w[f'w_{i}_{k}_{j}'] for k in range(0, pods_num + 1)
                if k != i and k != j
            ) == 1
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j 
        )

        topology_pods = m.addVars(pods_num, pods_num, vtype = grb.GRB.CONTINUOUS, name = 'x')
        # topology_pods = m.addVars(pods_num, pods_num, vtype = grb.GRB.INTEGER, name = 'x')
        # summation(d_ij) <= r_i_(e/in)gress
        m.addConstrs(
            grb.quicksum(
                topology_pods[i, j] for j in range(pods_num)
            ) <= self._r_egress[i]
            for i in range(pods_num)
        )
        m.addConstrs(
            grb.quicksum(
                topology_pods[j, i] for j in range(pods_num)
            ) <= self._r_ingress[i]
            for i in range(pods_num)
        )

        for traffic in self._traffic_sequence:
            # summation(T_ij*w_) <= u * capacity
            # w下标直接对应pods编号，bandwidth和topology_pods下标只有二维所以从0开始，对应w的下标减1
            m.addConstrs(
                grb.quicksum(
                    traffic[k - 1][j - 1] * w[f'w_{k}_{i}_{j}']
                    + traffic[i - 1][k - 1] * w[f'w_{i}_{j}_{k}']
                    if k != 0 else traffic[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) <= max_link_utilization * bandwidth[i - 1][j - 1] * topology_pods[i - 1, j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j
            )
        # m.addConstrs(
        #     w[f'w_{i}_{k}_{j}'] <= y * bandwidth[i - 1][j - 1] * topology_pods[i - 1, j - 1]
        #     for k in range(0, pods_num + 1)
        #     for i in range(1, pods_num + 1)
        #     for j in range(1, pods_num + 1)
        #     if i != j and i != k and k != j
        # )
        
        traffic_seq = pp.get_ori_traffic_seq(pp.TRAFFIC_FILE)
        before_var = pp.get_place_var(traffic_seq[start_index : start_index + pp.WINDOW_SIZE])
        before_std = np.sqrt(before_var)
        max_std = max(before_std)
        norm_before_std = np.array([std / max_std for std in before_std])
        traffic_std = norm_before_std.reshape(self._pods_num, self._pods_num)

        # 对抗uncertainty
        m.addConstrs(
            traffic_std[i - 1][j - 1] * w[f'w_{i}_{0}_{j}'] <= y * bandwidth[i - 1][j - 1] * topology_pods[i - 1, j - 1]
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j
        )
        m.addConstrs(
            traffic_std[i - 1][j - 1] * w[f'w_{i}_{k}_{j}'] <= y * bandwidth[k - 1][j - 1] * topology_pods[k - 1, j - 1]
            for k in range(1, pods_num + 1)
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j and i != k and k != j
        )
        m.addConstrs(
            traffic_std[i - 1][j - 1] * w[f'w_{i}_{k}_{j}'] <= y * bandwidth[i - 1][k - 1] * topology_pods[i - 1, k - 1]
            for k in range(1, pods_num + 1)
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j and i != k and k != j
        )
        
        # 对角线置0，其他位置不能为0，会让求解变慢
        # m.addConstrs(
        #     topology_pods[i, j] == 0
        #     if i == j else topology_pods[i, j] >= 1
        #     for i in range(pods_num)
        #     for j in range(pods_num)
        # )
        m.addConstrs(
            topology_pods[i, j] == 0
            for i in range(pods_num)
            for j in range(pods_num)
            if i == j
        )
        # 让链接数尽可能多
        #m.setObjective(grb.quicksum(
         #   topology_pods[i, j]
         #   for i in range(pods_num)
         #   for j in range(pods_num)
         #   if i != j
        #), grb.GRB.MAXIMIZE)

        # 此约束效果明显，让两跳routing尽量少，单跳links数随之增加
        #m.setObjective(grb.quicksum(
         #   w[f'w_{i}_{k}_{j}'] * w[f'w_{i}_{k}_{j}']
         #   for k in range(1, pods_num + 1)
         #   for i in range(1, pods_num + 1)
         #   for j in range(1, pods_num + 1)
         #   if i != j and i != k and j != k
        #), grb.GRB.MINIMIZE)

        m.optimize()
        # m.write('againToE.lp')
        if m.status == grb.GRB.Status.OPTIMAL:
            solution = m.getAttr('X', topology_pods)

            x_ij = np.zeros((self._pods_num, self._pods_num), dtype=np.float)
            for i in range(pods_num):
                for j in range(pods_num):
                    x_ij[i][j] = solution[i, j]
            return 'Have solution', x_ij
        else:
            print('No solution')
            return 'No solution', None


if __name__ == "__main__":
    # ------demo test------
    pods_num = 8
    first_sample_traffic = np.random.rand(pods_num, pods_num) * 100000
    second_sample_traffic = np.random.rand(pods_num, pods_num) * 100000
    third_sample_traffic = np.random.rand(pods_num, pods_num) * 100000

    obj = FindFractionalTopology()
    obj.load_init_config('../../config/pods_config.csv')
    
    obj.add_a_traffic(first_sample_traffic)
    obj.add_a_traffic(second_sample_traffic)
    obj.add_a_traffic(third_sample_traffic)
    u, d_ij = obj.get_fractional_topology()
    d_ij = obj.to_integer_topo()
    print(u)
    print(d_ij)
    x_ij = obj.again_ToE(1 / u)
    print(x_ij)
    print(np.array(x_ij).sum())

