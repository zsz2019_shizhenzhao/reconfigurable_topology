from root_const import ROOT_PATH
import numpy as np
import utils.paint as pt
from utils.base import cosine_similarity
from utils.base import data_norm
import random
import math
import caopeirui.LFToE_simulation.preprocess as pp



if __name__ == "__main__":

    traffic_seq = pp.get_ori_traffic_seq(pp.TRAFFIC_FILE)
    one_week_traffic = traffic_seq[0 : 0 + 4032]
    future_traffic = traffic_seq[0 + 4032 : 0 + 8064]
    before_var = pp.get_place_var(one_week_traffic)
    after_var = pp.get_place_var(future_traffic)
    # total_var = pp.get_place_var(traffic_seq)

    before_mean = pp.get_place_mean(one_week_traffic)
    after_mean = pp.get_place_mean(future_traffic)

    before_max = pp.get_place_max(one_week_traffic)
    after_max = pp.get_place_max(future_traffic)

    before_min = pp.get_place_min(one_week_traffic)
    after_min = pp.get_place_min(future_traffic)

    plt = pt.draw_line_graph(
        # before_var = before_var,
        # after_var = after_var,
        before_mean = before_mean,
        after_mean = after_mean,
        # before_max = before_max,
        # after_max = after_max,
        # before_min = before_min,
        # after_min = after_min
    )
    # print(cosine_similarity(before_mean.tolist(), after_mean.tolist()))
    plt.show()
    exit()
    

    import caopeirui.LFToE_simulation.find_fractional_topology as fft
    # 得到fractional topology, 即pod间连接数的非整数解
    obj_frac_topo = fft.FindFractionalTopology()
    obj_frac_topo.load_init_config( f'{ROOT_PATH}/config/g_config_8.csv')
    obj_frac_topo._d_ij = norms
    d_ij = obj_frac_topo.to_integer_topo()
    print(d_ij)
    print(d_ij.sum())
    print(cosine_similarity(d_ij.flatten().tolist(), future_traffic[1].flatten().tolist()))



    col_mean = traffic_seq.mean(axis = 0)
    col_max = traffic_seq.max(axis = 0)
    col_min = traffic_seq.min(axis = 0)
    print(col_mean)
    print(col_max)
    print(col_min)
    print(len(col_max))
    print(np.cov(traffic_seq))