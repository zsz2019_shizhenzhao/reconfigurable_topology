"""
两次ToE，第二次用了ρ（scale traffic）做ToE之后，在做普通LP routing能保证mlu和alu的cdf图都好。
如果LE再做一次，mlu会好的更多，但是损失一部分alu。
"""
import numpy as np
from root_const import ROOT_PATH
import caopeirui.LFToE_simulation.find_fractional_topology as fft
import caopeirui.LFToE_simulation.find_traffic_cluster as ftc
# import caopeirui.LFToE_simulation.map_to_ocs as mto
import caopeirui.LFToE_simulation.LP_routing as lr
import caopeirui.LFToE_simulation.LP_routing_w2 as lrw2
import caopeirui.LFToE_simulation.LP_routing_sen as lrsen
import caopeirui.LFToE_simulation.LP_routing_sen_w2 as lrsenw2
import caopeirui.LFToE_simulation.mesh_topology as mt
import ZB.random_topology as zbrt
from ZB.convex_hull import TrafficConvex
import caopeirui.LFToE_simulation.valiant_routing as vr
from utils.base import cosine_similarity
import caopeirui.LFToE_simulation.preprocess as pp
import logging
from utils.log import init_log


init_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/' + __file__)

# 存储不同 representative traffic 对应 routing 的 类对象
# 后面直接调用，避免重复
LR_ROUTING = None
LRAGAIN_ROUTING = None
LRW2_ROUTING = None
LRSEN_ROUTING = None
LRSENW2_ROUTING = None

def low_frequency_ToE(conf_file, rep_traffic):
    """
    Desc: 整合拓扑工程的分步过程
    """
    pred_traffic = rep_traffic
    # 得到fractional topology, 即pod间连接数的非整数解
    obj_frac_topo = fft.FindFractionalTopology()
    obj_frac_topo.load_init_config(conf_file)

    obj_frac_topo.set_traffic_sequence(pred_traffic)
    u, d_ij = obj_frac_topo.get_fractional_topology()
    # print(d_ij)
    print('scaling factor = ', u)
    # print(d_ij.sum())

    d_ij = obj_frac_topo.again_ToE(1.01 / u)
    d_ij = obj_frac_topo.to_integer_topo(d_ij)
    # d_ij = obj_frac_topo.third_ToE(1 / u, d_ij)

    return d_ij


def record_lr_routing(conf_file, x_ij, traffic):
    obj_TE = lr.LinearProgrammingRouting(x_ij)
    obj_TE.load_init_config(conf_file)
    w_routing, max_link_utilization = obj_TE.traffic_engineering_grb(traffic)
    return obj_TE


def record_lragain_routing(conf_file, x_ij, traffic):
    obj_TE = lr.LinearProgrammingRouting(x_ij)
    obj_TE.load_init_config(conf_file)
    w_routing, max_link_utilization = obj_TE.traffic_engineering_grb(traffic)
    obj_TE.again_routing(traffic, max_link_utilization)
    return obj_TE


def record_lrw2_routing(conf_file, x_ij, traffic):
    obj_TE = lrw2.LinearProgrammingRouting(x_ij)
    obj_TE.load_init_config(conf_file)
    w_routing, max_link_utilization = obj_TE.traffic_engineering_grb(traffic)
    obj_TE.again_routing(traffic, max_link_utilization)
    return obj_TE


def record_lrsen_routing(conf_file, x_ij, traffic, start_index):
    obj_TE = lrsen.LinearProgrammingRouting(x_ij)
    obj_TE.load_init_config(conf_file)
    w_routing, max_link_utilization = obj_TE.traffic_engineering_grb(traffic)
    obj_TE.again_routing(traffic, max_link_utilization, start_index)
    return obj_TE


def record_lrsenw2_routing(conf_file, x_ij, traffic, start_index):
    obj_TE = lrsenw2.LinearProgrammingRouting(x_ij)
    obj_TE.load_init_config(conf_file)
    w_routing, max_link_utilization = obj_TE.traffic_engineering_grb(traffic)
    abs_var = obj_TE.again_routing(traffic, max_link_utilization, start_index)
    obj_TE.third_routing(traffic, max_link_utilization, abs_var, start_index)
    return obj_TE


def test_LP_TE(conf_file, x_ij, rep_traffic, future_traffic, i):
    lr_lu, lr_mlu, lr_alu = LR_ROUTING.calc_link_utilization(future_traffic[i + 1])
    lr_ahc = LR_ROUTING.ave_hop_count(future_traffic[i + 1])

    lragain_lu, lragain_mlu, lragain_alu = LRAGAIN_ROUTING.calc_link_utilization(future_traffic[i + 1])
    lragain_ahc = LRAGAIN_ROUTING.ave_hop_count(future_traffic[i + 1])

    lrw2_lu, lrw2_mlu, lrw2_alu = LRW2_ROUTING.calc_link_utilization(future_traffic[i + 1])
    lrw2_ahc = LRW2_ROUTING.ave_hop_count(future_traffic[i + 1])

    lrsen_lu, lrsen_mlu, lrsen_alu = LRSEN_ROUTING.calc_link_utilization(future_traffic[i + 1])
    lrsen_ahc = LRSEN_ROUTING.ave_hop_count(future_traffic[i + 1])

    lrsenw2_lu, lrsenw2_mlu, lrsenw2_alu = LRSENW2_ROUTING.calc_link_utilization(future_traffic[i + 1])
    lrsenw2_ahc = LRSENW2_ROUTING.ave_hop_count(future_traffic[i + 1])

    res_str = (f'{lr_mlu},{lr_alu},{lr_ahc},{lragain_mlu},{lragain_alu},{lragain_ahc},'
        f'{lrw2_mlu},{lrw2_alu},{lrw2_ahc},{lrsen_mlu},{lrsen_alu},{lrsen_ahc},'
        f'{lrsenw2_mlu},{lrsenw2_alu},{lrsenw2_ahc}')
    
    logging.critical(res_str)


def compare_result(start_index):
    # 每个测试程序都利用统一接口获取输入
    # scale = True 引入参数rou，代表性矩阵做scale数值拓展
    rep_traffic, future_traffic = pp.get_traffic_data(start_index, scale = True)

    x_ij = low_frequency_ToE(pp.CONF_FILE, rep_traffic)
    print(x_ij)
    print(x_ij.sum())
    print(x_ij.sum(axis=0))
    print(x_ij.sum(axis=1))

    # obj = mt.MeshTopology()
    # obj.load_init_config(pp.CONF_FILE)
    # mesh_x_ij = obj.get_connects_matrix()
    # print(mesh_x_ij)
    # print(mesh_x_ij.sum())
    # print(mesh_x_ij.sum(axis=0))
    # print(mesh_x_ij.sum(axis=1))

    # 每两周为周期预测、比较完后，刷新全局ROUTING存储变量。
    global LR_ROUTING
    global LRAGAIN_ROUTING
    global LRW2_ROUTING
    global LRSEN_ROUTING
    global LRSENW2_ROUTING
    LR_ROUTING = None
    LRAGAIN_ROUTING = None
    LRW2_ROUTING = None
    LRSEN_ROUTING = None
    LRSENW2_ROUTING = None

    print('compare_result start_index: ' + str(start_index))
    print(LR_ROUTING)

    # 计算各自topo对应不同 representative traffic 的 routing，存储到全局变量
    for i in range(len(rep_traffic)):
        lr_obj = record_lr_routing(pp.CONF_FILE, x_ij, rep_traffic)
        lragain_obj = record_lragain_routing(pp.CONF_FILE, x_ij, rep_traffic)
        lrw2_obj = record_lrw2_routing(pp.CONF_FILE, x_ij, rep_traffic)
        lrsen_obj = record_lrsen_routing(pp.CONF_FILE, x_ij, rep_traffic, start_index)
        lrsenw2_obj = record_lrsenw2_routing(pp.CONF_FILE, x_ij, rep_traffic, start_index)

        LR_ROUTING = lr_obj
        LRAGAIN_ROUTING = lragain_obj
        LRW2_ROUTING = lrw2_obj
        LRSEN_ROUTING = lrsen_obj
        LRSENW2_ROUTING = lrsenw2_obj

    size = len(future_traffic) - 1
    for i in range(size):
        test_LP_TE(pp.CONF_FILE, x_ij, rep_traffic, future_traffic, i)


if __name__ == "__main__":
    print('Desc: 两次ToE, single-traffic TE')
    print(f'读取{pp.CONF_FILE}')
    logging.critical(
        ('lr_mlu,lr_alu,lr_ahc,lragain_mlu,lragain_alu,lragain_ahc,'
        'lrw2_mlu,lrw2_alu,lrw2_ahc,lrsen_mlu,lrsen_alu,lrsen_ahc,'
        'lrsenw2_mlu,lrsenw2_alu,lrsenw2_ahc')
    )
    for i in range(10):
        compare_result(i * 4032)

   