import utils.paint as pt
import pandas as pd
from root_const import ROOT_PATH 
import numpy as np

def draw_mlu_cdf_from_log(file_name):
    pd_data = pd.read_csv(file_name)

    plt = pt.draw_cdf(
        mlu = pd_data['mlu'],
        mesh_mlu = pd_data['mesh_mlu'],
        random_mlu = pd_data['random_mlu'],
    )
    # plt.savefig(file_name + '_mlu.jpg')
    return plt

def draw_alu_cdf_from_log(file_name):
    pd_data = pd.read_csv(file_name)

    plt = pt.draw_cdf(
        alu = pd_data['alu'],
        mesh_alu = pd_data['mesh_alu'],
        random_alu = pd_data['random_alu'],
    )
    # plt.savefig(file_name + '_alu.jpg')
    return plt


def draw_mlualu_cdf_from_log(file_name):
    pd_data = pd.read_csv(file_name)
    plt = pt.draw_cdf(
        lr_mlu = pd_data['lr_mlu'],
        lragain_mlu = pd_data['lragain_mlu'],
        lrw2_mlu = pd_data['lrw2_mlu'],
        lrsen_mlu = pd_data['lrsen_mlu'],
        lrsenw2_mlu = pd_data['lrsenw2_mlu'],

        lr_alu = pd_data['lr_alu'],
        lragain_alu = pd_data['lragain_alu'],
        lrw2_alu = pd_data['lrw2_alu'],
        lrsen_alu = pd_data['lrsen_alu'],
        lrsenw2_alu = pd_data['lrsenw2_alu'],
    )
    plt.show()
    # plt.savefig(file_name + '_mlualuahc.jpg')
    return plt


def draw_line_graph_from_log(file_name):
    pd_data = pd.read_csv(file_name, header = 0)

    plt = pt.draw_line_graph(
        lr_mlu = pd_data['lr_mlu'],
        lragain_mlu = pd_data['lragain_mlu'],
        lrw2_mlu = pd_data['lrw2_mlu'],
        lrsen_mlu = pd_data['lrsen_mlu'],

        lr_alu = pd_data['lr_alu'],
        lragain_alu = pd_data['lragain_alu'],
        lrw2_alu = pd_data['lrw2_alu'],
        lrsen_alu = pd_data['lrsen_alu'],
    )
    plt.show()
    # plt.savefig(file_name + 'linegraph.jpg')
    return plt

if __name__ == "__main__":
    # draw_line_graph_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/2ToE_self_TE_fixed1topo.py.log')
    draw_mlualu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/compareLP.py.log')
    # draw_mlualu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/compare_multiple_LP.py.log')
