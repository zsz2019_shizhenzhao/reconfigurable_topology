import utils.paint as pt
import pandas as pd
from root_const import ROOT_PATH 
import numpy as np

def get_mlumax_alumean(file_name):
    pd_data = pd.read_csv(file_name, header = 0)
    mean_list = []
    max_list = []
    for i in range(1, 11):
        mean_list.append(pd_data['alu'+ str(i)].mean())
        # max_list.append(pd_data['mlu'+ str(i)].mean())
        max_list.append(np.percentile(pd_data['mlu'+ str(i)], 99))
    

    return max_list, mean_list

if __name__ == "__main__":
    # Topo_list = ['Toe', 'Mesh']
    # Topo = Topo_list[1]
    # week_start = 1
    # weeke_end = 10
    # kmeans_num = 3
    # file_name = f'{Topo}_kmeans{kmeans_num}_week{week_start}-{weeke_end}'
    from caopeirui.LFToE_simulation.test.norm_compare_config import *

    max_list, mean_list = get_mlumax_alumean(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/norm_result/{FILE_NAME_ONE}.log')
    max_list2, mean_list2 = get_mlumax_alumean(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/norm_result/{FILE_NAME_TWO}.log')
    # max_list2, mean_list2 = get_mlumax_alumean(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/Toe_compare_norm_LP.py.log')
    print(max_list)
    print(mean_list)
    # print(max_list2)
    # print(mean_list2)
    plt = pt.draw_point_graph(
        ToE_LP_routing = [[max_list[0]], [mean_list[0]]],
        ToE_LP_routing_ahc_oldsen = [[max_list[1]], [mean_list[1]]],
        ToE_LP_routing_ahc_sen = [[max_list[2]], [mean_list[2]]],
        ToE_LP_routing_oldsen_w2 = [[max_list[3]], [mean_list[3]]],
        ToE_LP_routing_sen = [[max_list[4]], [mean_list[4]]],
        ToE_LP_routing_sen_w2 = [[max_list[5]], [mean_list[5]]],
        ToE_LP_routing_varw2 = [[max_list[6]], [mean_list[6]]],
        ToE_LP_routing_w2 = [[max_list[7]], [mean_list[7]]],
        ToE_LP_routing_oldsen_w2_std = [[max_list[8]], [mean_list[8]]],
        ToE_LP_routing_oldsen_w_std = [[max_list[9]], [mean_list[9]]],
        ##
        mesh_LP_routing = [[max_list2[0]], [mean_list2[0]]],
        mesh_LP_routing_ahc_oldsen = [[max_list2[1]], [mean_list2[1]]],
        mesh_LP_routing_ahc_sen = [[max_list2[2]], [mean_list2[2]]],
        mesh_LP_routing_oldsen_w2 = [[max_list2[3]], [mean_list2[3]]],
        mesh_LP_routing_sen = [[max_list2[4]], [mean_list2[4]]],
        mesh_LP_routing_sen_w2 = [[max_list2[5]], [mean_list2[5]]],
        mesh_LP_routing_varw2 = [[max_list2[6]], [mean_list2[6]]],
        mesh_LP_routing_w2 = [[max_list2[7]], [mean_list2[7]]],
        mesh_LP_routing_oldsen_w2_std = [[max_list2[8]], [mean_list2[8]]],
        mesh_LP_routing_oldsen_w_std = [[max_list2[9]], [mean_list2[9]]],
    )
    plt.title(FILE_NAME)
    # plt.savefig(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/norm_result/{file_name}.png')
    plt.show()


    
    ##TODO: oldsen 加上 std 不加, 再试一下var* w2 / capacity2