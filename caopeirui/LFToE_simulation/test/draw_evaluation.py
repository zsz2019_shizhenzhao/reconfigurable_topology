import utils.paint as pt
import pandas as pd
from root_const import ROOT_PATH 
import numpy as np

def draw_mlu_cdf_from_log(file_name):
    pd_data = pd.read_csv(file_name)

    plt = pt.draw_cdf(
        mlu = pd_data['mlu'],
        mesh_mlu = pd_data['mesh_mlu'],
        random_mlu = pd_data['random_mlu'],
    )
    # plt.savefig(file_name + '_mlu.jpg')
    return plt

def draw_alu_cdf_from_log(file_name):
    pd_data = pd.read_csv(file_name)

    plt = pt.draw_cdf(
        alu = pd_data['alu'],
        mesh_alu = pd_data['mesh_alu'],
        random_alu = pd_data['random_alu'],
    )
    # plt.savefig(file_name + '_alu.jpg')
    return plt


def draw_mlualu_cdf_from_log(file_name):
    pd_data = pd.read_csv(file_name)
    plt = pt.draw_cdf(
        mlu = pd_data['mlu'],
        again_mlu = pd_data['again_mlu'],
        mesh_mlu = pd_data['mesh_mlu'],
        mesh_again_mlu = pd_data['mesh_again_mlu'],
        # random_mlu = pd_data['random_mlu'],
        # random_again_mlu = pd_data['random_again_mlu'],
        # ftmv_mlu = pd_data['ftmv_mlu'],
        # ftmv_again_mlu = pd_data['ftmv_again_mlu'],

        alu = pd_data['alu'],
        again_alu = pd_data['again_alu'],
        mesh_alu = pd_data['mesh_alu'],
        mesh_again_alu = pd_data['mesh_again_alu'],

        # random_alu = pd_data['random_alu'],
        # random_again_alu = pd_data['random_again_alu'],
        # ftmv_alu = pd_data['ftmv_alu'],
        # ftmv_again_alu = pd_data['ftmv_again_alu'],

        # ahc = pd_data['ahc'],
        # again_ahc = pd_data['again_ahc'],
        # mesh_ahc = pd_data['mesh_ahc'],
        # mesh_again_ahc = pd_data['mesh_again_ahc'],
    )
    plt.show()
    # plt.savefig(file_name + '_mlualuahc.jpg')
    return plt


def draw_line_graph_from_log(file_name):
    pd_data = pd.read_csv(file_name, header = 0)

    plt = pt.draw_line_graph(
        mlu = pd_data['mlu'],
        again_mlu = pd_data['again_mlu'],
        mesh_mlu = pd_data['mesh_mlu'],
        mesh_again_mlu = pd_data['mesh_again_mlu'],
        # random_mlu = pd_data['random_mlu'],
        # random_again_mlu = pd_data['random_again_mlu'],
        # ftmv_mlu = pd_data['ftmv_mlu'],
        # ftmv_again_mlu = pd_data['ftmv_again_mlu'],
        alu = pd_data['alu'],
        again_alu = pd_data['again_alu'],
        mesh_alu = pd_data['mesh_alu'],
        mesh_again_alu = pd_data['mesh_again_alu'],
        # random_alu = pd_data['random_alu'],
        # random_again_alu = pd_data['random_again_alu'],
        # ftmv_alu = pd_data['ftmv_alu'],
        # ftmv_again_alu = pd_data['ftmv_again_alu'],
    )
    plt.show()
    # plt.savefig(file_name + 'linegraph.jpg')
    return plt

if __name__ == "__main__":
    # draw_mlu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/2ToE_TE.py.csv')
    # draw_mlu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/2ToE_mulitipleTE.py.log')
    # draw_mlu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/3ToE_mulitipleTE.py.log')
    # draw_mlu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/3ToE_TE.py.log')

    # draw_alu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/2ToE_TE.py.csv')
    # draw_alu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/2ToE_mulitipleTE.py.log')
    # draw_alu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/3ToE_mulitipleTE.py.log')
    # draw_alu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/3ToE_TE.py.log')

    # draw_mlualuahc_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/2ToE_TE.py.csv')
    # draw_mlualuahc_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/2ToE_mulitipleTE.py.log')
    # draw_mlualuahc_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/3ToE_mulitipleTE.py.log')
    # draw_mlualuahc_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/3ToE_TE.py.log')
    
    # draw_mlualu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/2ToE_self_TE_fixed1topo.py.log.good')
    # draw_line_graph_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/2ToE_self_TE_fixed1topo.py.log')
    draw_mlualu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/2ToE_self_TE_fixed1topo.py.log')
    # draw_mlualu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/2ToE_self_multiTE_fixed1topo.py.log')
    # draw_mlualu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/ftmv_2ToE_fixed1topo.py.log')
    # draw_line_graph_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/ftmv_2ToE_fixed1topo.py.log')
