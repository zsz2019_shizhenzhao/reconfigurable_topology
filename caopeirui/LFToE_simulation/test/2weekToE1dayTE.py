"""
两次ToE，第二次用了ρ（scale traffic）做ToE之后，在做普通LP routing能保证mlu和alu的cdf图都好。
如果LE再做一次，mlu会好的更多，但是损失一部分alu。
"""
import numpy as np
from root_const import ROOT_PATH
import caopeirui.LFToE_simulation.find_fractional_topology as fft
import caopeirui.LFToE_simulation.find_traffic_cluster as ftc
# import caopeirui.LFToE_simulation.LP_routing as lr
# import caopeirui.LFToE_simulation.LP_routing_w2 as lr
import caopeirui.LFToE_simulation.LP_routing_sen as lr
import caopeirui.LFToE_simulation.mesh_topology as mt
import caopeirui.LFToE_simulation.valiant_routing as vr
from utils.base import cosine_similarity
import caopeirui.LFToE_simulation.preprocess as pp
import logging
from utils.log import init_log


init_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/' + __file__)

# 288个采样点是一天
TE_PERIOD = 288

def two_week_ToE(conf_file, rep_traffic):
    """
    Desc: 整合拓扑工程的分步过程
    """
    pred_traffic = rep_traffic
    # 得到fractional topology, 即pod间连接数的非整数解
    obj_frac_topo = fft.FindFractionalTopology()
    obj_frac_topo.load_init_config(conf_file)

    obj_frac_topo.set_traffic_sequence(pred_traffic)
    u, d_ij = obj_frac_topo.get_fractional_topology()
    # print(d_ij)
    print('scaling factor = ', u)
    # print(d_ij.sum())

    d_ij = obj_frac_topo.again_ToE(1.01 / u)

    mesh_obj = mt.MeshTopology()
    mesh_obj.load_init_config(pp.CONF_FILE)
    mesh_x_ij = mesh_obj.get_connects_matrix()
    # np.set_printoptions(precision=3, suppress=True)
    # print(d_ij)
    # print(mesh_x_ij)
    d_ij = d_ij * 0.5 + mesh_x_ij * 0.5

    d_ij = obj_frac_topo.to_integer_topo(d_ij)

    # print(d_ij)
    # print(d_ij.sum())
    # print(d_ij.sum(axis=0))
    # print(d_ij.sum(axis=1))
    # exit()
    return d_ij


def record_routing(conf_file, x_ij, traffic):
    obj_TE = lr.LinearProgrammingRouting(x_ij)
    obj_TE.load_init_config(conf_file)
    w_routing, max_link_utilization = obj_TE.traffic_engineering_grb(traffic)
    return obj_TE


def record_again_routing(conf_file, x_ij, traffic, start_index):
    obj_TE = lr.LinearProgrammingRouting(x_ij)
    obj_TE.load_init_config(conf_file)
    w_routing, max_link_utilization = obj_TE.traffic_engineering_grb(traffic)
    obj_TE.again_routing(traffic, max_link_utilization, start_index)
    return obj_TE


def test_LP_TE(conf_file, x_ij, mesh_x_ij, future_traffic, start_index):
    # 未来两周，每天traffic做出代表性矩阵 evaluate 下一天
    one_day_traffic = future_traffic[start_index : start_index + TE_PERIOD]
    obj_km = ftc.TrafficKMeans(k = 4)
    for a_traffic in one_day_traffic:
        obj_km.add_a_traffic(a_traffic)
    representative_traffic = obj_km.get_pred_traffic()
    # 预测未来代表性矩阵的时候也做一下 scale
    representative_traffic = pp.scale_traffic(representative_traffic)

    obj_TE = record_routing(conf_file, x_ij, representative_traffic)
    obj_again_TE = record_again_routing(conf_file, x_ij, representative_traffic, start_index)

    mesh_TE = record_routing(conf_file, mesh_x_ij, representative_traffic)
    mesh_again_TE = record_again_routing(conf_file, mesh_x_ij, representative_traffic, start_index)
    
    for k in range(TE_PERIOD, 2 * TE_PERIOD):
        # evaluate
        evaluate_traffic = future_traffic[start_index + k]

        print('problem? ', start_index + k)

        lu, mlu, alu = obj_TE.calc_link_utilization(evaluate_traffic)
        ahc = obj_TE.ave_hop_count(evaluate_traffic)
        again_lu, again_mlu, again_alu = obj_again_TE.calc_link_utilization(evaluate_traffic)
        again_ahc = obj_again_TE.ave_hop_count(evaluate_traffic)

        mesh_lu, mesh_mlu, mesh_alu = mesh_TE.calc_link_utilization(evaluate_traffic)
        mesh_ahc = mesh_TE.ave_hop_count(evaluate_traffic)
        mesh_again_lu, mesh_again_mlu, mesh_again_alu = mesh_again_TE.calc_link_utilization(evaluate_traffic)
        mesh_again_ahc = mesh_again_TE.ave_hop_count(evaluate_traffic)

        res_str = (f'{mlu},{alu},{ahc},{again_mlu},{again_alu},{again_ahc},'
            f'{mesh_mlu},{mesh_alu},{mesh_ahc},{mesh_again_mlu},{mesh_again_alu},{mesh_again_ahc}')
        
        logging.critical(res_str)

    return mlu, alu, ahc, again_mlu, again_alu, again_ahc


def compare_result(start_index):
    # 每个测试程序都利用统一接口获取输入
    # scale = True 引入参数rou，代表性矩阵做scale数值拓展
    rep_traffic, future_traffic = pp.get_traffic_data(start_index, scale = True)

    x_ij = two_week_ToE(pp.CONF_FILE, rep_traffic)
    print('----our topo----')
    print(x_ij)
    print(x_ij.sum())
    print(x_ij.sum(axis=0))
    print(x_ij.sum(axis=1))

    obj = mt.MeshTopology()
    obj.load_init_config(pp.CONF_FILE)
    mesh_x_ij = obj.get_connects_matrix()
    print('----mesh topo----')
    print(mesh_x_ij)
    print(mesh_x_ij.sum())
    print(mesh_x_ij.sum(axis=0))
    print(mesh_x_ij.sum(axis=1))

    for i in range(start_index, start_index + pp.WINDOW_SIZE, TE_PERIOD):
        # 两周中每次移动一天
        test_LP_TE(pp.CONF_FILE, x_ij, mesh_x_ij, future_traffic, i)


if __name__ == "__main__":
    print('Desc: two week ToE one day TE')
    print(f'读取{pp.CONF_FILE}')
    logging.critical(
        ('mlu,alu,ahc,again_mlu,again_alu,again_ahc,'
         'mesh_mlu,mesh_alu,mesh_ahc,mesh_again_mlu,mesh_again_alu,mesh_again_ahc')
    )
    for i in range(0, 3):
        compare_result(i * pp.WINDOW_SIZE)

   