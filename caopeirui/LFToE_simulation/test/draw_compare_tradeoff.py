import utils.paint as pt
import pandas as pd
from root_const import ROOT_PATH 
import numpy as np

def get_mlumax_alumean(file_name):
    pd_data = pd.read_csv(file_name, header = 0)
    mean_list = []
    #mean_list.append(np.percentile(pd_data['lrsenw2_alu_1'], 50))
    #mean_list.append(np.percentile(pd_data['lrsenw2_alu_3'], 50))
    #mean_list.append(np.percentile(pd_data['lrsenw2_alu_5'], 50))
    #mean_list.append(np.percentile(pd_data['lrsenw2_alu_7'], 50))
    #mean_list.append(np.percentile(pd_data['lrsenw2_alu_9'], 50))
    mean_list.append(pd_data['lrsenw2_alu_1'].mean())
    mean_list.append(pd_data['lrsenw2_alu_3'].mean())
    mean_list.append(pd_data['lrsenw2_alu_5'].mean())
    mean_list.append(pd_data['lrsenw2_alu_7'].mean())
    mean_list.append(pd_data['lrsenw2_alu_9'].mean())
    max_list = []
    max_list.append(np.percentile(pd_data['lrsenw2_mlu_1'], 99))
    max_list.append(np.percentile(pd_data['lrsenw2_mlu_3'], 99))
    max_list.append(np.percentile(pd_data['lrsenw2_mlu_5'], 99))
    max_list.append(np.percentile(pd_data['lrsenw2_mlu_7'], 99))
    max_list.append(np.percentile(pd_data['lrsenw2_mlu_9'], 99))

    return max_list, mean_list

def get_mlumax_alumean2(file_name):
    pd_data = pd.read_csv(file_name, header = 0)
    mean_list = []
    #mean_list.append(np.percentile(pd_data['lrahcsen_alu_100'], 50))
    #mean_list.append(np.percentile(pd_data['lrahcsen_alu_95'], 50))
    #mean_list.append(np.percentile(pd_data['lrahcsen_alu_90'], 50))
    #mean_list.append(np.percentile(pd_data['lrahcsen_alu_85'], 50))
    #mean_list.append(np.percentile(pd_data['lrahcsen_alu_80'], 50))
    mean_list.append(pd_data['lrahcsen_alu_100'].mean())
    mean_list.append(pd_data['lrahcsen_alu_95'].mean())
    mean_list.append(pd_data['lrahcsen_alu_90'].mean())
    mean_list.append(pd_data['lrahcsen_alu_85'].mean())
    mean_list.append(pd_data['lrahcsen_alu_80'].mean())
    max_list = []
    max_list.append(np.percentile(pd_data['lrahcsen_mlu_100'], 99))
    max_list.append(np.percentile(pd_data['lrahcsen_mlu_95'], 99))
    max_list.append(np.percentile(pd_data['lrahcsen_mlu_90'], 99))
    max_list.append(np.percentile(pd_data['lrahcsen_mlu_85'], 99))
    max_list.append(np.percentile(pd_data['lrahcsen_mlu_80'], 99))

    return max_list, mean_list

if __name__ == "__main__":
    # get_mean_and_max(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/tradeoff_mlu_alu.py.log')
    old_max_list0, old_mean_list0 = get_mlumax_alumean2(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/2ToE_result/tradeoff_lr_ahc_oldsen.py.log')
    print(old_max_list0)
    print(old_mean_list0)
    old_max_list, old_mean_list = get_mlumax_alumean2(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/mesh_result/tradeoff_lr_ahc_oldsen.py.log')
    print(old_max_list)
    print(old_mean_list)
    max_list0, mean_list0 = get_mlumax_alumean2(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/2ToE_result/tradeoff_lr_ahc_sen.py.log')
    print(max_list0)
    print(mean_list0)
    max_list, mean_list = get_mlumax_alumean2(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/mesh_result/tradeoff_lr_ahc_sen.py.log')
    print(max_list)
    print(mean_list)
    plt = pt.draw_line_graph(
        ToE2_oldsen = [old_max_list0, old_mean_list0],
        mesh_oldsen = [old_max_list, old_mean_list],
        ToE2_sen = [max_list0, mean_list0],
        mesh_sen = [max_list, mean_list]
    )
    plt.show()