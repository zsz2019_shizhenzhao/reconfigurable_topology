import utils.paint as pt
import pandas as pd
from root_const import ROOT_PATH 
import numpy as np

def draw_mlu_cdf_from_log(file_name):
    pd_data = pd.read_csv(file_name)

    plt = pt.draw_cdf(
        mlu = pd_data['mlu'],
        mesh_mlu = pd_data['mesh_mlu'],
        random_mlu = pd_data['random_mlu'],
    )
    # plt.savefig(file_name + '_mlu.jpg')
    return plt

def draw_alu_cdf_from_log(file_name):
    pd_data = pd.read_csv(file_name)

    plt = pt.draw_cdf(
        alu = pd_data['alu'],
        mesh_alu = pd_data['mesh_alu'],
        random_alu = pd_data['random_alu'],
    )
    # plt.savefig(file_name + '_alu.jpg')
    return plt


def draw_mlualu_cdf_from_log(file_name):
    pd_data = pd.read_csv(file_name)
    plt = pt.draw_cdf(
        lrsenw2_mlu_1 = pd_data['lrsenw2_mlu_1'],
        lrsenw2_mlu_3 = pd_data['lrsenw2_mlu_3'],
        lrsenw2_mlu_5 = pd_data['lrsenw2_mlu_5'],
        lrsenw2_mlu_7 = pd_data['lrsenw2_mlu_7'],
        lrsenw2_mlu_9 = pd_data['lrsenw2_mlu_9'],

        lrsenw2_alu_1 = pd_data['lrsenw2_alu_1'],
        lrsenw2_alu_3 = pd_data['lrsenw2_alu_3'],
        lrsenw2_alu_5 = pd_data['lrsenw2_alu_5'],
        lrsenw2_alu_7 = pd_data['lrsenw2_alu_7'],
        lrsenw2_alu_9 = pd_data['lrsenw2_alu_9'],
    )
    plt.show()
    # plt.savefig(file_name + '_mlualuahc.jpg')
    return plt


def draw_lrahcsen_cdf_from_log(file_name):
    pd_data = pd.read_csv(file_name)
    plt = pt.draw_cdf(
        lrahcsen_mlu_100 = pd_data['lrahcsen_mlu_100'],
        lrahcsen_mlu_95 = pd_data['lrahcsen_mlu_95'],
        lrahcsen_mlu_90 = pd_data['lrahcsen_mlu_90'],
        lrahcsen_mlu_85 = pd_data['lrahcsen_mlu_85'],
        lrahcsen_mlu_80 = pd_data['lrahcsen_mlu_80'],

        lrahcsen_alu_100 = pd_data['lrahcsen_alu_100'],
        lrahcsen_alu_95 = pd_data['lrahcsen_alu_95'],
        lrahcsen_alu_90 = pd_data['lrahcsen_alu_90'],
        lrahcsen_alu_85 = pd_data['lrahcsen_alu_85'],
        lrahcsen_alu_80 = pd_data['lrahcsen_alu_80'],
    )
    plt.show()
    # plt.savefig(file_name + '_mlualuahc.jpg')
    return plt


def draw_line_graph_from_log(file_name):
    pd_data = pd.read_csv(file_name, header = 0)

    plt = pt.draw_line_graph(
        lr_mlu = pd_data['lr_mlu'],
        lragain_mlu = pd_data['lragain_mlu'],
        lrw2_mlu = pd_data['lrw2_mlu'],
        lrsen_mlu = pd_data['lrsen_mlu'],

        lr_alu = pd_data['lr_alu'],
        lragain_alu = pd_data['lragain_alu'],
        lrw2_alu = pd_data['lrw2_alu'],
        lrsen_alu = pd_data['lrsen_alu'],
    )
    plt.show()
    # plt.savefig(file_name + 'linegraph.jpg')
    return plt

if __name__ == "__main__":
    # draw_mlualu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/tradeoff_mlu_alu.py.log')
    draw_lrahcsen_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/tradeoff_lr_ahc_oldsen.py.log')
    # draw_mlualu_cdf_from_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/tradeoff_oldsen_mlu_alu.py.log')
