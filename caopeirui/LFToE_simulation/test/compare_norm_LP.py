"""
两次ToE，第二次用了ρ（scale traffic）做ToE之后，在做普通LP routing能保证mlu和alu的cdf图都好。
如果LE再做一次，mlu会好的更多，但是损失一部分alu。
"""
import numpy as np
from root_const import ROOT_PATH
import caopeirui.LFToE_simulation.find_fractional_topology as fft
import caopeirui.LFToE_simulation.find_topo_by_mlu_oldsen_w2 as fft_mlu_oldw2
import caopeirui.LFToE_simulation.find_traffic_cluster as ftc
# import caopeirui.LFToE_simulation.map_to_ocs as mto
import caopeirui.LFToE_simulation.LP_routing as lr
import caopeirui.LFToE_simulation.LP_routing_ahc_oldsen as lraold
import caopeirui.LFToE_simulation.LP_routing_ahc_sen as lrasen
import caopeirui.LFToE_simulation.LP_routing_oldsen_w2 as lroldw2
import caopeirui.LFToE_simulation.LP_routing_oldsen_w2_std as lroldw2std
import caopeirui.LFToE_simulation.LP_routing_sen as lrsen
import caopeirui.LFToE_simulation.LP_routing_sen_w2 as lrsenw2
import caopeirui.LFToE_simulation.LP_routing_varw2 as lrvarw2
import caopeirui.LFToE_simulation.LP_routing_w2 as lrw2
import caopeirui.LFToE_simulation.LP_routing_oldsen_w_std as lroldwstd
import caopeirui.LFToE_simulation.mesh_topology as mt
import ZB.random_topology as zbrt
from ZB.convex_hull import TrafficConvex
import caopeirui.LFToE_simulation.valiant_routing as vr
from utils.base import cosine_similarity
import caopeirui.LFToE_simulation.preprocess as pp
import logging
from utils.log import init_log


# 存储不同 representative traffic 对应 routing 的 类对象
# 后面直接调用，避免重复
LP_list = []

LP_routing = []
LP_routing_ahc_oldsen = []
LP_routing_ahc_sen = []
LP_routing_oldsen_w2 = []
LP_routing_sen = []
LP_routing_sen_w2 = []
LP_routing_varw2 = []
LP_routing_w2 = []
LP_routing_oldsen_w2_std = []
LP_routing_oldsen_w_std = []


def low_frequency_ToE(conf_file, rep_traffic):
    """
    Desc: 整合拓扑工程的分步过程
    """
    pred_traffic = rep_traffic
    # 得到fractional topology, 即pod间连接数的非整数解
    obj_frac_topo = fft.FindFractionalTopology()
    obj_frac_topo.load_init_config(conf_file)

    obj_frac_topo.set_traffic_sequence(pred_traffic)
    u, d_ij = obj_frac_topo.get_fractional_topology()
    # print(d_ij)
    print('scaling factor = ', u)
    # print(d_ij.sum())

    d_ij = obj_frac_topo.again_ToE(1.01 / u)
    d_ij = obj_frac_topo.to_integer_topo(d_ij)
    # d_ij = obj_frac_topo.third_ToE(1 / u, d_ij)

    return d_ij

def find_topo_by_mlu_oldsen_w2(conf_file, rep_traffic, start_index):
    pred_traffic = rep_traffic

    obj_frac_topo = fft_mlu_oldw2.FindFractionalTopology()
    obj_frac_topo.load_init_config(conf_file)

    obj_frac_topo.set_traffic_sequence(pred_traffic)
    mlu, d_ij = obj_frac_topo.get_fractional_topology()
    d_ij = obj_frac_topo.again_ToE(mlu, start_index)
    d_ij = obj_frac_topo.third_ToE(mlu,start_index = start_index)
    d_ij = obj_frac_topo.to_integer_topo(d_ij)
    return d_ij

def record_routing(obj_TE, conf_file, x_ij, traffic, start_index):
    obj_TE.load_init_config(conf_file)
    obj_TE.routing(traffic, start_index)
    return obj_TE


def test_LP_TE(conf_file, x_ij, rep_traffic, future_traffic, i):
    # 当前矩阵与哪个代表性矩阵相似度高，就用哪个做routing
    cos_sim = 0

    opt_index = None
    for t in range(len(rep_traffic)):
        tmp_cos_sim = cosine_similarity(rep_traffic[t].flatten().tolist(), future_traffic[i].flatten().tolist())
        if tmp_cos_sim > cos_sim:
            cos_sim = tmp_cos_sim
            opt_index = t

    _ ,mlu1, alu1 = LP_routing[opt_index].calc_normal_link_utilization(future_traffic[i + 1])
    _ ,mlu2, alu2 = LP_routing_ahc_oldsen[opt_index].calc_normal_link_utilization(future_traffic[i + 1])
    _ ,mlu3, alu3 = LP_routing_ahc_sen[opt_index].calc_normal_link_utilization(future_traffic[i + 1])
    _ ,mlu4, alu4 = LP_routing_oldsen_w2[opt_index].calc_normal_link_utilization(future_traffic[i + 1])
    _ ,mlu5, alu5 = LP_routing_sen[opt_index].calc_normal_link_utilization(future_traffic[i + 1])
    _ ,mlu6, alu6 = LP_routing_sen_w2[opt_index].calc_normal_link_utilization(future_traffic[i + 1])
    _ ,mlu7, alu7 = LP_routing_varw2[opt_index].calc_normal_link_utilization(future_traffic[i + 1])
    _ ,mlu8, alu8 = LP_routing_w2[opt_index].calc_normal_link_utilization(future_traffic[i + 1])
    _ ,mlu9, alu9 = LP_routing_oldsen_w2_std[opt_index].calc_normal_link_utilization(future_traffic[i + 1])
    _ ,mlu10, alu10 = LP_routing_oldsen_w_std[opt_index].calc_normal_link_utilization(future_traffic[i + 1])

    ahc1 = LP_routing[opt_index].ave_hop_count(future_traffic[i + 1])
    ahc2 = LP_routing_ahc_oldsen[opt_index].ave_hop_count(future_traffic[i + 1])
    ahc3 = LP_routing_ahc_sen[opt_index].ave_hop_count(future_traffic[i + 1])
    ahc4 = LP_routing_oldsen_w2[opt_index].ave_hop_count(future_traffic[i + 1])
    ahc5 = LP_routing_sen[opt_index].ave_hop_count(future_traffic[i + 1])
    ahc6 = LP_routing_sen_w2[opt_index].ave_hop_count(future_traffic[i + 1])
    ahc7 = LP_routing_varw2[opt_index].ave_hop_count(future_traffic[i + 1])
    ahc8 = LP_routing_w2[opt_index].ave_hop_count(future_traffic[i + 1])
    ahc9 = LP_routing_oldsen_w2_std[opt_index].ave_hop_count(future_traffic[i + 1])
    ahc10 = LP_routing_oldsen_w_std[opt_index].ave_hop_count(future_traffic[i + 1])
    res_str = (f'{mlu1},{alu1},{ahc1},'
        f'{mlu2},{alu2},{ahc2},'
        f'{mlu3},{alu3},{ahc3},'
        f'{mlu4},{alu4},{ahc4},'
        f'{mlu5},{alu5},{ahc5},'
        f'{mlu6},{alu6},{ahc6},'
        f'{mlu7},{alu7},{ahc7},'
        f'{mlu8},{alu8},{ahc8},'
        f'{mlu9},{alu9},{ahc9},'
        f'{mlu10},{alu10},{ahc10}')
    
    logging.critical(res_str)


def compare_result(start_index, topo='Toe', kmeans_num=4):
    # 每个测试程序都利用统一接口获取输入
    # scale = True 引入参数rou，代表性矩阵做scale数值拓展


    rep_traffic, future_traffic = pp.get_traffic_data(start_index, scale = True, kmeans_num=kmeans_num)

    if topo == 'Toe':
        x_ij = low_frequency_ToE(pp.CONF_FILE, rep_traffic)
    elif topo == 'Toe_oldsen_w2':
        x_ij = find_topo_by_mlu_oldsen_w2(pp.CONF_FILE, rep_traffic, start_index)
    else:
        obj = mt.MeshTopology()
        obj.load_init_config(pp.CONF_FILE)
        x_ij = obj.get_connects_matrix()
    print(x_ij)
    print(x_ij.sum())
    print(x_ij.sum(axis=0))
    print(x_ij.sum(axis=1))

    # 每两周为周期预测、比较完后，刷新全局ROUTING存储变量。
    global LP_routing
    global LP_routing_ahc_oldsen
    global LP_routing_ahc_sen
    global LP_routing_oldsen_w2
    global LP_routing_sen
    global LP_routing_sen_w2 
    global LP_routing_varw2
    global LP_routing_w2
    global LP_routing_oldsen_w2_std
    global LP_routing_oldsen_w_std
    LP_routing = []
    LP_routing_ahc_oldsen = []
    LP_routing_ahc_sen = []
    LP_routing_oldsen_w2 = []
    LP_routing_sen = []
    LP_routing_sen_w2 = []
    LP_routing_varw2 = []
    LP_routing_w2 = []
    LP_routing_oldsen_w2_std = []
    LP_routing_oldsen_w_std = []
    print('compare_result start_index: ' + str(start_index))
    # print(LR_ROUTING)

    # 计算各自topo对应不同 representative traffic 的 routing，存储到全局变量
    for i in range(len(rep_traffic)):
        LP_routing.append(record_routing(lr.LinearProgrammingRouting(x_ij), pp.CONF_FILE, x_ij, rep_traffic[i], start_index))
        LP_routing_ahc_oldsen.append(record_routing(lraold.LinearProgrammingRouting(x_ij), pp.CONF_FILE, x_ij, rep_traffic[i], start_index))
        LP_routing_ahc_sen.append(record_routing(lrasen.LinearProgrammingRouting(x_ij), pp.CONF_FILE, x_ij, rep_traffic[i], start_index))
        LP_routing_oldsen_w2.append(record_routing(lroldw2.LinearProgrammingRouting(x_ij), pp.CONF_FILE, x_ij, rep_traffic[i], start_index))
        LP_routing_oldsen_w2_std.append(record_routing(lroldw2std.LinearProgrammingRouting(x_ij), pp.CONF_FILE, x_ij, rep_traffic[i], start_index))
        LP_routing_sen.append(record_routing(lrsen.LinearProgrammingRouting(x_ij), pp.CONF_FILE, x_ij, rep_traffic[i], start_index))
        LP_routing_sen_w2.append(record_routing(lrsenw2.LinearProgrammingRouting(x_ij), pp.CONF_FILE, x_ij, rep_traffic[i], start_index))
        LP_routing_varw2.append(record_routing(lrvarw2.LinearProgrammingRouting(x_ij), pp.CONF_FILE, x_ij, rep_traffic[i], start_index))
        LP_routing_w2.append(record_routing(lrw2.LinearProgrammingRouting(x_ij), pp.CONF_FILE, x_ij, rep_traffic[i], start_index))
        LP_routing_oldsen_w_std.append(record_routing(lroldwstd.LinearProgrammingRouting(x_ij), pp.CONF_FILE, x_ij, rep_traffic[i], start_index))
    
    size = len(future_traffic) - 1
    for i in range(size):
        test_LP_TE(pp.CONF_FILE, x_ij, rep_traffic, future_traffic, i)


if __name__ == "__main__":
    from caopeirui.LFToE_simulation.test.norm_compare_config import *
    init_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/norm_result/' + FILE_NAME)
    print('Desc: 两次ToE, single-traffic TE')
    print(f'读取{pp.CONF_FILE}')
    logging.critical(
        ('mlu1,alu1,ahc1,'
        'mlu2,alu2,ahc2,'
        'mlu3,alu3,ahc3,'
        'mlu4,alu4,ahc4,'
        'mlu5,alu5,ahc5,'
        'mlu6,alu6,ahc6,'
        'mlu7,alu7,ahc7,'
        'mlu8,alu8,ahc8,'
        'mlu9,alu9,ahc9,'
        'mlu10,alu10,ahc10')
    )
    for i in range(week_start - 1, weeke_end):
        compare_result(i * 4032, topo=Topo,kmeans_num=kmeans_num)

   