"""
两次ToE，第二次用了ρ（scale traffic）做ToE之后，在做普通LP routing能保证mlu和alu的cdf图都好。
如果LE再做一次，mlu会好的更多，但是损失一部分alu。
"""
import numpy as np
from root_const import ROOT_PATH
import caopeirui.LFToE_simulation.find_fractional_topology as fft
import caopeirui.LFToE_simulation.find_topo_by_mean_var as ftmv

# import caopeirui.LFToE_simulation.map_to_ocs as mto
import caopeirui.LFToE_simulation.LP_routing as lr
# import caopeirui.LFToE_simulation.LP_routing_w2 as lr
# import caopeirui.LFToE_simulation.LP_routing_sen as lr
import caopeirui.LFToE_simulation.mesh_topology as mt
import ZB.random_topology as zbrt
from ZB.convex_hull import TrafficConvex
import caopeirui.LFToE_simulation.valiant_routing as vr
from utils.base import cosine_similarity
import caopeirui.LFToE_simulation.preprocess as pp
import logging
from utils.log import init_log


init_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/' + __file__)


# 存储不同representative traffic对应routing 的 类对象
# 后面直接调用，避免重复
OUR_ROUTING = []
OUR_AGAIN_ROUTING = []
MESH_ROUTING = []
MESH_AGAIN_ROUTING = []
RANDOM_ROUTING = []
RANDOM_AGAIN_ROUTING = []


def low_frequency_ToE(conf_file, rep_traffic, start_index = 0):
    """
    Desc: 整合拓扑工程的分步过程
    """
    # test begin
    # obj = ftmv.FindTopologyByMeanVar()
    # obj.load_init_config(pp.CONF_FILE)
    # d_ij = obj.get_topology(start_index)
    # return d_ij
    # 直接跑该程序，这里返回用的是mean最小链接后填充的topo
    # 注释掉上面几句话跑，走的逻辑是下面的
    # test end

    pred_traffic = rep_traffic
    
    # 得到fractional topology, 即pod间连接数的非整数解
    obj_frac_topo = fft.FindFractionalTopology()
    obj_frac_topo.load_init_config(conf_file)
    obj_frac_topo.set_traffic_sequence(pred_traffic)
    alpha, d_ij = obj_frac_topo.get_fractional_topology()
    print('scaling factor = ', alpha)

    d_ij = obj_frac_topo.again_ToE(1.01 / alpha, small_flag = False)
    d_ij = obj_frac_topo.to_integer_topo(d_ij)

    # 填充剩下极少的能填充的link
    d_ij = obj_frac_topo.fill_residual_links(d_ij)
    return d_ij


def record_routing(conf_file, x_ij, traffic):
    obj_TE = lr.LinearProgrammingRouting(x_ij)
    obj_TE.load_init_config(conf_file)
    w_routing, max_link_utilization = obj_TE.traffic_engineering_grb(traffic)
    return obj_TE


def record_again_routing(conf_file, x_ij, traffic, start_index):
    obj_TE = lr.LinearProgrammingRouting(x_ij)
    obj_TE.load_init_config(conf_file)
    w_routing, max_link_utilization = obj_TE.traffic_engineering_grb(traffic)
    obj_TE.again_routing(traffic, max_link_utilization)
    return obj_TE


def test_LP_TE(conf_file, x_ij, rep_traffic, future_traffic, i, method_name):
    # 当前矩阵与哪个代表性矩阵相似度高，就用哪个做routing
    cos_sim = 0

    opt_index = None
    for t in range(len(rep_traffic)):
        tmp_cos_sim = cosine_similarity(rep_traffic[t].flatten().tolist(), future_traffic[i+1].flatten().tolist())
        if tmp_cos_sim > cos_sim:
            cos_sim = tmp_cos_sim
            opt_index = t
    
    if method_name == 'our':
        obj_TE = OUR_ROUTING[opt_index]
        obj_again_TE = OUR_AGAIN_ROUTING[opt_index]
    elif method_name == 'mesh':
        obj_TE = MESH_ROUTING[opt_index]
        obj_again_TE = MESH_AGAIN_ROUTING[opt_index]
    elif method_name == 'random':
        obj_TE = RANDOM_ROUTING[opt_index]
        obj_again_TE = RANDOM_AGAIN_ROUTING[opt_index]

    # evaluate
    # lu, mlu, alu = obj_TE.calc_link_utilization(future_traffic[i + 1])
    lu, mlu, alu = obj_TE.calc_normal_link_utilization(future_traffic[i + 1])
    ave_hop_count = obj_TE.ave_hop_count(future_traffic[i + 1])

    # again_lu, again_mlu, again_alu = obj_again_TE.calc_link_utilization(future_traffic[i + 1])
    again_lu, again_mlu, again_alu = obj_again_TE.calc_normal_link_utilization(future_traffic[i + 1])
    again_ave_hop_count = obj_again_TE.ave_hop_count(future_traffic[i + 1])

    if mlu > 1:
        print(i, cos_sim, mlu, again_mlu)

    return mlu, alu, ave_hop_count, again_mlu, again_alu, again_ave_hop_count


def compare_result(start_index):
    # 每个测试程序都利用统一接口获取输入
    # scale = True 引入参数rou，代表性矩阵做scale数值拓展
    rep_traffic, future_traffic = pp.get_traffic_data(start_index, scale = False, kmeans_num = 4, overprovision = True)
    rep_traffic_routing, _ = pp.get_traffic_data(start_index, scale = False, kmeans_num = 6, overprovision = False)

    x_ij = low_frequency_ToE(pp.CONF_FILE, rep_traffic, start_index)

    print('----our topo----')
    print(x_ij)
    print(x_ij.sum())
    print(x_ij.sum(axis=0))
    print(x_ij.sum(axis=1))

    obj = mt.MeshTopology()
    obj.load_init_config(pp.CONF_FILE)
    mesh_x_ij = obj.get_connects_matrix()
    print('----mesh topo----')
    print(mesh_x_ij)
    print(mesh_x_ij.sum())
    print(mesh_x_ij.sum(axis=0))
    print(mesh_x_ij.sum(axis=1))

    obj = zbrt.RandomTopology()
    obj.load_init_config(pp.CONF_FILE)
    random_x_ij = obj.get_connects_matrix(1)
    print('----random topo----')
    print(random_x_ij)
    print(random_x_ij.sum())
    print(random_x_ij.sum(axis=0))
    print(random_x_ij.sum(axis=1))

    # 每两周为周期预测、比较完后，刷新全局ROUTING存储变量。
    global OUR_ROUTING
    global OUR_AGAIN_ROUTING
    global MESH_ROUTING
    global MESH_AGAIN_ROUTING
    global RANDOM_ROUTING
    global RANDOM_AGAIN_ROUTING
    OUR_ROUTING = []
    OUR_AGAIN_ROUTING = []
    MESH_ROUTING = []
    MESH_AGAIN_ROUTING = []
    RANDOM_ROUTING = []
    RANDOM_AGAIN_ROUTING = []
    print('compare_result start_index: ' + str(start_index))
    print(OUR_AGAIN_ROUTING)

    # 计算各自topo对应不同 representative traffic 的 routing，存储到全局变量
    for i in range(len(rep_traffic_routing)):
        our_obj = record_routing(pp.CONF_FILE, x_ij, rep_traffic_routing[i])
        our_again_obj = record_again_routing(pp.CONF_FILE, x_ij, rep_traffic_routing[i], start_index)

        OUR_ROUTING.append(our_obj)
        OUR_AGAIN_ROUTING.append(our_again_obj)

        mesh_obj = record_routing(pp.CONF_FILE, mesh_x_ij, rep_traffic_routing[i])
        mesh_again_obj = record_again_routing(pp.CONF_FILE, mesh_x_ij, rep_traffic_routing[i], start_index)
        MESH_ROUTING.append(mesh_obj)
        MESH_AGAIN_ROUTING.append(mesh_again_obj)

        random_obj = record_routing(pp.CONF_FILE, random_x_ij, rep_traffic_routing[i])
        random_again_obj = record_again_routing(pp.CONF_FILE, random_x_ij, rep_traffic_routing[i], start_index)
        RANDOM_ROUTING.append(random_obj)
        RANDOM_AGAIN_ROUTING.append(random_again_obj)

    size = len(future_traffic) - 1
    for i in range(size):
        mlu, alu, ahc, again_mlu, again_alu, again_ahc  = test_LP_TE(pp.CONF_FILE, x_ij, rep_traffic_routing, future_traffic, i, 'our')
        mesh_mlu, mesh_alu, mesh_ahc, mesh_again_mlu, mesh_again_alu, mesh_again_ahc  = test_LP_TE(pp.CONF_FILE, mesh_x_ij, rep_traffic_routing, future_traffic, i, 'mesh')
        # random_mlu, random_alu, random_ahc, random_again_mlu, random_again_alu, random_again_ahc  = test_LP_TE(pp.CONF_FILE, random_x_ij, rep_traffic_routing, future_traffic, i, 'random')

        good = 0
        if mlu < mesh_mlu and alu < mesh_alu and alu < mesh_alu: # and alu < random_alu:
            good = 1

        res_str = (f'{good},{mlu},{alu},{ahc},{again_mlu},{again_alu},{again_ahc},'
         f'{mesh_mlu},{mesh_alu},{mesh_ahc},{mesh_again_mlu},{mesh_again_alu},{mesh_again_ahc}')
         # f'{random_mlu},{random_alu},{random_ahc},{random_again_mlu},{random_again_alu},{random_again_ahc}')
        
        logging.critical(res_str)


if __name__ == "__main__":
    print('Desc: 两次ToE, single-traffic TE')
    print(f'读取{pp.CONF_FILE}')
    logging.critical(
        ('good,mlu,alu,ahc,again_mlu,again_alu,again_ahc,'
         'mesh_mlu,mesh_alu,mesh_ahc,mesh_again_mlu,mesh_again_alu,mesh_again_ahc')
         # 'random_mlu,random_alu,random_ahc,random_again_mlu,random_again_alu,random_again_ahc')
    )
    for i in range(3,4):
        compare_result(i * pp.WINDOW_SIZE)
