"""
两次ToE，第二次用了ρ（scale traffic）做ToE之后，在做普通LP routing能保证mlu和alu的cdf图都好。
让 sen 的 abs_var 结果*系数，再去做w2 routing
对比系数1.1,1.2 …… 结果。画出  横坐标mlu，纵坐标alu 图。
"""
import numpy as np
from root_const import ROOT_PATH
import caopeirui.LFToE_simulation.find_fractional_topology as fft
import caopeirui.LFToE_simulation.find_traffic_cluster as ftc
# import caopeirui.LFToE_simulation.map_to_ocs as mto
import caopeirui.LFToE_simulation.LP_routing_ahc_sen as lrahcsen
import caopeirui.LFToE_simulation.mesh_topology as mt
import ZB.random_topology as zbrt
from ZB.convex_hull import TrafficConvex
import caopeirui.LFToE_simulation.valiant_routing as vr
from utils.base import cosine_similarity
import caopeirui.LFToE_simulation.preprocess as pp
import logging
from utils.log import init_log


init_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/' + __file__)

# 存储不同 representative traffic 对应 routing 的 类对象
# 后面直接调用，避免重复
LRAHCSEN_ROUTING_100 = []  # coe = 1
LRAHCSEN_ROUTING_95 = []  # coe = 0.95
LRAHCSEN_ROUTING_90 = []  # coe = 0.9
LRAHCSEN_ROUTING_85 = []  # coe = 0.85
LRAHCSEN_ROUTING_80 = []  # coe = 0.8

def low_frequency_ToE(conf_file, rep_traffic):
    """
    Desc: 整合拓扑工程的分步过程
    """
    pred_traffic = rep_traffic
    # 得到fractional topology, 即pod间连接数的非整数解
    obj_frac_topo = fft.FindFractionalTopology()
    obj_frac_topo.load_init_config(conf_file)

    obj_frac_topo.set_traffic_sequence(pred_traffic)
    u, d_ij = obj_frac_topo.get_fractional_topology()
    # print(d_ij)
    print('scaling factor = ', u)
    # print(d_ij.sum())

    d_ij = obj_frac_topo.again_ToE(1.01 / u)
    d_ij = obj_frac_topo.to_integer_topo(d_ij)
    # d_ij = obj_frac_topo.third_ToE(1 / u, d_ij)

    return d_ij


def record_lrahcsen_routing(conf_file, x_ij, traffic, coe, start_index):
    obj_TE = lrahcsen.LinearProgrammingRouting(x_ij)
    obj_TE.load_init_config(conf_file)
    w_routing, max_link_utilization = obj_TE.traffic_engineering_grb(traffic)
    abs_var = obj_TE.again_routing(traffic, max_link_utilization)
    obj_TE.third_routing(traffic, max_link_utilization, coe, start_index)
    return obj_TE


def test_LP_TE(conf_file, x_ij, rep_traffic, future_traffic, i):
    # 当前矩阵与哪个代表性矩阵相似度高，就用哪个做routing
    cos_sim = 0

    opt_index = None
    for t in range(len(rep_traffic)):
        tmp_cos_sim = cosine_similarity(rep_traffic[t].flatten().tolist(), future_traffic[i].flatten().tolist())
        if tmp_cos_sim > cos_sim:
            cos_sim = tmp_cos_sim
            opt_index = t

    lrahcsen_lu_100, lrahcsen_mlu_100, lrahcsen_alu_100 = LRAHCSEN_ROUTING_100[opt_index].calc_link_utilization(future_traffic[i + 1])
    lrahcsen_ahc_100 = LRAHCSEN_ROUTING_100[opt_index].ave_hop_count(future_traffic[i + 1])

    lrahcsen_lu_95, lrahcsen_mlu_95, lrahcsen_alu_95 = LRAHCSEN_ROUTING_95[opt_index].calc_link_utilization(future_traffic[i + 1])
    lrahcsen_ahc_95 = LRAHCSEN_ROUTING_95[opt_index].ave_hop_count(future_traffic[i + 1])

    lrahcsen_lu_90, lrahcsen_mlu_90, lrahcsen_alu_90 = LRAHCSEN_ROUTING_90[opt_index].calc_link_utilization(future_traffic[i + 1])
    lrahcsen_ahc_90 = LRAHCSEN_ROUTING_90[opt_index].ave_hop_count(future_traffic[i + 1])

    lrahcsen_lu_85, lrahcsen_mlu_85, lrahcsen_alu_85 = LRAHCSEN_ROUTING_85[opt_index].calc_link_utilization(future_traffic[i + 1])
    lrahcsen_ahc_85 = LRAHCSEN_ROUTING_85[opt_index].ave_hop_count(future_traffic[i + 1])

    lrahcsen_lu_80, lrahcsen_mlu_80, lrahcsen_alu_80 = LRAHCSEN_ROUTING_80[opt_index].calc_link_utilization(future_traffic[i + 1])
    lrahcsen_ahc_80 = LRAHCSEN_ROUTING_80[opt_index].ave_hop_count(future_traffic[i + 1])

    res_str = (
        f'{lrahcsen_mlu_100},{lrahcsen_alu_100},{lrahcsen_ahc_100},'
        f'{lrahcsen_mlu_95},{lrahcsen_alu_95},{lrahcsen_ahc_95},'
        f'{lrahcsen_mlu_90},{lrahcsen_alu_90},{lrahcsen_ahc_90},'
        f'{lrahcsen_mlu_85},{lrahcsen_alu_85},{lrahcsen_ahc_85},'
        f'{lrahcsen_mlu_80},{lrahcsen_alu_80},{lrahcsen_ahc_80},'
    )

    logging.critical(res_str)


def compare_result(start_index):
    # 每个测试程序都利用统一接口获取输入
    # scale = True 引入参数rou，代表性矩阵做scale数值拓展
    rep_traffic, future_traffic = pp.get_traffic_data(start_index, scale = True)

    #x_ij = low_frequency_ToE(pp.CONF_FILE, rep_traffic)
    #print(x_ij)
    #print(x_ij.sum())
    #print(x_ij.sum(axis=0))
    #print(x_ij.sum(axis=1))

    obj = mt.MeshTopology()
    obj.load_init_config(pp.CONF_FILE)
    x_ij = obj.get_connects_matrix()
    print(x_ij)
    print(x_ij.sum())
    print(x_ij.sum(axis=0))
    print(x_ij.sum(axis=1))

    # 每两周为周期预测、比较完后，刷新全局ROUTING存储变量。
    global LRAHCSEN_ROUTING_100
    global LRAHCSEN_ROUTING_95
    global LRAHCSEN_ROUTING_90
    global LRAHCSEN_ROUTING_85
    global LRAHCSEN_ROUTING_80
    LRAHCSEN_ROUTING_100 = []
    LRAHCSEN_ROUTING_95 = []
    LRAHCSEN_ROUTING_90 = []
    LRAHCSEN_ROUTING_85 = []
    LRAHCSEN_ROUTING_80 = []

    print('compare_result start_index: ' + str(start_index))
    #print(LRAHCSEN_ROUTING_80)

    # 计算各自topo对应不同 representative traffic 的 routing，存储到全局变量
    for i in range(len(rep_traffic)):
        lrahcsen_obj_100 = record_lrahcsen_routing(pp.CONF_FILE, x_ij, rep_traffic[i], 0.99, start_index)
        lrahcsen_obj_95 = record_lrahcsen_routing(pp.CONF_FILE, x_ij, rep_traffic[i], 0.9, start_index)
        lrahcsen_obj_90 = record_lrahcsen_routing(pp.CONF_FILE, x_ij, rep_traffic[i], 0.8, start_index)
        lrahcsen_obj_85 = record_lrahcsen_routing(pp.CONF_FILE, x_ij, rep_traffic[i], 0.7, start_index)
        lrahcsen_obj_80 = record_lrahcsen_routing(pp.CONF_FILE, x_ij, rep_traffic[i], 0.6, start_index)

        LRAHCSEN_ROUTING_100.append(lrahcsen_obj_100)
        LRAHCSEN_ROUTING_95.append(lrahcsen_obj_95)
        LRAHCSEN_ROUTING_90.append(lrahcsen_obj_90)
        LRAHCSEN_ROUTING_85.append(lrahcsen_obj_85)
        LRAHCSEN_ROUTING_80.append(lrahcsen_obj_80)

    size = len(future_traffic) - 1
    for i in range(size):
        test_LP_TE(pp.CONF_FILE, x_ij, rep_traffic, future_traffic, i)


if __name__ == "__main__":
    print('Desc: 两次ToE, single-traffic TE')
    print(f'读取{pp.CONF_FILE}')
    logging.critical(
        ('lrahcsen_mlu_100,lrahcsen_alu_100,lrahcsen_ahc_100,'
        'lrahcsen_mlu_95,lrahcsen_alu_95,lrahcsen_ahc_95,'
        'lrahcsen_mlu_90,lrahcsen_alu_90,lrahcsen_ahc_90,'
        'lrahcsen_mlu_85,lrahcsen_alu_85,lrahcsen_ahc_85,'
        'lrahcsen_mlu_80,lrahcsen_alu_80,lrahcsen_ahc_80,')
    )
    for i in range(10):
        compare_result(i * 4032)

   