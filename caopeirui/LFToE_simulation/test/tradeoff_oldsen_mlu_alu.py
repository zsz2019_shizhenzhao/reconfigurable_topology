"""
两次ToE，第二次用了ρ（scale traffic）做ToE之后，在做普通LP routing能保证mlu和alu的cdf图都好。
让 sen 的 abs_var 结果*系数，再去做w2 routing
对比系数1.1,1.2 …… 结果。画出  横坐标mlu，纵坐标alu 图。
"""
import numpy as np
from root_const import ROOT_PATH
import caopeirui.LFToE_simulation.find_fractional_topology as fft
import caopeirui.LFToE_simulation.find_traffic_cluster as ftc
import caopeirui.LFToE_simulation.LP_routing_oldsen_w2 as lrsenw2
import caopeirui.LFToE_simulation.mesh_topology as mt
import ZB.random_topology as zbrt
from ZB.convex_hull import TrafficConvex
import caopeirui.LFToE_simulation.valiant_routing as vr
from utils.base import cosine_similarity
import caopeirui.LFToE_simulation.preprocess as pp
import logging
from utils.log import init_log


init_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/test/result/' + __file__)

# 存储不同 representative traffic 对应 routing 的 类对象
# 后面直接调用，避免重复
LRSENW2_ROUTING_1 = []  # coe = 1.1
LRSENW2_ROUTING_3 = []  # coe = 1.3
LRSENW2_ROUTING_5 = []  # coe = 1.5
LRSENW2_ROUTING_7 = []  # coe = 1.7
LRSENW2_ROUTING_9 = []  # coe = 1.9

def low_frequency_ToE(conf_file, rep_traffic):
    """
    Desc: 整合拓扑工程的分步过程
    """
    pred_traffic = rep_traffic
    # 得到fractional topology, 即pod间连接数的非整数解
    obj_frac_topo = fft.FindFractionalTopology()
    obj_frac_topo.load_init_config(conf_file)

    obj_frac_topo.set_traffic_sequence(pred_traffic)
    u, d_ij = obj_frac_topo.get_fractional_topology()
    # print(d_ij)
    print('scaling factor = ', u)
    # print(d_ij.sum())

    d_ij = obj_frac_topo.again_ToE(1.01 / u)
    d_ij = obj_frac_topo.to_integer_topo(d_ij)
    # d_ij = obj_frac_topo.third_ToE(1 / u, d_ij)

    return d_ij


def record_lrsenw2_routing(conf_file, x_ij, traffic, coe):
    obj_TE = lrsenw2.LinearProgrammingRouting(x_ij)
    obj_TE.load_init_config(conf_file)
    w_routing, max_link_utilization = obj_TE.traffic_engineering_grb(traffic)
    max_w = obj_TE.again_routing(traffic, max_link_utilization)
    obj_TE.third_routing(traffic, max_link_utilization, max_w * coe)
    return obj_TE


def test_LP_TE(conf_file, x_ij, rep_traffic, future_traffic, i):
    # 当前矩阵与哪个代表性矩阵相似度高，就用哪个做routing
    cos_sim = 0

    opt_index = None
    for t in range(len(rep_traffic)):
        tmp_cos_sim = cosine_similarity(rep_traffic[t].flatten().tolist(), future_traffic[i].flatten().tolist())
        if tmp_cos_sim > cos_sim:
            cos_sim = tmp_cos_sim
            opt_index = t


    lrsenw2_lu_1, lrsenw2_mlu_1, lrsenw2_alu_1 = LRSENW2_ROUTING_1[opt_index].calc_link_utilization(future_traffic[i + 1])
    lrsenw2_ahc_1 = LRSENW2_ROUTING_1[opt_index].ave_hop_count(future_traffic[i + 1])

    lrsenw2_lu_3, lrsenw2_mlu_3, lrsenw2_alu_3 = LRSENW2_ROUTING_3[opt_index].calc_link_utilization(future_traffic[i + 1])
    lrsenw2_ahc_3 = LRSENW2_ROUTING_3[opt_index].ave_hop_count(future_traffic[i + 1])

    lrsenw2_lu_5, lrsenw2_mlu_5, lrsenw2_alu_5 = LRSENW2_ROUTING_5[opt_index].calc_link_utilization(future_traffic[i + 1])
    lrsenw2_ahc_5 = LRSENW2_ROUTING_5[opt_index].ave_hop_count(future_traffic[i + 1])

    lrsenw2_lu_7, lrsenw2_mlu_7, lrsenw2_alu_7 = LRSENW2_ROUTING_7[opt_index].calc_link_utilization(future_traffic[i + 1])
    lrsenw2_ahc_7 = LRSENW2_ROUTING_7[opt_index].ave_hop_count(future_traffic[i + 1])

    lrsenw2_lu_9, lrsenw2_mlu_9, lrsenw2_alu_9 = LRSENW2_ROUTING_9[opt_index].calc_link_utilization(future_traffic[i + 1])
    lrsenw2_ahc_9 = LRSENW2_ROUTING_9[opt_index].ave_hop_count(future_traffic[i + 1])

    res_str = (
        f'{lrsenw2_mlu_1},{lrsenw2_alu_1},{lrsenw2_ahc_1},'
        f'{lrsenw2_mlu_3},{lrsenw2_alu_3},{lrsenw2_ahc_3},'
        f'{lrsenw2_mlu_5},{lrsenw2_alu_5},{lrsenw2_ahc_5},'
        f'{lrsenw2_mlu_7},{lrsenw2_alu_7},{lrsenw2_ahc_7},'
        f'{lrsenw2_mlu_9},{lrsenw2_alu_9},{lrsenw2_ahc_9},'
    )
    
    logging.critical(res_str)


def compare_result(start_index):
    # 每个测试程序都利用统一接口获取输入
    # scale = True 引入参数rou，代表性矩阵做scale数值拓展
    rep_traffic, future_traffic = pp.get_traffic_data(start_index, scale = True)

    #x_ij = low_frequency_ToE(pp.CONF_FILE, rep_traffic)
    #print(x_ij)
    #print(x_ij.sum())
    #print(x_ij.sum(axis=0))
    #print(x_ij.sum(axis=1))

    obj = mt.MeshTopology()
    obj.load_init_config(pp.CONF_FILE)
    x_ij = obj.get_connects_matrix()
    print(x_ij)
    print(x_ij.sum())
    print(x_ij.sum(axis=0))
    print(x_ij.sum(axis=1))

    # 每两周为周期预测、比较完后，刷新全局ROUTING存储变量。
    global LRSENW2_ROUTING_1
    global LRSENW2_ROUTING_3
    global LRSENW2_ROUTING_5
    global LRSENW2_ROUTING_7
    global LRSENW2_ROUTING_9
    LRSENW2_ROUTING_1 = []
    LRSENW2_ROUTING_3 = []
    LRSENW2_ROUTING_5 = []
    LRSENW2_ROUTING_7 = []
    LRSENW2_ROUTING_9 = []

    print('compare_result start_index: ' + str(start_index))
    print(LRSENW2_ROUTING_9)

    # 计算各自topo对应不同 representative traffic 的 routing，存储到全局变量
    for i in range(len(rep_traffic)):
        lrsenw2_obj_1 = record_lrsenw2_routing(pp.CONF_FILE, x_ij, rep_traffic[i], 1.1)
        lrsenw2_obj_3 = record_lrsenw2_routing(pp.CONF_FILE, x_ij, rep_traffic[i], 1.3)
        lrsenw2_obj_5 = record_lrsenw2_routing(pp.CONF_FILE, x_ij, rep_traffic[i], 1.5)
        lrsenw2_obj_7 = record_lrsenw2_routing(pp.CONF_FILE, x_ij, rep_traffic[i], 1.7)
        lrsenw2_obj_9 = record_lrsenw2_routing(pp.CONF_FILE, x_ij, rep_traffic[i], 1.9)

        LRSENW2_ROUTING_1.append(lrsenw2_obj_1)
        LRSENW2_ROUTING_3.append(lrsenw2_obj_3)
        LRSENW2_ROUTING_5.append(lrsenw2_obj_5)
        LRSENW2_ROUTING_7.append(lrsenw2_obj_7)
        LRSENW2_ROUTING_9.append(lrsenw2_obj_9)

    size = len(future_traffic) - 1
    for i in range(size):
        test_LP_TE(pp.CONF_FILE, x_ij, rep_traffic, future_traffic, i)


if __name__ == "__main__":
    print('Desc: 两次ToE, single-traffic TE')
    print(f'读取{pp.CONF_FILE}')
    logging.critical(
        ('lrsenw2_mlu_1,lrsenw2_alu_1,lrsenw2_ahc_1,'
        'lrsenw2_mlu_3,lrsenw2_alu_3,lrsenw2_ahc_3,'
        'lrsenw2_mlu_5,lrsenw2_alu_5,lrsenw2_ahc_5,'
        'lrsenw2_mlu_7,lrsenw2_alu_7,lrsenw2_ahc_7,'
        'lrsenw2_mlu_9,lrsenw2_alu_9,lrsenw2_ahc_9,')
    )
    for i in range(10):
        compare_result(i * 4032)

   