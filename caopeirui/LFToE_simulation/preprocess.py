import numpy as np
from root_const import ROOT_PATH
# import caopeirui.LFToE_simulation.find_traffic_cluster as ftc
import caopeirui.LFToE_simulation.find_traffic_cluster_new as ftc
import caopeirui.LFToE_simulation.find_fractional_topology as fft
from utils.base import cosine_similarity
from utils.base import DcnBase

# 流量采样间隔是5min，故4032代表两周
WINDOW_SIZE = 4032
CONF_FILE = f'{ROOT_PATH}/config/g_config_8.csv'
# CONF_FILE = f'{ROOT_PATH}/config/g_config_19.csv'
TRAFFIC_FILE = f'{ROOT_PATH}/data/g_data/8pod_traffic.npy'
# TRAFFIC_FILE = f'{ROOT_PATH}/data/g_data/19pod_traffic.npy'


def traffic_norm(traffic_array):
    """
    输入是二维 traffic array
    输出是归一化后的二维 traffic array
    """
    conf_obj = DcnBase()
    conf_obj.load_init_config(CONF_FILE)
    # 每个port的bandwidth
    port_bandwidth = conf_obj.get_bandwidth()
    pod_num = conf_obj.get_pods_num()
    # 每个pod的进出的bandwidth，含了所有进端口或者出端口
    # 目前配置比较均匀，所以每个pod进端口数跟出端口数都相等
    pod_bandwidth = port_bandwidth[0][0] * conf_obj._r_egress[0]

    # -------method1 此法相当于没有针对每个pod差异归一化，对19个pod仿真效果不佳
    # 每个pod进出总流量除以端口带宽，全局最大的值作为归一化基准
    egress_traffic = traffic_array.sum(axis = 1) / pod_bandwidth
    ingress_traffic = traffic_array.sum(axis = 0) / pod_bandwidth
    egress_max = egress_traffic.max()
    ingress_max = ingress_traffic.max()
    norm_baseline = max(egress_max, ingress_max)
    return traffic_array / norm_baseline
    # --------
    
    # ------- method2
    # traffic 每个位置的行列和取最大除以端口带宽，作为归一化基准
    # 每个位置的baseline不同
    """egress_traffic = traffic_array.sum(axis = 1) / pod_bandwidth
    ingress_traffic = traffic_array.sum(axis = 0) / pod_bandwidth
    new_traffic = np.zeros((pod_num, pod_num), dtype = float)
    for i in range(pod_num):
        for j in range(pod_num):
            baseline = max(egress_traffic[i], ingress_traffic[j])
            if baseline != 0:
                new_traffic[i][j] = traffic_array[i][j] / baseline
            else:
                new_traffic[i][j] = traffic_array[i][j]

    return new_traffic"""
    # -------


def get_traffic_data(start_index, scale = True, kmeans_num = 4, overprovision = False):
    """
    统一供每个测试程序获取一系列流量矩阵
    从文件获取历史流量矩阵数据
    生成并返回用一周流量序列数据做k-means求出来的代表性矩阵 representative_traffic
    以及WINDOW_SIZE后的矩阵序列 future_traffic
    """
    traffic_seq = get_ori_traffic_seq(TRAFFIC_FILE)
    # 当 WINDOW_SIZE = 4032，表示取出前两周 (5m * 4032 = 14 days)
    one_week_traffic = traffic_seq[start_index : start_index + WINDOW_SIZE]
    future_traffic = traffic_seq[start_index + WINDOW_SIZE : start_index + WINDOW_SIZE * 2]

    # 用k-means找代表性流量矩阵
    obj_km = ftc.TrafficKMeans(k = kmeans_num)
    for a_traffic in one_week_traffic:
        a_traffic = traffic_norm(a_traffic)    # test norm
        obj_km.add_a_traffic(a_traffic)
    representative_traffic = obj_km.get_pred_traffic(overprovision)

    if scale == True:
        representative_traffic = scale_traffic(representative_traffic)

    return representative_traffic, future_traffic

def get_traffic(**kwargs):
    #params
    start_index = kwargs['start_index']
    scale = kwargs['scale']
    kmeans_num = kwargs['kmeans_num']
    overprovision = kwargs['overprovision']

    traffic_seq = get_ori_traffic_seq(TRAFFIC_FILE)
    # 当 WINDOW_SIZE = 4032，表示取出前两周 (5m * 4032 = 14 days)
    now_traffic = traffic_seq[start_index : start_index + WINDOW_SIZE]
    future_traffic = traffic_seq[start_index + WINDOW_SIZE : start_index + WINDOW_SIZE * 2]

    # 用k-means找代表性流量矩阵
    obj_km = ftc.TrafficKMeans(k = kmeans_num)
    for a_traffic in now_traffic:
        a_traffic = traffic_norm(a_traffic)    # test norm
        obj_km.add_a_traffic(a_traffic)
    representative_traffic = obj_km.get_pred_traffic(overprovision)

    if scale == True:
        representative_traffic = scale_traffic(representative_traffic)

    return now_traffic, representative_traffic, future_traffic

def get_ori_traffic_seq(file_name):
    """从某个历史流量矩阵文件中得到矩阵序列
    """
    traffic_history = np.load(file_name, allow_pickle = True)[0]
    
    traffic_seq = []
    for timestamp, traffic in traffic_history.items():
        # pod自己到自己的流量置0
        pod_num = traffic.shape[0]
        for i in range(pod_num):
            traffic[i][i] = 0
        traffic_seq.append(traffic / 300 * 8 / 1000000000)

    return np.array(traffic_seq)


def scale_traffic(traffic_seq):
    obj_frac_topo = fft.FindFractionalTopology()
    obj_frac_topo.load_init_config(CONF_FILE)
    scale_traffic_seq = []
    for i in range(len(traffic_seq)):
        obj_frac_topo.set_traffic_sequence([traffic_seq[i]])
        alpha, d_ij = obj_frac_topo.get_fractional_topology()
        scale_traffic_seq.append(alpha * traffic_seq[i])
        print(i, alpha)
        # if alpha == 0:
        #     print(traffic_seq[i])

    scale_traffic_seq = np.array(scale_traffic_seq)
    return scale_traffic_seq


def get_svd_SV(traffic_seq):
    flatten_traffic = []
    for traffic in traffic_seq:
        flatten_traffic.append(traffic.flatten())
    
    flatten_traffic = np.array(flatten_traffic)
    U, S, V = np.linalg.svd(flatten_traffic)
    result = []
    for i in range(20):
        result.append(S[i]*V[i])
    return result


def get_place_var(traffic_seq):
    flatten_traffic = []
    for traffic in traffic_seq:
        tmp = traffic.flatten()
        flatten_traffic.append(tmp)
    
    return np.var(flatten_traffic, axis = 0)

def get_place_cov(traffic_seq):
    flatten_traffic = []
    for traffic in traffic_seq:
        flatten_traffic.append(traffic.flatten())
    
    return np.cov(flatten_traffic, rowvar=False)


def get_place_mean(traffic_seq):
    flatten_traffic = []
    for traffic in traffic_seq:
        tmp = traffic.flatten()
        flatten_traffic.append(tmp)
    
    return np.mean(flatten_traffic, axis = 0)


def get_place_max(traffic_seq):
    flatten_traffic = []
    for traffic in traffic_seq:
        flatten_traffic.append(traffic.flatten())
    
    return np.max(flatten_traffic, axis = 0)


def get_place_min(traffic_seq):
    flatten_traffic = []
    for traffic in traffic_seq:
        flatten_traffic.append(traffic.flatten())
    
    return np.min(flatten_traffic, axis = 0)


def standardize(data, method = 'norm'):
    if method == 'norm':
        tmp = np.array(data)
        return (tmp - tmp.min()) / (tmp.max() - tmp.min())
    elif method == 'zscore':
        tmp = np.array(data)
        return (tmp - tmp.mean()) / (tmp.std())


if __name__ == "__main__":
    representative_traffic, future_traffic = get_traffic_data(0)
    # print(get_place_var(representative_traffic))
    traffic_seq = get_ori_traffic_seq(TRAFFIC_FILE)
    np.set_printoptions(precision=2, suppress=True, threshold=np.inf)
    for i in range(0, len(traffic_seq), 4032):
        print(f'max[{i} : {i + 4032}]:')
        print(get_place_max(traffic_seq[i : i + 4032]).reshape(traffic_seq[0].shape))
        print(f'mean[{i} : {i + 4032}]:')
        print(get_place_mean(traffic_seq[i : i + 4032]).reshape(traffic_seq[0].shape))
        
    # get_svd_SV(traffic_seq[0:4032])
    # print(cosine_similarity(representative_traffic[0].flatten().tolist(), representative_traffic[1].flatten().tolist()))
