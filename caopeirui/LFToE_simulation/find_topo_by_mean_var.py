from utils.base import DcnBase
from utils.base import data_norm
from root_const import ROOT_PATH
import numpy as np
import gurobipy as grb
import math
import caopeirui.LFToE_simulation.preprocess as pp
from utils.base import cosine_similarity
from utils.base import data_norm


def diagonal_to_zero(flatten_traffic):
    # 拉成一行的traffic对应原对角线位置置0
    pods_num = int(math.sqrt(len(flatten_traffic)))
    flatten_size = len(flatten_traffic)
    for i in range(flatten_size):
        for k in range(pods_num):
            if pods_num * k + k == i:
                flatten_traffic[i] = 0
    return flatten_traffic


class FindTopologyByMeanVar(DcnBase):
    def get_topology(self, start_index):
        traffic_seq = pp.get_ori_traffic_seq(pp.TRAFFIC_FILE)
        one_week_traffic = traffic_seq[start_index : start_index + pp.WINDOW_SIZE]
        # TODO 改回来灵活用mean还是max
        before_mean = pp.get_place_mean(one_week_traffic)
        traffic_mean = before_mean.reshape(self._pods_num, self._pods_num)
        # 对角线置0，忽视pods内部流量
        for i in range(self._pods_num):
            traffic_mean[i][i] = 0

        topo_links = np.ones((self._pods_num, self._pods_num), dtype = int)
        for i in range(self._pods_num):
            topo_links[i][i] = 0
        # 不断增加pod间link数，直到刚好覆盖mean且不超bound
        
        # 若links数还没有超界，继续迭代增加links数，直到满足所有mean情况或没法增加。
        while self.satisfy_physical_bound(topo_links) == True:
            capacities = topo_links * self._bandwidth
            # TODO: 如果links总数不多，可以针对mean或var排序后，再依次link+1
            
            # 想要让让两个np.array独立，必须用 copy 或 deepcopy。
            last_topo_links = topo_links.copy()
            for i in range(self._pods_num):
                for j in range(self._pods_num):
                    if traffic_mean[i][j] > capacities[i][j] and i != j:
                        # 单条link能加的情况才加
                        topo_links[i][j] += 1
                        if self.satisfy_physical_bound(topo_links) == False:
                            # 加之后不满足条件，则减回去
                            topo_links[i][j] -= 1
                        capacities = topo_links * self._bandwidth

            # 若遍历一遍topo，不能再增加links了，就算还有不满足的capacity，分配也需结束。
            have_change = False
            for i in range(self._pods_num):
                for j in range(self._pods_num):
                    if topo_links[i][j] != last_topo_links[i][j] and i != j:
                        have_change = True
            if have_change == False:
                break
        # print(topo_links)
        # print(topo_links.sum())

        # 到这里利用mean统计量约束构建完成了最小topo
        # 下面利用variance统计量填充剩下链路，TODO: 如何利用？
        # 以更好地应对uncertainty

        # print(cosine_similarity(future_traffic[10].flatten().tolist(), topo_links.flatten().tolist()))
        before_var = pp.get_place_var(one_week_traffic)
        traffic_var = before_var.reshape(self._pods_num, self._pods_num)
        for i in range(self._pods_num):
            traffic_var[i][i] = 0

        while self.satisfy_physical_bound(topo_links) == True:
            # 想要让让两个np.array独立，必须用 copy 或 deepcopy。
            last_topo_links = topo_links.copy()
            for i in range(self._pods_num):
                for j in range(self._pods_num):
                    # TODO: 测试，如果遇到link=1位置不加，本身流量不多，或者变成2后，不再加
                    # 单条link能加的情况才加
                    if i != j:
                        topo_links[i][j] += 1
                        if self.satisfy_physical_bound(topo_links) == False:
                            # 加之后不满足条件，则减回去
                            topo_links[i][j] -= 1            
            have_change = False
            for i in range(self._pods_num):
                for j in range(self._pods_num):
                    if topo_links[i][j] != last_topo_links[i][j] and i != j:
                        have_change = True
            if have_change == False:
                break
        # print(cosine_similarity(future_traffic[10].flatten().tolist(), topo_links.flatten().tolist()))
        return topo_links


if __name__ == "__main__":
    obj = FindTopologyByMeanVar()
    obj.load_init_config(f'{ROOT_PATH}/config/g_config_8.csv')
    obj.get_topology(8064)

    