import pandas as pd
import numpy as np
import math
from root_const import ROOT_PATH
import gurobipy as grb
import caopeirui.LFToE_simulation.preprocess as pp
import caopeirui.LFToE_simulation.mesh_topology as mt
import logging
from utils.log import init_log
def mesh_ToE(conf_file):
    obj = mt.MeshTopology()
    obj.load_init_config(conf_file)
    d_ij = obj.get_connects_matrix()
    return d_ij

if __name__ == "__main__":
    d_ij = mesh_ToE(pp.CONF_FILE)
    traffic_seq = pp.get_ori_traffic_seq(pp.TRAFFIC_FILE)
    import caopeirui.LFToE_simulation.LP_routing_oldsen_w2 as lr
    net = lr.LinearProgrammingRouting(d_ij)
    net.load_init_config(pp.CONF_FILE)

    init_log(f'{ROOT_PATH}/caopeirui/LFToE_simulation/sensitive/result' + 'findDominate')
    logging.critical('nmlu')
    length = len(traffic_seq)
    for i in range(length):
        net.routing(traffic_seq[i])
        _, nmlu, _ = net.calc_normal_link_utilization(traffic_seq[i])
        logging.critical(f'{nmlu}')
        if i % 100 == 0:
            print('\r' + f'{i}/{length}', end='', flush=True)

