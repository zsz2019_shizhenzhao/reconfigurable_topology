import pandas as pd
import numpy as np
import math

import gurobipy as grb
import caopeirui.LFToE_simulation.preprocess as pp

import caopeirui.LFToE_simulation.find_fractional_topology as fft
import caopeirui.LFToE_simulation.mesh_topology as mt

class LpModule:
    module = None
    changed_traffic = None

class FindTrafficSensitive:
    routing = None
    traffic = None
    unsensitve_topo = None
    sensitive_topo = None

    def load_init_config(self, config_file):
        """
        Desc: 从csv配置文件中读取各属性值，并变换成后续模块需要用到的数据
              转换后的数据，无论几维，统一化为np.ndarray类型
        """
        pd_config = pd.read_csv(config_file)
        # TODO: 错误或无效配置输入处理
        self._r_ingress = np.array(pd_config['r_ingress'])
        self._r_egress = np.array(pd_config['r_egress'])
        self._port_bandwidth = np.array(pd_config['port_bandwidth'])
        self._pods_num = len(self._r_egress)

        # 将读取的配置文件内容，变换为后续模块需用到的数据形式
        # 第i到j个pod的带宽，两者取其小
        all_bandwith = []
        for bi in range(self._pods_num):
            tmp = []
            for bj in range(self._pods_num):
                tmp.append(min(self._port_bandwidth[bi], self._port_bandwidth[bj]))
            all_bandwith.append(tmp)
        self._bandwidth = np.array(all_bandwith)

    def __init__(self, config_file, routing_class, traffic, unsensitve_topo, sensitve_topo):
        self.load_init_config(config_file)
        self.routing = routing_class
        self.traffic = traffic
        self.unsensitve_topo = unsensitve_topo
        self.sensitive_topo = sensitve_topo

        self.unsensitve_TE = routing_class(unsensitve_topo)
        self.unsensitve_TE.load_init_config(config_file)
        
        self.sensitve_TE = routing_class(sensitve_topo)
        self.sensitve_TE.load_init_config(config_file)

        self.unsensitve_TE.routing(traffic)
        self.sensitve_TE.routing(traffic)

    def _set_constraint(self):
        unsen_cap = self.unsensitve_TE.get_capacities()
        sen_cap = self.sensitve_TE.get_capacities()

        unsen_w = self.unsensitve_TE._w_routing
        sen_w = self.sensitve_TE._w_routing

        pods_num = self._pods_num
        traffic = self.traffic

        m = grb.Model("find_sensitive_grb")
        m.Params.LogToConsole = 0

        unsen_mlu = m.addVar(lb = 0, vtype = grb.GRB.CONTINUOUS, name='unsen_mlu')
        sen_mlu = m.addVar(lb = 0, vtype = grb.GRB.CONTINUOUS, name='sen_mlu')


        changed_traffic = m.addVars(pods_num, pods_num, lb = 0, vtype = grb.GRB.CONTINUOUS, name = 't')

        # un_w * t <= un_mlu
        binary_one = m.addVars(pods_num, pods_num,lb = 0, ub = 1, vtype = grb.GRB.BINARY, name = 'b1')

        m.addConstrs(
                grb.quicksum(
                    (traffic[k - 1][j - 1] + changed_traffic[k - 1,j - 1]) * unsen_w[f'w_{k}_{i}_{j}']
                    + (traffic[i - 1][k - 1] + changed_traffic[i - 1,k - 1]) * unsen_w[f'w_{i}_{j}_{k}']
                    if k != 0 else (traffic[i - 1][j - 1] + changed_traffic[i - 1,j - 1]) * unsen_w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) + 100000 * (1 - binary_one[i - 1, j - 1]) >= unsen_mlu * unsen_cap[i - 1][j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j
            )
        

        m.addConstrs(
                grb.quicksum(
                    (traffic[k - 1][j - 1] + changed_traffic[k - 1,j - 1]) * unsen_w[f'w_{k}_{i}_{j}']
                    + (traffic[i - 1][k - 1] + changed_traffic[i - 1,k - 1]) * unsen_w[f'w_{i}_{j}_{k}']
                    if k != 0 else (traffic[i - 1][j - 1] + changed_traffic[i - 1,j - 1]) * unsen_w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) <= unsen_mlu * unsen_cap[i - 1][j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j
            )
        # m.setObjective(unsen_mlu, grb.GRB.MINIMIZE)
        # m.optimize()
        # print(m.objVal)
        # return
        
        m.addConstr(
            grb.quicksum(
                binary_one[i - 1,j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j
            ) >= 1
        )


        # s_w * t <= s_mlu
        binary_two = m.addVars(pods_num, pods_num, lb = 0, ub = 1, vtype = grb.GRB.BINARY, name = 'b2')
        m.addConstrs(
                grb.quicksum(
                    (traffic[k - 1][j - 1] + changed_traffic[k - 1,j - 1]) * sen_w[f'w_{k}_{i}_{j}']
                    + (traffic[i - 1][k - 1] + changed_traffic[i - 1,k - 1]) * sen_w[f'w_{i}_{j}_{k}']
                    if k != 0 else (traffic[i - 1][j - 1] + changed_traffic[i - 1,j - 1]) * sen_w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) + 100000 * (1 - binary_two[i - 1, j - 1]) >= sen_mlu * sen_cap[i - 1][j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j
            )
        m.addConstrs(
                grb.quicksum(
                    (traffic[k - 1][j - 1] + changed_traffic[k - 1,j - 1]) * sen_w[f'w_{k}_{i}_{j}']
                    + (traffic[i - 1][k - 1] + changed_traffic[i - 1,k - 1]) * sen_w[f'w_{i}_{j}_{k}']
                    if k != 0 else (traffic[i - 1][j - 1] + changed_traffic[i - 1,j - 1]) * sen_w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) <= sen_mlu * sen_cap[i - 1][j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j
            )
        m.addConstr(
            grb.quicksum(
                binary_two[i - 1,j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j
            ) >= 1
        )
        
        m.addConstr(unsen_mlu * 1.05 <= sen_mlu, "compare")

    #     mymodule = LpModule()
    #     mymodule.module = m
    #     mymodule.changed_traffic = changed_traffic

    #     return mymodule

    # def _set_object_maximum_min_changed(self, lp_module):
    #     m = lp_module.module
    #     changed_traffic = lp_module.changed_traffic

        traffic = self.traffic
        pods_num = self._pods_num

        delta = m.addVar(lb = 0, vtype = grb.GRB.CONTINUOUS, name='delta')

        m.addConstr(
            grb.quicksum(
                changed_traffic[i - 1,j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
            ) <= delta
        )

        # m.addConstrs(
        #     delta >= changed_traffic[i - 1,j - 1]
        #     for i in range(1, pods_num + 1)
        #     for j in range(1, pods_num + 1)
        #     if i != j
        # )

        m.setObjective(delta, grb.GRB.MINIMIZE)
        m.optimize()

        if m.status == grb.GRB.Status.OPTIMAL:
            # print(unsen_mlu.getAttr('X'))
            # print(sen_mlu.getAttr('X'))
            solution = m.getAttr('X', binary_one)
            for i in range(pods_num):
                for j in range(pods_num):
                    if i != j and solution[i,j] > 0:
                        print('u', i, j)
            solution = m.getAttr('X', binary_two)
            for i in range(pods_num):
                for j in range(pods_num):
                    if i != j and solution[i,j] > 0:
                        print('s', i, j)

            return m.objVal
        else:
            print('No solution')
            return None

    def find_sensitive(self):
        _, unsen_mlu, unsen_alu = self.unsensitve_TE.calc_link_utilization(self.traffic)
        _, sen_mlu, sen_alu = self.sensitve_TE.calc_link_utilization(self.traffic)


        _, unsen_norm_mlu, _ = self.unsensitve_TE.calc_normal_link_utilization(self.traffic)
        _, sen_norm_mlu, _ = self.sensitve_TE.calc_normal_link_utilization(self.traffic)
        print(unsen_mlu , sen_mlu)
        print(unsen_alu, sen_alu)
        print(unsen_norm_mlu, sen_norm_mlu)
        if unsen_mlu * 1.000001 < sen_mlu:
            print('the ToE is not good!')
            return
        # # return
        # mymodule = self._set_constraint()
        # res = self._set_object_maximum_min_changed(mymodule)
        res = self._set_constraint()
        return res



def low_frequency_ToE(conf_file, rep_traffic):
    """
    Desc: 整合拓扑工程的分步过程
    """
    pred_traffic = rep_traffic
    # 得到fractional topology, 即pod间连接数的非整数解
    obj_frac_topo = fft.FindFractionalTopology()
    obj_frac_topo.load_init_config(conf_file)

    obj_frac_topo.add_a_traffic(pred_traffic)
    u, d_ij = obj_frac_topo.get_fractional_topology()
    # print(d_ij)
    print('scaling factor = ', u)
    # print(d_ij.sum())

    d_ij = obj_frac_topo.again_ToE(1 / u)
    d_ij = obj_frac_topo.to_integer_topo(d_ij)

    return d_ij

def mesh_ToE(conf_file):
    obj = mt.MeshTopology()
    obj.load_init_config(conf_file)
    d_ij = obj.get_connects_matrix()

    return d_ij


if __name__ == "__main__":
    traffic_seq = pp.get_ori_traffic_seq(pp.TRAFFIC_FILE)
    traffic = traffic_seq[10]
    unsen_topo = mesh_ToE(pp.CONF_FILE)
    sen_topo = low_frequency_ToE(pp.CONF_FILE, traffic)
    print(traffic)
    import caopeirui.LFToE_simulation.LP_routing_oldsen_w2 as lr
    fts = FindTrafficSensitive(pp.CONF_FILE, lr.LinearProgrammingRouting, traffic, unsen_topo, sen_topo)
    res = fts.find_sensitive()
    print(res)
