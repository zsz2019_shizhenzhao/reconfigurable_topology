# 项目简介
低频动态拓扑切换切换的仿真实现，参考论文《The Tortoise and the Hare:Handling Fast Data Center Traﬃc DynamicsUsing Low Frequency Topology Engineering》。

# 工程文件描述
```
|____find_traffic_cluster.py      继承了DcnBase类，实现根据历史流量矩阵预测未来流量矩阵
|____find_fractional_topology.py  继承DcnBase类，实现根据未来流量矩阵，解算出pod间连接数总数（非整数解）。
|____map_to_ocs.py                继承DcnBase类，实现将pod间连接总数分解映射到OCS中连接总数（整数解）。
|____main.py                      整合拓扑工程分步实现过程
```