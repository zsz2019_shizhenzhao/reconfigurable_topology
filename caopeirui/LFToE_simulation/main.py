import numpy as np
from root_const import ROOT_PATH
# import find_int_topo as fft
import find_fractional_topology as fft
import find_traffic_cluster as ftc
import map_to_ocs as mto
import LP_routing as lr
import mesh_topology as mt
import ZB.random_topology as zbrt
from ZB.convex_hull import TrafficConvex
import valiant_routing as vr

def low_frequency_ToE(conf_file):
    """
    Desc: 整合拓扑工程的分步过程
    """
    # 预测未来流量矩阵
    obj_km = ftc.TrafficKMeans(k = 3)
    # obj_hull = TrafficConvex()
    for i in range(8):
        traffic = np.load(f"{ROOT_PATH}/data/data_properties_100/out_pod{i}.npy")
        obj_km.add_a_traffic(traffic)
        # obj_hull.add_a_traffic(traffic)
 
    pred_traffic = obj_km.get_pred_traffic()
    # pred_traffic = obj_hull.get_convex_traffic()[:3]

    # 得到fractional topology, 即pod间连接数的非整数解
    obj_frac_topo = fft.FindFractionalTopology()
    obj_frac_topo.load_init_config(conf_file)
    obj_frac_topo.set_traffic_sequence(pred_traffic)
    u, d_ij = obj_frac_topo.get_fractional_topology()

    d_ij = obj_frac_topo.again_ToE(1 / u)
    # print(d_ij)
    # print(d_ij.sum())
    # print(d_ij.sum(axis = 0))
    # print(d_ij.sum(axis = 1))

    # 在again_ToE中把变量约束直接变为整数就得到了整数解
    # 在假设不用ocs或者只有1个ocs的情况下，直接返回结果
    return d_ij

    # 进一步映射到每个ocs中的连接数整数解
    obj_map = mto.MapFractionalTopoToOcs(
        ocs_num = 5,
        d_ij = d_ij,
    )

    obj_map.load_init_config(conf_file)
    # x_ij = obj_map.topology_engineering('equal')
    x_ij = obj_map.topology_engineering()
    return x_ij


def test_LP_TE(conf_file, x_ij):
    """
    Desc: 根据pods间连接数和带宽，测试LP解routing后的link utilization
    Inputs: 
        - conf_file
        - x_ij(np.ndarray): pods间连接数（二维）
    """
    # 流量工程
    obj_TE = lr.LinearProgrammingRouting(x_ij)
    obj_TE.load_init_config(conf_file)
    traffic_TE = np.load(f"{ROOT_PATH}/data/data_properties_100/out_pod{8}.npy")
    w_routing = obj_TE.traffic_engineering_grb(traffic_TE)
    lu, mlu, alu = obj_TE.calc_link_utilization(traffic_TE)
    print(mlu, alu)

    obj_TE.again_routing(traffic_TE, mlu)  # test

    lu, mlu, alu = obj_TE.calc_link_utilization(traffic_TE)
    print(mlu, alu)
    
    print('针对未来流量矩阵计算出MLU和ALU')
    for i in range(10):
        traffic_calc_lu = np.load(f"{ROOT_PATH}/data/data_properties_100/out_pod{i}.npy")
        lu, mlu, alu = obj_TE.calc_link_utilization(traffic_calc_lu)
        print(mlu, alu)

    # print('用流量矩阵算出routing再计算出MLU和ALU')
    # for i in range(10):
    #     traffic_calc_lu = np.load(f"{ROOT_PATH}/data/data_properties_100/out_pod{i}.npy")
    #     obj_TE.traffic_engineering(traffic_calc_lu)
    #     lu, mlu, alu = obj_TE.calc_link_utilization(traffic_calc_lu)
    #     print(mlu, alu)


def test_valiant_TE(conf_file, x_ij):
    """
    Desc: 根据pods间连接数和带宽，
          测试valiant(最多两跳下，平均routing)解routing后的link utilization
    Inputs: 
        - conf_file
        - x_ij(np.ndarray): pods间连接数（二维）
    """
    # 流量工程
    obj_TE = vr.ValiantRouting(x_ij)
    obj_TE.load_init_config(conf_file)
    traffic_TE = np.load(f"{ROOT_PATH}/data/data_properties_100/out_pod{8}.npy")
    w_routing = obj_TE.traffic_engineering(traffic_TE)
    lu, mlu, alu = obj_TE.calc_link_utilization(traffic_TE)
    print(mlu, alu)
    
    print('针对未来流量矩阵计算出MLU和ALU')
    for i in range(10):
        traffic_calc_lu = np.load(f"{ROOT_PATH}/data/data_properties_100/out_pod{i}.npy")
        lu, mlu, alu = obj_TE.calc_link_utilization(traffic_calc_lu)
        print(mlu, alu)

    # print('用流量矩阵算出routing再计算出MLU和ALU')
    # for i in range(10):
    #     traffic_calc_lu = np.load(f"{ROOT_PATH}/data/data_properties_100/out_pod{i}.npy")
    #     obj_TE.traffic_engineering(traffic_calc_lu)
    #     lu, mlu, alu = obj_TE.calc_link_utilization(traffic_calc_lu)
    #     print(mlu, alu)


if __name__ == "__main__":
    conf_file = f'{ROOT_PATH}/config/pods_config.csv'
    print("-----LFToE and TE-----")
    x_ij = low_frequency_ToE(conf_file)
    print(x_ij)
    print('total links ', x_ij.sum())
    # print(x_ij.sum(axis = 0))
    # print(x_ij.sum(axis = 1))
    test_LP_TE(conf_file, x_ij)
    # test_valiant_TE(conf_file, x_ij)

    print("-----mesh ToE and TE-----")
    obj = mt.MeshTopology()
    obj.load_init_config(f'{ROOT_PATH}/config/pods_config.csv')
    x_ij = obj.get_connects_matrix()
    print(x_ij)
    print('total links ', x_ij.sum())
    test_LP_TE(conf_file, x_ij)
    # test_valiant_TE(conf_file, x_ij)

    print("-----random ToE and TE-----")
    obj = zbrt.RandomTopology()
    obj.load_init_config(f'{ROOT_PATH}/config/pods_config.csv')
    x_ij = obj.get_connects_matrix(1)
    print(x_ij)
    print('total links ', x_ij.sum())
    test_LP_TE(conf_file, x_ij)
    # test_valiant_TE(conf_file, x_ij)