import numpy as np
from sklearn.cluster import KMeans
from utils.base import DcnBase

# 用来记录用cluster中小流量traffic的位置
G_SMALL_TRAFFIC_PLACE = []

class TrafficKMeans(DcnBase):
    """
    k-means cluster algorithm for traffic matrix.
    If model input(data matrix) is m*n dimensional matrix,
    the result of k-means cluster will be k rows, every of which represents a predicting result.
    Then every predicting traffic is reshaped to 2-dimensional matrix.
    Return results(3-dimensional) of including predicting traffic.
    """
    _k = 1

    def __init__(self, k = 1):
        self._k = k


    def get_pred_traffic(self, overprovision = False):
        data_mat = []
        record_shape = self._traffic_sequence[0].shape
        pods_num = record_shape[0]

        global G_SMALL_TRAFFIC_PLACE
        G_SMALL_TRAFFIC_PLACE = np.zeros(record_shape, dtype=int)

        for a_traffic in self._traffic_sequence:
            traffic_to_one_dimension = a_traffic.reshape(1, np.prod(np.shape(a_traffic)))[0]
            data_mat.append(traffic_to_one_dimension)

        data_mat = np.array(data_mat)
        km = KMeans(n_clusters = self._k)
        km.fit(data_mat)
        centers = km.cluster_centers_ # 质心
        
        pred_traffic = []
        for i in range(self._k):
            # 求选取出的traffic的routing，算 sensitivity
            center = centers[i]
            if overprovision:
                sensitivity_diff = self.get_topo_sensitivity(center.reshape(record_shape))

                # 获取聚类后每个类别的数据
                current_cluster = data_mat[km.labels_ == i]
                cluster_max = current_cluster.max(axis = 0)

                sensitivity_diff = sensitivity_diff.reshape(len(center))
                # 增加 sensitivity 超过阈值的位置的流量
                sorted_diff = sorted(sensitivity_diff)
                threshold = sorted_diff[int(len(center) * 0)]    # 未来是否做成参数
                print("diff: ", sensitivity_diff)

                counter = 0
                for j in range(len(center)):
                    if sensitivity_diff[j] < threshold:
                        counter = counter + 1
                        center[j] = cluster_max[j]
                        # 记录修改过traffic的位置
                        G_SMALL_TRAFFIC_PLACE[j // pods_num][j % pods_num] = 1
                print("Number of entries that need to be overprovisioned: ", counter)

            # previous code
            center_np = np.asarray(center)
            negative_values = center_np < 0.0001  # Small values could make the traffic matrix unsolvable.
            center_np[negative_values] = 0.0001 
            tmp = np.array(center_np).reshape(record_shape)
            pred_traffic.append(tmp)
        return pred_traffic


    def get_topo_sensitivity(self, rep_traffic):
        # test begin
        import caopeirui.LFToE_simulation.mesh_topology as mt
        import caopeirui.LFToE_simulation.preprocess as pp
        obj = mt.MeshTopology()
        obj.load_init_config(pp.CONF_FILE)
        d_ij_mesh = obj.get_connects_matrix()
        # test end

        import caopeirui.LFToE_simulation.find_fractional_topology as fft
        # import caopeirui.LFToE_simulation.preprocess as pp
        obj_frac_topo = fft.FindFractionalTopology()
        obj_frac_topo.load_init_config(pp.CONF_FILE)        
        obj_frac_topo.set_traffic_sequence([rep_traffic])
        _, d_ij = obj_frac_topo.get_fractional_topology()

        # 单个traffic来解again ToE不会出现逆直觉的topo，解出来就是近似traffic的
        # d_ij = obj_frac_topo.again_ToE(1.01 / alpha)
        # 此处仅为了求后面的sensitivity，故不需要转成整数解了
        # d_ij = obj_frac_topo.to_integer_topo(d_ij)

        import caopeirui.LFToE_simulation.LP_routing as lr
        obj_TE_mesh = lr.LinearProgrammingRouting(d_ij_mesh)
        obj_TE_mesh.load_init_config(pp.CONF_FILE)
        w_routing_mesh, _ = obj_TE_mesh.traffic_engineering_grb(rep_traffic)

        obj_TE = lr.LinearProgrammingRouting(d_ij)
        obj_TE.load_init_config(pp.CONF_FILE)
        w_routing, _ = obj_TE.traffic_engineering_grb(rep_traffic)
        # obj_TE.again_routing(rep_traffic, max_link_utilization)
        # w_routing = obj_TE._w_routing

        capacities_mesh = obj_TE_mesh.get_capacities()
        capacities = obj_TE.get_capacities()
        pods_num = obj_TE.get_pods_num()
        # 计算每个位置的 sensitivity
        sensitivity_mesh = np.zeros((pods_num, pods_num), dtype = float)
        sensitivity = np.zeros((pods_num, pods_num), dtype = float)
        for i in range(1, pods_num + 1):
            for j in range(1, pods_num + 1):
                if i != j:
                    sensitivity[i - 1][j - 1] = 0
                    sensitivity_mesh[i - 1][j - 1] = 0
                    for k in range(0, pods_num + 1):
                        if k != i and k != j:
                            if k == 0:
                                if w_routing[f'w_{i}_{0}_{j}'] > 0:
                                    sen = w_routing[f'w_{i}_{0}_{j}'] / capacities[i - 1][j - 1]
                                else:
                                    sen = 0
                                if w_routing_mesh[f'w_{i}_{0}_{j}'] > 0:
                                    sen_mesh = w_routing_mesh[f'w_{i}_{0}_{j}'] / capacities_mesh[i - 1][j - 1]
                                else:
                                    sen_mesh = 0
                            else:
                                if w_routing[f'w_{i}_{k}_{j}'] > 0:
                                    sen = max(
                                        w_routing[f'w_{i}_{k}_{j}'] / capacities[i - 1][k - 1],
                                        w_routing[f'w_{i}_{k}_{j}'] / capacities[k - 1][j - 1]
                                    )
                                else:
                                    sen = 0
                                if w_routing_mesh[f'w_{i}_{k}_{j}'] > 0:
                                    sen_mesh = max(
                                        w_routing_mesh[f'w_{i}_{k}_{j}'] / capacities_mesh[i - 1][k - 1],
                                        w_routing_mesh[f'w_{i}_{k}_{j}'] / capacities_mesh[k - 1][j - 1]
                                    )
                                else:
                                    sen_mesh = 0
                            if sen > sensitivity[i - 1][j - 1]:
                                sensitivity[i - 1][j - 1] = sen
                            if sen_mesh > sensitivity_mesh[i - 1][j - 1]:
                                sensitivity_mesh[i - 1][j - 1] = sen_mesh

        return sensitivity_mesh - sensitivity
        # from utils.base import data_norm
        # 对 sensitivity 归一化，这样才能统一使用 threshold
        # sensitivity = data_norm(sensitivity)

        # return sensitivity


if __name__ == "__main__":
    # ----test1
    # obj = TrafficKMeans(k = 3)

    # obj.add_a_traffic(np.random.rand(5, 4) * 100)
    # obj.add_a_traffic(np.random.rand(5, 4) * 100)
    # obj.add_a_traffic(np.random.rand(5, 4) * 100)
    # res = obj.get_pred_traffic()
    # print(res)
    # exit()
    # ----test2
    
    obj = TrafficKMeans(k = 4)
    for i in range(10):
        traffic = np.load(f"../../data/data_properties_100/out_pod{i}.npy")
        obj.add_a_traffic(traffic)
        # print(traffic)
    res = obj.get_pred_traffic()
    print(res)
