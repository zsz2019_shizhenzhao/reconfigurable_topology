from ortools.graph import pywrapgraph
import numpy as np
from utils.base import DcnBase


class MapFractionalTopoToOcs(DcnBase):
    """
    Based on fractional topology results(d_ij) of fore steps,
    map onto near-optimal OCS's links 
    """
    # Fractional topology; 
    # d_ij denotes the number of s_i egress links connected to ingress links of s_j
    _d_ij = []

    # The number of OCSs
    _ocs_num = 0

    # Number of physical egress links connecting s_i to o_k , 
    # and number of physical ingress links of s_j to o_k , respectively
    # Both _h_ingress and _h_egress are saved as in tow-dimension list,
    # so k responses every row data.
    _h_ingress = []
    _h_egress = []

    _x_solution = [[]]

    def __init__(self, ocs_num, d_ij):
        # TODO: exception handling. eg:  raise ValueError('xxx')
        # TODO: the setting function of these parameters  
        self._ocs_num = ocs_num
        self._d_ij = d_ij


    def load_init_config(self, config_file):
        super().load_init_config(config_file)
        for _ in range(self._ocs_num):
            ingress_links_num =  [0]
            egress_links_num = [0]
            for j in range(self._pods_num):
                ingress_links_num.append(int(self._r_ingress[j] // self._ocs_num))    
                egress_links_num.append(int(self._r_egress[j] // self._ocs_num))
            self._h_ingress.append(ingress_links_num)
            self._h_egress.append(egress_links_num)


    def fill_reamin_connects_equal(self, x_ij, r_egress, r_ingress):
        """平均分配连接剩余空闲链路
        Args:
            - x_ij 原本pods间连接数矩阵
            - r_egress
            - r_ingress
        Returns:
            - new_x_ij
        """
        new_x_ij = np.zeros((self._pods_num, self._pods_num), dtype=np.int)

        # 该方法只能保证每个pods出去链路数满足约束
        for i in range(len(x_ij)):
            delta = (r_egress[i] - sum(x_ij[i])) // self._pods_num
            for j in range(len(x_ij)):
                if i != j:
                    new_x_ij[i][j] = x_ij[i][j] + delta

        return new_x_ij


    def fill_reamin_connects_random(self, x_ij, r_egress, r_ingress):
        """随机分配连接剩余空闲链路
        Args:
            - x_ij 原本pods间连接数矩阵
            - r_egress
            - r_ingress
        Returns:
            - new_x_ij
        """
        new_x_ij = np.zeros((self._pods_num, self._pods_num), dtype=np.int)
        x_ij = np.array(x_ij)
        # x_ij 连接矩阵按每行求和得到每个pods已连接的egress数
        x_egress = x_ij.sum(axis = 1)
        # x_ij 连接矩阵按每列求和得到每个pods已连接的ingress数
        x_ingress = x_ij.sum(axis = 0)

        x_egress_remain = self._r_egress - x_egress 
        x_ingress_remain = self._r_ingress - x_ingress
        print(x_egress_remain)
        print(x_ingress_remain)

        exit()



    def topology_engineering(self,  fill_remain = False):
        """
        Iteratively optimize the links within OCSs,
        until function U(x) (paper's formula 8) can't decrease any more.
        Args:
            - fill_remain: 是否连接剩余空余链路, 默认False表示不连接剩余空余链路，True表示连接
        """
        # A part of initial cost value
        # each row of x_solution denotes a OCS's intra links
        # every ocs links' 2-dimension subscript -> 1-dimension subscript. eg: x_ij -> x_y : y = (i-1) * n + j
        self._x_solution = np.array([[0] * (self._pods_num * self._pods_num + 1)] * self._ocs_num)

        if type(self._d_ij) != np.ndarray: 
            self._d_ij = np.array(self._d_ij)
        d_ij_ceil = np.ceil(self._d_ij)
        d_ij_floor = np.floor(self._d_ij)
        cost_matrix_from_dij = d_ij_ceil + d_ij_floor

        last_big_u = 999999999
        while True:
            for k in range(self._ocs_num):
                x_solution_sum_k = np.sum(self._x_solution, axis = 0)
                min_cost_object = pywrapgraph.SimpleMinCostFlow()
                self._min_cost_flow(
                    min_cost_object,
                    x_solution_sum_k,
                    cost_matrix_from_dij,
                    self._h_ingress[k],
                    self._h_egress[k],
                    self._pods_num,
                    k
                )

            big_u = 0
            x_solution_sum_k = np.sum(self._x_solution, axis = 0)
            for i in range(self._pods_num):
                for j in range(self._pods_num):
                    big_u += (x_solution_sum_k[i * self._pods_num + j + 1] - d_ij_floor[i][j]) * (x_solution_sum_k[i * self._pods_num + j + 1] - d_ij_ceil[i][j])

            if big_u >= last_big_u:
                # iteration stop
                break
            last_big_u = big_u
            last_x_solution_sum_k = x_solution_sum_k

        # 变形得到新的拓扑连接数整数解x_ij
        # print(x_solution_sum_k)
        x_ij = np.zeros((self._pods_num, self._pods_num), dtype = np.int)
        for i in range(self._pods_num):
            for j in range(self._pods_num):
                x_ij[i][j] = last_x_solution_sum_k[(i + 1 - 1) * self._pods_num + j + 1]

        # x_ij 是传给TE和计算link utilization用
        # TODO: 具体数据中心每个OCS中连接方式可从 x_solution_sum_k 中获取
        
        if fill_remain == 'equal':
            new_x_ij = self.fill_reamin_connects_equal(x_ij, self._r_egress, self._r_ingress)
            return new_x_ij
        elif fill_remain == 'random':
            new_x_ij = self.fill_reamin_connects_random(x_ij, self._r_egress, self._r_ingress)
            return new_x_ij
        else:
            return x_ij

    def _min_cost_flow(self, min_cost_object, x_solution_sum_k, cost_matrix_from_dij, h_ingress, h_egress, pods_num, k):
        """
        For every OCS k, optimize the intra links
        """
        # In order to transfer the range of x_(k)_ij  to [0, ...],
        # let new_x = x_(k)_ij - (x_solution_sum_k - 1).
        # So change following constraint conditions, based on x_solution_sum_k or delta
        
        # 放大器常量，为了满足反馈回路-0.000001的cost，但是min cost flow求解器需要整数故，其他流cost也整体放大
        AMPLIFIER = 1000000

        delta = int(np.sum(x_solution_sum_k)) + self._pods_num
        
        for i in range(self._pods_num):
            # Add a (virtual) source node.
            # Add Source->OcsPort links constraint.
            # Source's index = 0, so OcsPort's index = i + 1
            # The cost of these link is 0
            min_cost_object.AddArcWithCapacityAndUnitCost(
                0, i + 1, h_ingress[i + 1] + delta, 0
            )

            # Suppose a (virtual) Sink node
            # Add (virtul) OcsPort->Sink links constraint
            # Sink's index = 2 * _pods_num + 1
            # Corresponding OcsPort(link Pods) in paper is left nodes[i]->right nodes[n + i]
            # where i = 1,2,...,n, j = 1,2,...,n
            # These (virtual) OcsPort's index is like n + i,
            # Circulation variable i is began as 0, so OcsPort's index is n + i + 1
            min_cost_object.AddArcWithCapacityAndUnitCost(
                pods_num + i + 1,
                2 * pods_num + 1,
                h_egress[i + 1] + delta,
                0
            )

            # Add real OcsPort-> virtual OcsPort links
            # The index i of virtual OcsPort corresponds to the index n + i of real OcsPort
            for j in range(pods_num):
                # the identical virtual and real OcsPort don't connect
                if i != j:
                    min_cost_object.AddArcWithCapacityAndUnitCost(
                        i + 1,
                        pods_num + j + 1,
                        2 if x_solution_sum_k[i * pods_num + j + 1] > 1 else 1,
                        int(2 * x_solution_sum_k[i * pods_num + j + 1] - cost_matrix_from_dij[i][j]) * AMPLIFIER,
                    )


        # 给反馈回路，极小负cost，目的是选出min-cost相同的选择中，尽可能大的flow
        min_cost_object.AddArcWithCapacityAndUnitCost(
            2 * pods_num + 1,
            0,
            999999999999,
            int(-0.000001 * AMPLIFIER),
        )

        # 不需要supply和demand，自闭环解出min cost flow
        # min_cost_object.SetNodeSupply(0, sum(h_ingress))
        # min_cost_object.SetNodeSupply(2 * pods_num + 1, -sum(h_egress))

        # if min_cost_object.Solve() == min_cost_object.OPTIMAL:
        if min_cost_object.SolveMaxFlowWithMinCost() == min_cost_object.OPTIMAL:
            # print('\nMinimum cost:', min_cost_object.OptimalCost())
            # print('  Arc    Flow / Capacity  Cost')
            for i in range(min_cost_object.NumArcs()):
                cost = min_cost_object.Flow(i) * min_cost_object.UnitCost(i)
                # print('%1s -> %1s   %3s  / %3s       %3s' % (
                #     min_cost_object.Tail(i),
                #     min_cost_object.Head(i),
                #     min_cost_object.Flow(i),
                #     min_cost_object.Capacity(i),
                #     cost))
                if ((0 < min_cost_object.Tail(i) <= self._pods_num) 
                    and (self._pods_num < min_cost_object.Head(i) <= self._pods_num * 2)):
                    # index needs two map, because of
                    # (i-1) * n + j
                    # virtual ports = real ports + n
                    index = (min_cost_object.Tail(i) - 1) * self._pods_num + (min_cost_object.Head(i) - self._pods_num)
                    # Because of new_x = x_(k)_ij - (x_solution_sum_k - 1)
                    # real solution = flow + last derivative + 1
                    if self._x_solution[k][index] > 0:
                        self._x_solution[k][index] = min_cost_object.Flow(i) + self._x_solution[k][index] - 1
                    else:
                        self._x_solution[k][index] = min_cost_object.Flow(i)
        else:
            print('There was an issue with the min cost flow input.')
        

if __name__ == '__main__':
    # ---test
    obj = MapFractionalTopoToOcs(
        ocs_num = 4,
        d_ij = [
            [0, 4.4, 5.2, 6.4, 4.4, 5.2, 6.4, 0],
            [3.2, 0, 3.2, 16.4, 6.4, 4.4, 15.2, 16.4],
            [6.4, 15.3, 0, 18.9, 6.4, 14.4, 15.2, 16.4],
            [2.3, 15.4, 2.3, 0, 16.4, 14.4, 15.2, 16.4],
            [0, 14.4, 15.2, 16.4, 14.4, 15.2, 16.4, 0],
            [13.2, 0, 3.2, 16.4, 16.4, 14.4, 15.2, 16.4],
            [6.4, 15.3, 0, 18.9, 16.4, 14.4, 15.2, 16.4],
            [12.3, 15.4, 12.3, 0, 16.4, 14.4, 15.2, 16.4],
        ],
    )
    obj.load_init_config('../../config/pods_config.csv')
    x_ij = obj.topology_engineering(True)
    print(x_ij)