from ortools.linear_solver import pywraplp
import numpy as np
from utils.base import DcnBase
import math
import gurobipy as grb
from root_const import ROOT_PATH

class ValiantRouting(DcnBase):
    """
    Desc: 流量工程做routing，顺便计算link utilization
    """
 
    _w_routing = {}
    
    
    def __init__(self, topology_pods):
        """
        Desc: 做routing前，除了需要加载基类的配置读取，配置其他参数
              还需要添加前面的拓扑工程计算出来的pods间连接数
        Inputs:
            - topology_pods(2-dimension list): Number of s_i egress links connected to igress links of s_j.
        """
        self._topology_pods = topology_pods

    
    def traffic_engineering(self, actual_traffic):
        """
        """
        if not isinstance(actual_traffic, np.ndarray):
            traffic = np.array(actual_traffic)
        else:
            traffic = actual_traffic
    
        if not isinstance(self._bandwidth, np.ndarray):
            bandwidth = np.array(self._bandwidth)
        else:
            bandwidth = self._bandwidth

        pods_num = self._pods_num
        capacity = self._topology_pods * bandwidth
        
        w_routing = {}
        even_ratio = 1 / (pods_num - 1)
        # TODO: even_ratio按照capacity比例去分。
        for i in range(1, pods_num + 1):
            for j in range(1, pods_num + 1):
                for k in range(pods_num + 1):
                    w_routing[f'w_{i}_{k}_{j}'] = even_ratio
                    # if k == 0:
                    #     w_routing[f'w_{i}_{k}_{j}'] = even_ratio * 2
                    # else:
                    #     w_routing[f'w_{i}_{k}_{j}'] = even_ratio / 2

        self._w_routing = w_routing
        return w_routing


if __name__ == "__main__":
    test = ValiantRouting([1])
    conf_file = f'{ROOT_PATH}/config/pods_config.csv'
    test.load_init_config(conf_file)
    traffic_TE = np.load(f"{ROOT_PATH}/data/data_properties_100/out_pod{8}.npy")
    # test.again_routing(traffic_TE, 380)
    # test.traffic_engineering(traffic_TE)
    test.traffic_engineering(traffic_TE)
    lu, mlu, alu  = test.calc_link_utilization(traffic_TE)
    # print(test._w_routing)
    print(lu, mlu, alu)