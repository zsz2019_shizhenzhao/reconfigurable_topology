from ortools.linear_solver import pywraplp
from utils.base import DcnBase
import numpy as np
import gurobipy as grb
import math
import caopeirui.LFToE_simulation.find_traffic_cluster_new as ftc

class FindFractionalTopology(DcnBase):
    def _find_mlu(self, tm, rows, cols, ingress_capacity, egress_capacity):
        # print(rows, cols)
        # print(egress_capacity, ingress_capacity)
        mlu = 0.0
        for row in rows:
            mlu = max(mlu, sum([tm[row][col] for col in cols]) / egress_capacity[row])
        for col in cols:
            mlu = max(mlu, sum([tm[row][col] for row in rows]) / ingress_capacity[col])
        return mlu

    def _analyze_tm(self, tm):
        busy_rows = []
        busy_cols = []
        row_mlus = []
        col_mlus = []
        for row in range(self._pods_num):
            row_mlus.append(sum([tm[row][col] for col in range(self._pods_num)]) / self._r_egress[row])
        for col in range(self._pods_num):
            col_mlus.append(sum([tm[row][col] for row in range(self._pods_num)]) / self._r_ingress[col])
        max_mlu = max(max(row_mlus), max(col_mlus))
        for row in range(self._pods_num):
            if row_mlus[row] > max_mlu * 0.8:   # 0.8 can be also a tunable parameter
                busy_rows.append(row)
        for col in range(self._pods_num):
            if col_mlus[col] > max_mlu * 0.8:
                busy_cols.append(col)
        base = np.zeros((self._pods_num, self._pods_num), dtype=np.float)
        for row in busy_rows:
            total = sum([tm[row][col] for col in range(self._pods_num)])
            threshold = total / (self._pods_num - 1) / 2
            for col in range(self._pods_num):
                if col != row:
                    if tm[row][col] < threshold: 
                        base[row][col] = 1
        for col in busy_cols:
            total = sum([tm[row][col] for row in range(self._pods_num)])
            threshold = total / (self._pods_num - 1) / 2
            for row in range(self._pods_num):
                if col != row:
                    if tm[row][col] < threshold: 
                        base[row][col] = 1
        return base


    def get_fractional_topology(self, testing_tms):
        """
        Desc: According to the number of pods and pods' ingress/egress, and bandwidth,
              find the topology of all pods' connection by LP.
        
        Returns: (u.solution_value(), d_ij)
            - u.solution_value(): 1/(max link utilization)
            - d_ij: Denote the number of s_i egress links connected to ingress links of s_j.
        """
        # testing_tms is the set of historical TMs
        self._testing_tms = testing_tms
        # Use all the historical TMs for ToE
        self._traffic_sequence = testing_tms
        self._traffic_count = len(testing_tms)
        
        """import caopeirui.LFToE_simulation.preprocess as pp
        before_var = pp.get_place_var(testing_tms)
        before_std = np.sqrt(before_var)
        traffic_std = before_std.reshape(self._pods_num, self._pods_num)"""

        base = np.zeros((self._pods_num, self._pods_num), dtype=np.float)
        for tm in self._traffic_sequence:
            tmp_base = self._analyze_tm(tm)
            for row in range(self._pods_num):
                for col in range(self._pods_num):
                    base[row][col] = max(base[row][col], tmp_base[row][col])
        print(base)
        scale = 4
        base_topology = np.zeros((self._pods_num, self._pods_num), dtype=np.float)
        for row in range(self._pods_num):
            for col in range(self._pods_num):
                base_topology[row][col] = base[row][col] * scale

        d_ij = self._compute_topology(base_topology)
        print("topology:\n", d_ij)
        return d_ij

        """
        for i in range(1):

            mlu, weight = self._test_topology(d_ij)
            print("weight:\n", weight)
            print("mlu:", mlu)
            threshold = weight.max() * 0.6
            for src in range(self._pods_num):
                for dst in range(self._pods_num):
                    if weight[src][dst] > threshold:
                        base_topology[src][dst] += 1
            print("base\n", base_topology)
        return d_ij
        """


    def cosine_similarity(self, x, y):
        assert len(x) == len(y), "len(x) != len(y)"
        zero_list = [0] * len(x)
        if x == zero_list or y == zero_list:
            return float(1) if x == y else float(0)
        res = np.array([[x[i] * y[i], x[i] * x[i], y[i] * y[i]] for i in range(len(x))])
        cos = sum(res[:, 0]) / (np.sqrt(sum(res[:, 1])) * np.sqrt(sum(res[:, 2])))
        return cos

    """
    def _test_topology(self, d_ij):
        import caopeirui.LFToE_simulation.LP_routing as lr
        ROUTING = []
        for tm in self._traffic_sequence:
            obj_TE = lr.LinearProgrammingRouting(d_ij)
            obj_TE.load_init_config(self._config_file)
            _, _ = obj_TE.traffic_engineering_grb(tm)
            ROUTING.append(obj_TE)
        # Start testing topology
        weight = np.zeros((self._pods_num, self._pods_num), dtype=np.float)
        overall_mlu = 0.0
        for idx in range(len(self._testing_tms) - 1):
            cos_sim = 0
            previous_traffic = self._testing_tms[idx]
            current_traffic = self._testing_tms[idx + 1]

            opt_index = None
            for t in range(len(self._traffic_sequence)):
                tmp_cos_sim = self.cosine_similarity(self._traffic_sequence[t].flatten().tolist(), previous_traffic.flatten().tolist())
                if tmp_cos_sim > cos_sim:
                    cos_sim = tmp_cos_sim
                    opt_index = t
            
            TE = ROUTING[opt_index]
            _, mlu, _, position = TE.calc_normal_link_utilization(current_traffic)
            overall_mlu = max(overall_mlu, mlu)
            for i in range(len(position[0])):
                # give higher weight for links causing higher mlu
                if mlu > 1:
                    weight[position[0][i]][position[1][i]] += 1
        return overall_mlu, weight
    """


    def _compute_topology(self, base_topology):
        if self._traffic_count == 0:
            raise ValueError('''
                No traffic input has been set yet, 
                please call add_traffic() firstly.
            ''')
        # Set the diagonal entries as 0.
        for tm in self._traffic_sequence:
            for pod in range(self._pods_num):
                tm[pod][pod] = 0

        remain_ingress = self._r_ingress.astype(np.float)
        remain_egress = self._r_egress.astype(np.float)
        remain_rows = [i for i in range(self._pods_num)]
        remain_cols = [i for i in range(self._pods_num)]

        d_ij = np.zeros((self._pods_num, self._pods_num), dtype=np.float)

        # Set up base topology
        for i in range(self._pods_num):
            for j in range(self._pods_num):
                d_ij[i][j] = base_topology[i][j]
                remain_ingress[j] -= base_topology[i][j]
                remain_egress[i] -= base_topology[i][j]

        while True:
            # Computes the max scaling factor
            mlu = [self._find_mlu(tm, remain_rows, remain_cols, remain_ingress, remain_egress) for tm in self._traffic_sequence]
            max_tm = np.zeros((self._pods_num, self._pods_num), dtype=np.float)
            for multiple in range(0, self._traffic_count):
                tm = self._traffic_sequence[multiple]
                if mlu[multiple] < 0.0001:
                    continue
                max_scale_up = 1 / mlu[multiple]
                for row in remain_rows:
                    for col in remain_cols:
                        max_tm[row][col] = max(tm[row][col] * max_scale_up, max_tm[row][col])
            overall_mlu = self._find_mlu(max_tm, remain_rows, remain_cols, remain_ingress, remain_egress)
            if overall_mlu < 0.0001:
                break
            overall_scale_up = 1 / overall_mlu
            # Updates topology
            for row in remain_rows:
                for col in remain_cols:
                    scale_entry = max_tm[row][col] * overall_scale_up
                    remain_egress[row] -= scale_entry
                    remain_ingress[col] -= scale_entry
                    d_ij[row][col] += scale_entry
            # Remove rows and cols with no capacity
            rows_to_be_removed = []
            for row in remain_rows:
                if remain_egress[row] < 0.0001:
                    rows_to_be_removed.append(row)
            for row in rows_to_be_removed:
                remain_rows.remove(row)
            cols_to_be_removed = []
            for col in remain_cols:
                if remain_ingress[col] < 0.0001:
                    cols_to_be_removed.append(col)
            for col in cols_to_be_removed:
                remain_cols.remove(col)
            # Break if remain_ingress or remain_egress is empty
            if len(remain_rows) == 0 or len(remain_cols) == 0:
                break
            
        return d_ij
    

if __name__ == "__main__":
    # ------demo test------
    pods_num = 8
    first_sample_traffic = np.random.rand(pods_num, pods_num) * 100000
    second_sample_traffic = np.random.rand(pods_num, pods_num) * 100000
    third_sample_traffic = np.random.rand(pods_num, pods_num) * 100000

    obj = FindFractionalTopology()
    obj.load_init_config('../../config/pods_config.csv')
    
    obj.add_a_traffic(first_sample_traffic)
    print(first_sample_traffic)
    obj.add_a_traffic(second_sample_traffic)
    obj.add_a_traffic(third_sample_traffic)
    d_ij = obj.get_fractional_topology()
    print(d_ij)
    d_ij = obj.to_integer_topo(d_ij)
    print(d_ij)