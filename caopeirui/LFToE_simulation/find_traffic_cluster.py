import numpy as np
from sklearn.cluster import KMeans
from utils.base import DcnBase

class TrafficKMeans(DcnBase):
    """
    k-means cluster algorithm for traffic matrix.
    If model input(data matrix) is m*n dimensional matrix,
    the result of k-means cluster will be k rows, every of which represents a predicting result.
    Then every predicting traffic is reshaped to 2-dimensional matrix.
    Return results(3-dimensional) of including predicting traffic.
    """
    _k = 1

    def __init__(self, k = 1):
        self._k = k


    def get_pred_traffic(self):
        data_mat = []
        record_shape = self._traffic_sequence[0].shape
        for a_traffic in self._traffic_sequence:
            traffic_to_one_dimension = a_traffic.reshape(1, np.prod(np.shape(a_traffic)))[0]
            data_mat.append(traffic_to_one_dimension)

        km = KMeans(n_clusters = self._k)
        km.fit(data_mat)
        centers = km.cluster_centers_ # 质心
        
        pred_traffic = []
        for center in centers:
            center_np = np.asarray(center)
            negative_values = center_np < 0.0001  # Small values could make the traffic matrix unsolvable.
            center_np[negative_values] = 0.0001 
            tmp = np.array(center_np).reshape(record_shape)
            pred_traffic.append(tmp)
        return pred_traffic


if __name__ == "__main__":
    # ----test1
    # obj = TrafficKMeans(k = 3)

    # obj.add_a_traffic(np.random.rand(5, 4) * 100)
    # obj.add_a_traffic(np.random.rand(5, 4) * 100)
    # obj.add_a_traffic(np.random.rand(5, 4) * 100)
    # res = obj.get_pred_traffic()
    # print(res)
    # exit()
    # ----test2
    obj = TrafficKMeans(k = 2)
    for i in range(10):
        traffic = np.load(f"../../data/data_properties_100/out_pod{i}.npy")
        obj.add_a_traffic(traffic)
        print(traffic)
    res = obj.get_pred_traffic()
    print(res)
