import numpy as np
import find_traffic_cluster as ftc
import dcntopology as dcn
import find_topology_and_routing as ftr
import map_to_ocs as opt


if __name__ == "__main__":
    # ------test------
    # Find traffic cluster

    # Find fractional topoloty
    pods_num = 4
    first_sample_traffic = np.random.rand(pods_num, pods_num) * 100
    second_sample_traffic = np.random.rand(pods_num, pods_num) * 100
    third_sample_traffic = np.random.rand(pods_num, pods_num) * 100
    # first_sample_traffic = np.load('../../data/data100/out1.npy')
    # second_sample_traffic = np.load('../../data/data100/out2.npy')
    # third_sample_traffic = np.load('../../data/data100/out3.npy')

    # The actual used subscripts of r_(in/e)gress start at 1
    # The 0th element was added just for convenient programming
    r_ingress =  [0] + [128] * pods_num
    r_egress =  [0] + [128] * pods_num
    bandwidth = [[30] * pods_num for _ in range(pods_num)]
    obj = ftr.FindTopologyAndRouting(r_ingress, r_egress, bandwidth, pods_num)
    obj.add_traffic(first_sample_traffic)
    obj.add_traffic(second_sample_traffic)
    obj.add_traffic(third_sample_traffic)
    u, d_ij = obj.get_topology_by_LP()

    print(u)
    print(d_ij)    

    # traffic = np.load("../../data/data" + str(pods_num) + "/out" + str(1) + ".npy")
    u, w_ikj = obj.traffic_engineering(d_ij, third_sample_traffic)
    print(w_ikj)
    exit()

    # Map farational topology to OCS
    res = opt.MapFractionalTopoToOcs(
        ocs_num = 2,
        d_ij = [
            [0, 14.4, 15.2, 16.4],
            [13.2, 0, 13.2, 16.4],
            [16.4, 15.3, 0, 18.9],
            [12.3, 15.4, 12.3, 0]
        ],
        h_ingress = [
            [0, 118, 110, 120, 115],
            [0, 119, 116, 110, 111]
        ],
        h_egress = [
            [0, 118, 110, 120, 115],
            [0, 119, 116, 110, 111]
        ],
        pods_num = 4
    )

    res.get_result()