# Project Brief
Simulation for 《The Tortoise and the Hare:Handling Fast Data Center Traﬃc DynamicsUsing Low Frequency Topology Engineering》

# Code File Description
```
|____dcntopology.py(will be canceled)  Simulate a data center topology, specially including capacities, unit_costs etc.  
|____find_traffic_cluster.py        Custom k-means for traffic matrix.
|____find_fractional_topology.py(will be canceled)    According to traffic matrix and physical topology links' constraint,use linear programming to get fractional topology(d_ij).
|____find_topology_and_routing.py   According to the number of pods and pods' ingress/egress, and bandwidth,
                                    find the topology of all pods' connection by LP
                                    and the result of routing(allocated traffic between pods) by LP.
|____map_to_ocs.py                  Based on fractional topology results(d_ij) of fore steps,
                                    map onto near-optimal OCS's links.
|____main.py                        The main entrance of total simulation including other steps.
```