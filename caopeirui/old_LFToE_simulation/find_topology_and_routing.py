from ortools.linear_solver import pywraplp
import numpy as np
import random as rd


class FindTopologyAndRouting():
    """According to the number of pods and pods' ingress/egress, and bandwidth,
       find the topology of all pods' connection by LP
       and the result of routing(allocated traffic between pods) by LP.
    """
    # Every time add a traffic input, _traffic_count adds 1.
    _traffic_count = 0
    # Store traffic matrix(2-dimension), so _traffic is 3-dimension.
    _traffic = []
    _r_ingress = []
    _r_egress = []
    _bandwidth = []
    _pods_num = 0


    def __init__(self, r_ingress, r_egress, bandwidth, pods_num):
        """
        Inputs:
            - r_ingress(list): Number of ingress links of s_i. (List including pods_num+1 elements,
                            Because the 0 elements is not used)
            - r_egress(list): Number of egress links of s_i. (List including pods_num+1 elements)
            - bandwidth(2-dimension list): Link capacity(Gbps) between s_i and s_j.
            - pods_num(int): Number of pods.
        """
        # TODO: exception handling. eg:  raise ValueError('xxx')
        self._r_ingress = r_ingress
        self._r_egress = r_egress
        self._bandwidth = bandwidth
        self._pods_num = pods_num


    def add_traffic(self, traffic):
        """
        Desc: Add traffic matrices in chronological order.
        Inputs:
            - traffic(numpy.narray): 2-dimension traffic matrix.
        """
        # TODO: exception handling. eg:  raise ValueError('xxx')
        # For example, traffic matrix dimensions aren't 2,
        # or the number of rows and columns of traffic matrix is not consistent.
        self._traffic.append(traffic)
        self._traffic_count += 1


    def get_topology_by_LP(self):
        """
        Desc: According to the number of pods and pods' ingress/egress, and bandwidth,
              find the topology of all pods' connection by LP.
        
        Returns: (u.solution_value(), d_ij)
            - u.solution_value(): 1/(max link utilization)
            - d_ij: Denote the number of s_i egress links connected to ingress links of s_j.
        """
        if self._traffic_count == 0:
            raise ValueError('''
                No traffic input has been set yet, 
                please call add_traffic() firstly.
            ''')
        solver = pywraplp.Solver('FindFractionalTopology',
                                pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)

        u = solver.NumVar(0, solver.infinity(), 'u')

        # d_ij denotes the number of links which s_i connected to s_j
        name_d_ij = [
            f'd_{i}_{j}'
            for i in range(1, self._pods_num + 1)
            for j in range(1, self._pods_num + 1)
            if i != j
        ]
        variables_d_ij = {}
        for d in name_d_ij:
            variables_d_ij[d] = solver.NumVar(0, solver.infinity(), d)

        # Add all constraints for every input traffic matrix
        for multiple in range(0, self._traffic_count):
            a_traffic = self._traffic[multiple]
            # w_ikj denotes the actual traffic routed from pod s_i to pod s_j though s_k.
            # If k=0, w_i0j denotes the traffic routed from pod s_i to pod s_j directly.
            name_w_ikj = [
                f'{multiple}_w_{i}_{k}_{j}'
                for i in range(1, self._pods_num + 1)
                for k in range(self._pods_num + 1)
                for j in range(1, self._pods_num + 1)
                if i != k and i != j and j != k
            ]
            variables_w_ikj = {}
            for w in name_w_ikj:
                variables_w_ikj[w] = solver.NumVar(0, solver.infinity(), w)

            for i in range(1, self._pods_num + 1):
                # Corresponding to the paper formula(1)
                # summation(d_ij) <= r_i_(e/in)gress
                constraint_dr_egress = solver.Constraint(-solver.infinity(), self._r_egress[i])
                constraint_dr_ingress = solver.Constraint(-solver.infinity(), self._r_ingress[i])

                for j in range(1, self._pods_num + 1):
                    if i != j:
                        # Corresponding to the paper formula(1)
                        constraint_dr_egress.SetCoefficient(variables_d_ij[f'd_{i}_{j}'], 1)
                        constraint_dr_ingress.SetCoefficient(variables_d_ij[f'd_{j}_{i}'], 1)

                        # Corresponding to the paper formula(2)->1)
                        # summation(w_p_ij) = u * t_ij  =>  summation(w_p) - u * t_ij
                        constraint =  solver.Constraint(0, 0)
                        constraint.SetCoefficient(u, -a_traffic[i - 1][j - 1])

                        # Corresponding to the paper formula(2)->2)
                        # summation(w_p_sisj) ≤ d_ij * b_ij  =>  d_ij * b_ij - summation(w_p) ≥ 0
                        constraint_db = solver.Constraint(0, solver.infinity())
                        constraint_db.SetCoefficient(variables_d_ij[f'd_{i}_{j}'], self._bandwidth[i - 1][j - 1])    
                        for k in range(0, self._pods_num + 1):
                            if k != i and k != j:
                                # Corresponding to the paper formula(2)->1) 
                                constraint.SetCoefficient(variables_w_ikj[f'{multiple}_w_{i}_{k}_{j}'], 1)

                                # Corresponding to the paper formula(2)->2)
                                if k != 0:
                                    constraint_db.SetCoefficient(variables_w_ikj[f'{multiple}_w_{k}_{i}_{j}'], -1)
                                    constraint_db.SetCoefficient(variables_w_ikj[f'{multiple}_w_{i}_{j}_{k}'], -1)
                                else:
                                    constraint_db.SetCoefficient(variables_w_ikj[f'{multiple}_w_{i}_{0}_{j}'], -1)


        # Create the objective function, maximize u.
        objective = solver.Objective()
        objective.SetCoefficient(u, 1)
        objective.SetMaximization()
        # Call the solver and display the results.
        solver.Solve()
        # print('Solution:')
        # print('u = ', u.solution_value())

        d_ij = [[0] * self._pods_num for _ in range(self._pods_num)]
        for i in range(1, self._pods_num + 1):
            for j in range(1, self._pods_num + 1):
                if i != j:
                    d_ij[i - 1][j - 1] = variables_d_ij[f'd_{i}_{j}'].solution_value()

        return u.solution_value(), d_ij


    def traffic_engineering(self, d_ij, actual_traffic):
        """
        Desc: Using the linear programming to obtain the optimal routing(w_i_k_j)

        Inputs:
            - d_ij(2-dimension list): Number of s_i egress links connected to igress links of s_j.
            - actual_traffic(2-dimension list): Actual traffic in the datacenter.
        Returns: 
            - w: The result of routing.
        """
        if not isinstance(actual_traffic, np.ndarray):
            traffic = np.array(actual_traffic)
        else:
            traffic = actual_traffic

        if not isinstance(self._bandwidth, np.ndarray):
            bandwidth = np.array(self._bandwidth)

        pods_num = self._pods_num
        capacity = d_ij * bandwidth
        
        solver = pywraplp.Solver('FindFractionalTopology',
                                pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)
        max_link_utilization = solver.NumVar(0, solver.infinity(), 'max_link_utilization')

        # w_ikj denotes the actual traffic routed from pod s_i to pod s_j though s_k.
        # If k=0, w_i0j denotes the traffic routed from pod s_i to pod s_j directly.
        name_w_ikj = [
            f'w_{i}_{k}_{j}'
            for i in range(1, pods_num + 1)
            for k in range(pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != k and i != j and j != k
        ]
        variables_w_ikj = {}
        for w in name_w_ikj:
            variables_w_ikj[w] = solver.NumVar(0, 1, w)
            # variables_w_ikj[w] = solver.NumVar(0, solver.infinity(), w)

        for i in range(1, pods_num + 1):
            for j in range(1, pods_num + 1):
                if i != j:
                    # summation(w_ikj) = 1
                    constraint_w_ikj = solver.Constraint(1, 1)
                    # summation(T_ij*w_) <= u * capacity
                    # => 0 <= u * capacity - summation(T_ij*w_)
                    constraint_w_capacity = solver.Constraint(0, solver.infinity())
                    constraint_w_capacity.SetCoefficient(max_link_utilization, capacity[i - 1][j - 1])
                    for k in range(0, pods_num + 1):
                        if k != i and k != j:
                            constraint_w_ikj.SetCoefficient(variables_w_ikj[f'w_{i}_{k}_{j}'], 1)
                            if k != 0:
                                # In the physical sense, the routing traffic ratio w_ should multiply the actual traffic from source to destination
                                # constraint_w_capacity.SetCoefficient(variables_w_ikj[f'w_{k}_{i}_{j}'], -traffic[i - 1][j - 1])
                                constraint_w_capacity.SetCoefficient(variables_w_ikj[f'w_{k}_{i}_{j}'], -traffic[k - 1][j - 1])
                                # constraint_w_capacity.SetCoefficient(variables_w_ikj[f'w_{i}_{j}_{k}'], -traffic[i - 1][j - 1])
                                constraint_w_capacity.SetCoefficient(variables_w_ikj[f'w_{i}_{j}_{k}'], -traffic[i - 1][k - 1])
                            else:
                                constraint_w_capacity.SetCoefficient(variables_w_ikj[f'w_{i}_{0}_{j}'], -traffic[i - 1][j - 1])

        objective = solver.Objective()
        objective.SetCoefficient(max_link_utilization, 1)
        objective.SetMinimization()
        # Call the solver and display the results.
        solver.Solve()
        print('Solution:')
        print('max_link_utilization = ', max_link_utilization.solution_value())

        # The result of routing w_**
        w_ikj = [[[] for _ in range(self._pods_num)] for _ in range(self._pods_num)]
        for i in range(1, self._pods_num + 1):
            for j in range(1, self._pods_num + 1):
                if i != j:
                    w_ikj[i - 1][j - 1] = []
                    for k in range(self._pods_num + 1):
                        if k != i and k != j:
                            print(f'w_{i}_{k}_{j}', end=' ')
                            w_ikj[i - 1][j - 1].append(variables_w_ikj[f'w_{i}_{k}_{j}'].solution_value())
                    print(f'↑{i}_{j}')
        return max_link_utilization.solution_value(), w_ikj


if __name__ == '__main__':
    # ------test------
    pods_num = 4
    first_sample_traffic = np.random.rand(pods_num, pods_num) * 100
    second_sample_traffic = np.random.rand(pods_num, pods_num) * 100
    third_sample_traffic = np.random.rand(pods_num, pods_num) * 100
    # first_sample_traffic = np.load('../../data/data100/out1.npy')
    # second_sample_traffic = np.load('../../data/data100/out2.npy')
    # third_sample_traffic = np.load('../../data/data100/out3.npy')

    # The actual used subscripts of r_(in/e)gress start at 1
    # The 0th element was added just for convenient programming
    r_ingress =  [0] + [128] * pods_num
    r_egress =  [0] + [128] * pods_num
    bandwidth = [[30] * pods_num for _ in range(pods_num)]
    obj = FindTopologyAndRouting(r_ingress, r_egress, bandwidth, pods_num)
    obj.add_traffic(first_sample_traffic)
    obj.add_traffic(second_sample_traffic)
    obj.add_traffic(third_sample_traffic)
    u, d_ij = obj.get_topology_by_LP()

    print(u)
    print(d_ij)    

    # traffic = np.load("../../data/data" + str(pods_num) + "/out" + str(1) + ".npy")
    u, w_ikj = obj.traffic_engineering(d_ij, third_sample_traffic)
    print(w_ikj)
