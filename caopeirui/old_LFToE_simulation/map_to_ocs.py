from ortools.graph import pywrapgraph
import numpy as np

class MapFractionalTopoToOcs:
    """
    Based on fractional topology results(d_ij) of fore steps,
    map onto near-optimal OCS's links 
    """
    # Fractional topology; 
    # d_ij denotes the number of s_i egress links connected to ingress links of s_j
    _d_ij = []

    # The number of OCSs
    _ocs_num = 0
    # The number of pods
    _pods_num = 0

    # Number of physical egress links connecting s_i to o_k , 
    # and number of physical ingress links of s_j to o_k , respectively
    # Both _h_ingress and _h_egress are saved as in tow-dimension list,
    # so k responses every row data.
    _h_ingress = []
    _h_egress = []

    _x_derivative = [[]]

    def __init__(self, ocs_num, d_ij, h_ingress, h_egress, pods_num):
        # TODO: exception handling. eg:  raise ValueError('xxx')
        # TODO: the setting function of these parameters  
        self._ocs_num = ocs_num
        self._d_ij = d_ij
        self._h_ingress = h_ingress
        self._h_egress = h_egress
        self._pods_num = pods_num


    def get_result(self):
        """
        Iteratively optimize the links within OCSs,
        until function U(x) (paper's formula 8) can't decrease any more.
        """

        # A part of initial cost value
        # each row of x_derivative denotes a OCS's intra links
        # 2-dimension subscript -> 1-dimension subscript. eg: x_ij -> x_y : y = (i-1) * n + j
        self._x_derivative = np.array([[0] * (self._pods_num * self._pods_num + 1)] * self._ocs_num)

        if type(self._d_ij) != np.ndarray: 
            self._d_ij = np.array(self._d_ij)
        d_ij_ceil = np.ceil(self._d_ij)
        d_ij_floor = np.floor(self._d_ij)
        cost_matrix_from_dij = d_ij_ceil + d_ij_floor

        last_big_u = 999999999
        while True:
            for k in range(self._ocs_num):
                x_derivative_sum_k = np.sum(self._x_derivative, axis = 0)
                min_cost_object = pywrapgraph.SimpleMinCostFlow()
                self._min_cost_flow(
                    min_cost_object,
                    x_derivative_sum_k,
                    cost_matrix_from_dij,
                    self._h_ingress[k],
                    self._h_egress[k],
                    self._pods_num,
                    k
                )
                print(self._x_derivative)

            big_u = 0
            x_derivative_sum_k = np.sum(self._x_derivative, axis = 0)
            for i in range(self._pods_num):
                for j in range(self._pods_num):
                    big_u += (x_derivative_sum_k[i * self._pods_num + j + 1] - d_ij_floor[i][j]) * (x_derivative_sum_k[i * self._pods_num + j + 1] - d_ij_ceil[i][j])
            print(big_u)

            if big_u >= last_big_u:
                # iteration stop
                break
            last_big_u = big_u


    def _min_cost_flow(self, min_cost_object, x_derivative_sum_k, cost_matrix_from_dij, h_ingress, h_egress, pods_num, k):
        """
        For every OCS k, optimize the intra links
        """
        # In order to transfer the range of x_(k)_ij  to [0, ...],
        # let new_x = x_(k)_ij - (x_derivative_sum_k - 1).
        # So change following constraint conditions, based on x_derivative_sum_k or delta

        delta = int(np.sum(x_derivative_sum_k)) + self._pods_num
        
        for i in range(self._pods_num):
            # Add a (virtual) source node.
            # Add Source->OcsPort links constraint.
            # Source's index = 0, so OcsPort's index = i + 1
            # The cost of these link is 0
            min_cost_object.AddArcWithCapacityAndUnitCost(
                0, i + 1, h_ingress[i + 1] + delta, 0
            )

            # Suppose a (virtual) Sink node
            # Add (virtul) OcsPort->Sink links constraint
            # Sink's index = 2 * _pods_num + 1
            # Corresponding OcsPort(link Pods) in paper is left nodes[i]->right nodes[n + i]
            # where i = 1,2,...,n, j = 1,2,...,n
            # These (virtual) OcsPort's index is like n + i,
            # Circulation variable i is began as 0, so OcsPort's index is n + i + 1
            min_cost_object.AddArcWithCapacityAndUnitCost(
                pods_num + i + 1,
                2 * pods_num + 1,
                h_egress[i + 1] + delta,
                0
            )

            # Add real OcsPort-> virtual OcsPort links
            # The index i of virtual OcsPort corresponds to the index n + i of real OcsPort
            for j in range(pods_num):
                # the identical virtual and real OcsPort don't connect
                if i != j:
                    min_cost_object.AddArcWithCapacityAndUnitCost(
                        i + 1,
                        pods_num + j + 1,
                        2 if x_derivative_sum_k[i * pods_num + j + 1] > 1 else 1,
                        int(2 * x_derivative_sum_k[i * pods_num + j + 1] - cost_matrix_from_dij[i][j]),
                    )

        # Add a feedback link from Sink node to Source node.
        # The bounds of this feedback link is set as [0, infinite)
        # The cost is a very small negative value. eg: -10^-6
        min_cost_object.AddArcWithCapacityAndUnitCost(
            2 * pods_num + 1,
            0,
            99999999,
            0,
        )

        # TODO: Check with teacher the values of supplies
        min_cost_object.SetNodeSupply(0, sum(h_ingress))
        min_cost_object.SetNodeSupply(2 * pods_num + 1, -sum(h_egress))

        # if min_cost_object.Solve() == min_cost_object.OPTIMAL:
        if min_cost_object.SolveMaxFlowWithMinCost() == min_cost_object.OPTIMAL:
            print('\nMinimum cost:', min_cost_object.OptimalCost())
            print('  Arc    Flow / Capacity  Cost')
            for i in range(min_cost_object.NumArcs()):
                cost = min_cost_object.Flow(i) * min_cost_object.UnitCost(i)
                print('%1s -> %1s   %3s  / %3s       %3s' % (
                    min_cost_object.Tail(i),
                    min_cost_object.Head(i),
                    min_cost_object.Flow(i),
                    min_cost_object.Capacity(i),
                    cost))
                if (0 < min_cost_object.Tail(i) <= self._pods_num) and (self._pods_num < min_cost_object.Head(i) <= self._pods_num * 2):
                    # index needs two map, because of
                    # (i-1) * n + j
                    # virtual ports = real ports + n
                    index = (min_cost_object.Tail(i) - 1) * self._pods_num + (min_cost_object.Head(i) - self._pods_num)
                    # Because of new_x = x_(k)_ij - (x_derivative_sum_k - 1)
                    # real solution = flow + last derivative + 1
                    if self._x_derivative[k][index] > 0:
                        self._x_derivative[k][index] = min_cost_object.Flow(i) + self._x_derivative[k][index] - 1
                    else:
                        self._x_derivative[k][index] = min_cost_object.Flow(i)
        else:
            print('There was an issue with the min cost flow input.')
        

if __name__ == '__main__':
    # ---test
    res = MapFractionalTopoToOcs(
        ocs_num = 4,
        d_ij = [
            [0, 14.4, 15.2, 16.4, 14.4, 15.2, 16.4, 0],
            [13.2, 0, 13.2, 16.4, 16.4, 14.4, 15.2, 16.4],
            [16.4, 15.3, 0, 18.9, 16.4, 14.4, 15.2, 16.4],
            [12.3, 15.4, 12.3, 0, 16.4, 14.4, 15.2, 16.4],
            [0, 14.4, 15.2, 16.4, 14.4, 15.2, 16.4, 0],
            [13.2, 0, 13.2, 16.4, 16.4, 14.4, 15.2, 16.4],
            [16.4, 15.3, 0, 18.9, 16.4, 14.4, 15.2, 16.4],
            [12.3, 15.4, 12.3, 0, 16.4, 14.4, 15.2, 16.4],
        ],
        h_ingress = [
            [0, 118, 110, 120, 115, 118, 110, 120, 115],
            [0, 119, 116, 110, 111, 118, 110, 120, 115],
            [0, 119, 116, 110, 111, 118, 110, 120, 115],
            [0, 119, 116, 110, 111, 118, 110, 120, 115]
        ],
        h_egress = [
            [0, 118, 110, 120, 115, 118, 110, 120, 115],
            [0, 119, 116, 110, 111, 118, 110, 120, 115],
            [0, 119, 116, 110, 111, 118, 110, 120, 115],
            [0, 119, 116, 110, 111, 118, 110, 120, 115]
        ],
        pods_num = 8
    )

    res.get_result()