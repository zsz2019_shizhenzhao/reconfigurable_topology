import ortools
import numpy as np

# TODO: This part will be canceled

class DataCenterTopology:
    """
    simulate a data center topology.
    include traffic, topology etc.
    aim at subsequent optimizing conveniently through parameters such as _start_nodes, _end_nodes
    """
    _traffic = None        # traffic matrix(two-dimensional list)
    _topolinks = None      # topology links matrix(two-dimensional list)

    # other show pattern
    _start_nodes = []
    _end_nodes = []
    _capacities = []
    _unit_costs = []
    _supplies = []

    def __init__(self, traffic = None, topolinks = None):
        if traffic is not None:
            self.set_traffic(traffic)


    def set_traffic(self, traffic = None):
        """
        set or reset data center traffic.
        """
        if traffic == []:
            print('input traffic is []')
            return
        # if the type of traffic is not matrix(2-dim list), transform it.
        if type(traffic) == list:
            self._traffic = traffic
        else:
            # TODO: exception handling. eg:  raise ValueError('xxx')
            self._traffic = list(traffic)
        
        row_size = len(self._traffic)
        line_size = len(self._traffic[0])
        for i in range(0, row_size):
            for j in range(0, line_size):
                if self._traffic[i][j] != 0:
                    self._start_nodes.append(i)
                    self._end_nodes.append(j)
                    # TODO: fill _capacities, _unit_costs


    def set_topolinks(self, topolinks = None):
        """
        set or reset data center topology links.
        """
        pass


    def get_start_nodes(self):
        return self._start_nodes

    def get_end_nodes(self):
        return self._end_nodes
    
    def get_capacities(self):
        return self._capacities
    
    def get_unit_costs(self):
        return self._unit_costs


if __name__ == "__main__":
    # test
    a = DataCenterTopology([[0,2,3],[4,0,0],[1,2,0]])
    print(a.get_start_nodes())
