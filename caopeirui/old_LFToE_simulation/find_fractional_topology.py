from ortools.linear_solver import pywraplp
import numpy as np

def get_topology_by_LP(traffic, r_ingress, r_egress, bandwidth, pods_num):
    """
    Desc: According to predicted traffic matrix and other constraints, 
    use linear programming to get fractional topology d_ij matrix.

    Inputs:
        - traffic(numpy.narray): 2-dimension traffic matrix.
        - r_ingress(list): Number of ingress links of s_i. (List including pods_num+1 elements,
                           Because the 0 elements is not used)
        - r_egress(list): Number of egress links of s_i. (List including pods_num+1 elements)
        - bandwidth(2-dimension list): Link capacity(Gbps) between s_i and s_j.
        - pods_num(int): Number of pods.

    Returns:
        - d_ij(2-dimension list): Number of s_i egress links connected to igress links of s_j.
    """
    solver = pywraplp.Solver('FindFractionalTopology',
                            pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)

    u = solver.NumVar(-solver.infinity(), solver.infinity(), 'u')

    # traffic = traffic.reshape(1, np.prod(np.shape(traffic)))[0]

    # w_ikj denotes the actual traffic routed from pod s_i to pod s_j though s_k.
    # If k=0, w_i0j denotes the traffic routed from pod s_i to pod s_j directly.
    name_w_ikj = [
        f'w_{i}_{k}_{j}'
        for i in range(1, pods_num + 1)
        for k in range(pods_num + 1)
        for j in range(1, pods_num + 1)
        if i != k and i != j and j != k
    ]
    variables_w_ikj = {}
    for w in name_w_ikj:
        variables_w_ikj[w] = solver.NumVar(0, solver.infinity(), w)

    # d_ij denotes the number of links which s_i connected to s_j
    name_d_ij = [
        f'd_{i}_{j}'
        for i in range(1, pods_num + 1)
        for j in range(1, pods_num + 1)
        if i != j
    ]
    variables_d_ij = {}
    for d in name_d_ij:
        variables_d_ij[d] = solver.NumVar(0, solver.infinity(), d)

    for i in range(1, pods_num + 1):
        # Corresponding to the paper formula(1)
        # summation(d_ij) <= r_i_(e/in)gress
        constraint_dr_egress = solver.Constraint(-solver.infinity(), r_egress[i])
        constraint_dr_ingress = solver.Constraint(-solver.infinity(), r_ingress[i])

        for j in range(1, pods_num + 1):
            if i != j:
                # Corresponding to the paper formula(1)
                constraint_dr_egress.SetCoefficient(variables_d_ij[f'd_{i}_{j}'], 1)
                constraint_dr_ingress.SetCoefficient(variables_d_ij[f'd_{j}_{i}'], 1)

                # Corresponding to the paper formula(2)->1)
                # summation(w_p_ij) = u * t_ij  =>  summation(w_p) - u * t_ij
                constraint =  solver.Constraint(0, 0)
                constraint.SetCoefficient(u, -traffic[i - 1][j - 1])

                # Corresponding to the paper formula(2)->2)
                # summation(w_p_sisj) ≤ d_ij * b_ij  =>  d_ij * b_ij - summation(w_p) ≥ 0
                constraint_db = solver.Constraint(0, solver.infinity())
                constraint_db.SetCoefficient(variables_d_ij[f'd_{i}_{j}'], bandwidth[i - 1][j - 1])    
                for k in range(0, pods_num + 1):
                    if k != i and k != j:
                        # Corresponding to the paper formula(2)->1) 
                        constraint.SetCoefficient(variables_w_ikj[f'w_{i}_{k}_{j}'], 1)

                        # Corresponding to the paper formula(2)->2)
                        if k != 0:
                            constraint_db.SetCoefficient(variables_w_ikj[f'w_{k}_{i}_{j}'], -1)
                            constraint_db.SetCoefficient(variables_w_ikj[f'w_{i}_{j}_{k}'], -1)
                        else:
                            constraint_db.SetCoefficient(variables_w_ikj[f'w_{i}_{0}_{j}'], -1)


    # Create the objective function, maximize u.
    objective = solver.Objective()
    objective.SetCoefficient(u, 1)
    objective.SetMaximization()
    # Call the solver and display the results.
    solver.Solve()
    print('Solution:')
    print('u = ', u.solution_value())

    d_ij = [[0] * pods_num for _ in range(pods_num)]
    for i in range(1, pods_num + 1):
        for j in range(1, pods_num + 1):
            if i != j:
                d_ij[i - 1][j - 1] = variables_d_ij[f'd_{i}_{j}'].solution_value()
    return d_ij


if __name__ == '__main__':
    # ------test------
    a_sample_traffic = np.random.rand(10, 10) * 100
    # The actual used subscripts start at 1
    r_ingress =  [0] + [128] * 10
    r_egress =  [0] + [128] * 10
    bandwidth = [[30] * 10 for _ in range(10)]

    res = get_topology_by_LP(a_sample_traffic, r_ingress, r_egress, bandwidth, 10)
    print(res)