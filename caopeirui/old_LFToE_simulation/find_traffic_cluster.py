import numpy as np
# from sklearn.cluster import KMeans

class TrafficKMeans:
    """
    k-means cluster algorithm for traffic matrix.
    if model input(data matrix) is m*n dimensional matrix,
    the result of k-means cluster will be k rows, every of which represents a predicting result.

    provide two methods of input data:
    1. directly input data matrix.  (use set_data_mat() or __init__())
    2. incrementally add some sample data, which is more than one dimension.  (use add_a_sample())
    """
    _data_mat = []
    _have_data_mat_flag = False    # to avoid conflict between set_data_mat() and add_a_sample()
    _record_sample_shape = ()      # to recover sample shape
    _k = 1

    def __init__(self, k = 1, data_mat = None):
        self._k = k
        self.set_data_mat(data_mat)


    def set_data_mat(self, data_mat = None):
        if data_mat is not None:
            # TODO: exception handling. eg:  raise ValueError('xxx')
            self._data_mat = data_mat
            self._have_data_mat_flag = True


    def clear(self):
        """
        clear a TrafficKMeans object
        """
        self._data_mat = []
        self._record_sample_shape = ()
        self._have_data_mat_flag = False


    def add_a_sample(self, sample_mat):
        """
        when the dimension of samples is more than 1-dim,
        transform its shape to one row data and then append it to _data_mat.
        that is, m*n matrix is changed to 1*n matrix 
        """
        if self._have_data_mat_flag == True:
            raise ValueError('already have data_mat')

        if type(sample_mat) != np.ndarray:
            sample_mat = np.array(sample_mat)

        self._record_sample_shape = np.shape(sample_mat)
        one_row_data = sample_mat.reshape(1, np.prod(self._record_sample_shape))
        self._data_mat.append(one_row_data.tolist()[0])


    def euclid_distance(self, vec_a, vec_b):
        """
        call Euclidean distance.
        """
        return np.sqrt(np.sum(np.power(vec_a - vec_b, 2)))


    def random_centroid(self, data_mat = None, k = None):
        """
        create k random centroids for data set.
        """
        col_size = np.shape(data_mat)[1]
        centroids = np.mat(np.zeros((k, col_size)))

        # let centroids be limited within one-dimensional boundary
        for i in range(col_size):
            lower_bound = np.min(data_mat[:, i])
            range_bound = float(np.max(data_mat[:, i]) - lower_bound)
            centroids[:, i] = np.mat(lower_bound + range_bound * np.random.rand(k, 1))

        return centroids

    def cluster_result(
        self,
        k = None,
        distance_algorithm = 'euclid_distance',
        centroid_algorithm = 'random_centroid',
    ):
        """
        return k-means cluster result.
        [matrix(...), matrix(...), ...]
        """
        if k == None:
            k = self._k

        if type(self._data_mat) != np.mat:
            self._data_mat = np.mat(self._data_mat)
        
        if distance_algorithm == 'euclid_distance':
            distance_algorithm = self.euclid_distance
        if centroid_algorithm == 'random_centroid':
            centroid_algorithm = self.random_centroid

        sample_num = np.shape(self._data_mat)[0]
        cluster_assign = np.mat(np.zeros((sample_num, 2)))
        centroids =centroid_algorithm(data_mat = self._data_mat, k = k)
        cluster_changed = True

        while cluster_changed == True:
            cluster_changed = False
            # iteratively assign each row data to nearest centroid.
            for i in range(sample_num):
                min_distance = np.inf
                min_index = -1

                # calculate the distance between one row data and every centroid
                # choose the minimum distance centroid as cluster assignment.
                for j in range(k):
                    to_centroid_distance = distance_algorithm(centroids[j, :], self._data_mat[i, :])
                    if to_centroid_distance < min_distance:
                        min_distance = to_centroid_distance
                        min_index = j
                
                if cluster_assign[i, 0] != min_index:
                    cluster_changed = True
                    cluster_assign[i, :] = min_index, min_distance ** 2

            # update centroids
            for cent in range(k):
                # get all row data belonging one cluster
                pts_in_clust = self._data_mat[np.nonzero(cluster_assign[:, 0].A == cent)[0]]
                centroids[cent, :] = np.mean(pts_in_clust, axis = 0)
        
        # recover sample shape
        result = []
        for i in range(k):
            result.append(centroids[i].reshape(self._record_sample_shape))
        
        return result, cluster_assign


if __name__ == "__main__":
    # ----test1
    # samples = np.mat([[1,1,4], #sample 1
    #                    [0,2,3],  #sample 2
    #                    [3,1,7],  #sample 3
    #                    [6,4,0],
    #                    [0,5,2],
    #                    [8,4,9]])
    # a = TrafficKMeans(k = 2, data_mat = samples)
    # cent, cluster = a.cluster_result()
    # print(cent)
    # print(cluster)

    # ----test2
    traffic_test = TrafficKMeans(k = 3)
    for i in range(5):
        a_sample = np.random.rand(10, 8) * 100
        traffic_test.add_a_sample(a_sample)
    cent, cluster = traffic_test.cluster_result()
    print(cent)
    print(cluster)