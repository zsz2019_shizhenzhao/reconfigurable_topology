import requests
import time, functools
import json

# TODO: Could move these variables to a configuration file.
OCS_URL = 'http://202.120.40.8:14080'
USER_NAME = 'admin'
USER_PASSWARD = 'xinruoshui19*'

def time_cost(fn):
    """
    Decs: Calc and print the time cost of fn.
    eg: 
    @time_cost
    def fn():
        ...
    """
    @functools.wraps(fn)
    def wrapper(*args, **kw):
        start_time = time.time()
        tmp = fn(*args, **kw)
        end_time = time.time()
        print('%s executed in %s s' % (fn.__name__, end_time - start_time))
        return tmp
    return wrapper


@time_cost
def add_a_connect(in_port, out_port, direction = 'Uni'):
    """
    Desc: Add cross connection for OCS.
    Parameters:
        - in_port (str), eg: '1.2.1'
        - out_port (str), eg: '1.3.2'
        - direction (str) supported values is 'Uni' or 'Bi' representing unidirectional or bidirectional direction respectively.
    Return: The string info of the request's response.
    """
    s = requests.Session()
    s.auth = (USER_NAME, USER_PASSWARD)
    connects_data = {
        'id': 'add',
        'in': in_port,
        'out': out_port,
        'dir': direction,
    }
    r = s.post(OCS_URL + '/rest/crossconnects/', data = json.dumps(connects_data))

    return r.text


@time_cost
def add_many_connects(in_out_list):
    """
    Desc: 
    Inputs:
        - in_out_list([[in_port, out_port], [in_port, out_port], ...]): 
            2-dimension list, both in_port and out_port are string type.
    Return: The string info of the request's response.
    """
    s = requests.Session()
    s.auth = (USER_NAME, USER_PASSWARD)
    connects_data = []

    #TODO: Invalid inputs processing 
    for [in_port, out_port] in in_out_list:
        connects_data.append({
            'id': 'add',
            'in': in_port,
            'out': out_port,
            'dir': 'Uni',
        })

    r = s.post(OCS_URL + '/rest/crossconnects/?id=badd', data = json.dumps(connects_data))

    return r.text

@time_cost
def get_info():
    """
    Desc: Get cross connection list of OCS
    Return: The string info of the request's response.
    """
    s = requests.Session()
    s.auth = (USER_NAME, USER_PASSWARD)
    r = s.get(OCS_URL + '/rest/crossconnects/?id=list')
    return r.text


def delete_all_connects():
    """
    Desc:
    """
    pass


def delete_a_connect():
    """
    Desc:
    """
    pass


if __name__ == "__main__":
    # res = add_connect('1.3.1', '1.4.1')
    # print(res)
    in_out_list = []
    for i in range(1, 5 + 1):
        for j in range(1, 8 + 1):
            in_out_list.append([f'{i}.{j}.1', f'{i}.{j}.2'])
    res = add_many_connects(in_out_list)
    print(res)
    res = get_info()
    print(res)
    print(len(in_out_list))
    pass