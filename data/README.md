# Traffic Data

This directory contains some data used in the simulation.

---

- `data` contains (square) traffic matrix.

  - data type: `ndarray` in python

  - data size: ($n \times n $), n is the number at the end of the folder's name. 

- `flowTable` contains traffic flow log.

  - data type: serialized data, produced using `protobuf`. It has four attributes: src, dst, time, flowSize.
