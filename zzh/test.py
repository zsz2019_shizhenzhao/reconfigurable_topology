import numpy as np
from sklearn.decomposition import PCA
import sys
sys.path.append("..")
from caopeirui.LFToE_simulation.LP_routing import LinearProgrammingRouting 
from root_const import ROOT_PATH
import gurobipy as grb

# configerations
pod_num = 9
num_of_principal_components = 5
topo = np.array([[20] * pod_num] * pod_num)
conf_file = f'{ROOT_PATH}/config/pods_config.csv'
solver = LinearProgrammingRouting(topo)
solver.load_init_config(conf_file)
capacity = solver.get_bandwidth() * solver._topology_pods
dire = "/home/zzh/reconfigurable_topology/zzh/cluster2" 

def add_noise(routing, noise_par):
    noise = 1 + np.random.uniform(0, noise_par, size = (pod_num, pod_num, pod_num))
    routing_noisy = routing * noise

    for i in range(pod_num):
        for j in range(pod_num):
            wsum = sum(routing_noisy[i, j])
            if wsum == 0: 
                wsum += 1
            routing_noisy[i,j] = np.array([x/wsum for x in routing_noisy[i,j]])
    
    error_sum = 0
    cnt = 0
    for i in range(pod_num):
        for j in range(pod_num):
            for k in range(pod_num):
                if routing[i][j][k] != 0:
                    cnt += 1
                    error_sum += (abs(routing_noisy[i][j][k]-routing[i][j][k]) / routing[i][j][k])
    # print("routing noise:", error_sum/cnt)
    return routing_noisy

def calculate_up_down_traffic(traffic_matrix, routing_noisy):
    up_down_traffic = np.zeros(shape=(pod_num*2,))
    for i in range(pod_num):
        up_down_traffic[i] += traffic_matrix[i].sum()
    for j in range(pod_num):
        up_down_traffic[j+pod_num] += traffic_matrix[:,j].sum()
    for i in range(pod_num):
        for i in range(pod_num):
            for relay in list(range(min(i,j))) + list(range(min(i,j)+1, max(i, j))) + list(range(max(i,j)+1, pod_num)):
                up_down_traffic[relay] += traffic_matrix[i, j] * routing_noisy[i, j, relay]
                up_down_traffic[pod_num+relay] += traffic_matrix[i, j] * routing_noisy[i, j, relay]
    # print("up and down traffic:", up_down_traffic)
    return up_down_traffic

def routing_dict_2_array(routing_dict):
    routing_array = np.zeros(shape=(pod_num, pod_num, pod_num))
    for key in routing_dict.keys():
        i, j, k = key.split('_')[1:]
        if j == '0':
            routing_array[int(i)-1, int(k)-1, int(k)-1] = routing_dict[key]    
        else:
            routing_array[int(i)-1, int(k)-1, int(j)-1] = routing_dict[key]
    return routing_array

def routing_array_2_coefficient(routing_array):
    coefficient = np.zeros(shape=(pod_num * 2, pod_num ** 2))

    # first n equations: the upward traffic of n pods
    for i in range(pod_num):
        # from pod i to any other pod
        for j in range(pod_num * i, pod_num * (i + 1)):
            coefficient[i, j] = 1
        # pod i as a relay
        for start in list(range(i)) + list(range(i+1, pod_num)):
            for end in list(range(i)) + list(range(i+1, pod_num)):
                coefficient[i, start*pod_num + end] = routing_array[start,end,i]
    
    # last n equations: the downward traffic of n pods
    for i in range(pod_num, pod_num*2):
        # from any other pod to pod i
        for j in range(pod_num):
            coefficient[i, j * pod_num + i-pod_num] = 1
        # pod i as a ralay
        for start in list(range(i-pod_num)) + list(range(i-pod_num+1, pod_num)):
            for end in list(range(i-pod_num)) + list(range(i-pod_num+1, pod_num)):
                coefficient[i, start*pod_num + end] = routing_array[start,end,i-pod_num]
    
    # print("coefficient:", coefficient)
    return coefficient

def traffic_history_pca(history_array, num):
    meanRemoved = - history_array + np.mean(history_array, axis=0)
    covMat = np.cov(meanRemoved, rowvar=0)
    eigVals, eigVects = np.linalg.eig(np.mat(covMat))
    eigVallnd = np.argsort(-eigVals)
    eigVallnd = eigVallnd[:num]
    selectedEigVects = eigVects[:,eigVallnd]
    return np.array(selectedEigVects.T) 
    
    # history_array = history_array.T
    # pca = PCA(n_components = num)
    # res = pca.fit_transform(history_array)
    # return res.T

def optimization(coefficient, PC, up_down_traffic, num_of_pc):
    # print(PC[0],PC[1],PC[2])
    # print(PC.shape, coefficient.shape, up_down_traffic.shape)

    m = grb.Model("nameless")
    m.Params.LogToConsole = 0
        
    # ||Co * t - up_down_traffic||

    # t
    names_traffic = [
        f't_{i*pod_num+j}'
        for i in range(pod_num)
        for j in range(pod_num)
    ]
    t = m.addVars(names_traffic, lb=0, vtype=grb.GRB.CONTINUOUS, name='t')

    names_coef = [f'c_{i}' for i in range(num_of_pc)]
    coef = m.addVars(names_coef, vtype = grb.GRB.CONTINUOUS, name='coef')
    
    m.addConstrs(
        grb.quicksum(
            coef[f'c_{k}'] * PC[k][i*pod_num+j]
            for k in range(num_of_pc) 
        ) - t[f't_{i*pod_num+j}'] == 0
            for i in range(pod_num)
            for j in range(pod_num)
    )

    def diff_expr(i):
        return grb.quicksum(coefficient[i][index] * t[f't_{index}']
                for index in range(pod_num**2)) - up_down_traffic[i]
    
    m.setObjective(
        grb.quicksum(            
                diff_expr(i) * diff_expr(i)
                for i in range(2*pod_num)
        ),
        sense=grb.GRB.MINIMIZE
    )
    
    m.optimize()
    if m.status == grb.GRB.Status.OPTIMAL:
        solution = m.getAttr('X', t)
        traffic = {}
        for t_name in names_traffic:
            traffic[t_name] = solution[t_name]        
        res = np.array([traffic['t_{}'.format(i)] for i in range(pod_num**2)])
        return res
    else:
        print('No solution')

def traffic_normalization(traffic_array):
    egress_traffic = traffic_array.sum(axis = 1)
    ingress_traffic = traffic_array.sum(axis = 0)
    egress_max = egress_traffic.max()
    ingress_max = ingress_traffic.max()
    norm_baseline = max(egress_max, ingress_max)
    return traffic_array / norm_baseline

def run():
    history = list()
    for i in range(1, 401):
        file = "{}/traffic{}.npy".format(dire, str(i))
        mat = np.load(file)
        norm_mat = traffic_normalization(mat)
        arr = norm_mat.reshape(-1)
        history.append(arr)

    history_array = np.array(history)
    principal_components = traffic_history_pca(history_array, num_of_principal_components)

    # traffic_matrix = np.load("{}/traffic{}.npy".format(dire, 200))
    traffic_matrix = traffic_normalization(np.load("{}/traffic{}.npy".format(dire, 400)))
    routing_dict = solver.traffic_engineering_grb(traffic_matrix)[0]
    routing_array = routing_dict_2_array(routing_dict)
    routing_noisy = add_noise(routing_array, 0.3)
    coefficient = routing_array_2_coefficient(routing_array)

    ####################################################

    err_sum = 0
    for i in range(401, 1459):
        # next traffic matrix comes
        # traffic_matrix = np.load("{}/traffic{}.npy".format(dire, i))
        traffic_matrix = traffic_normalization(np.load("{}/traffic{}.npy".format(dire, i)))
        up_down_traffic = calculate_up_down_traffic(traffic_matrix, routing_noisy)
        # coefficient = routing_array_2_coefficient(routing_array)
        
        if i % 10 == 0:
            del history[0]
            file = "{}/traffic{}.npy".format(dire, str(i))
            mat = np.load(file)
            norm_mat = traffic_normalization(mat)
            arr = mat.reshape(-1)
            history.append(arr)
            print("New measurement is added to history.")
            # do PCA to traffic history
            history_array = np.array(history)
            principal_components = traffic_history_pca(history_array, num_of_principal_components)

        # calculate traffic matrix based on coefficient, principal components and up-down traffic
        res = optimization(coefficient, principal_components, up_down_traffic, num_of_principal_components)
        
        vec = traffic_matrix.reshape(-1)
        err = np.mean(abs((res - vec) / vec))
        err_sum += err
        print("matrix {} mean error:{}".format(i, err))
        
        # traffic_matrix = res.reshape(pod_num, pod_num)
        # routing_dict = solver.traffic_engineering_grb(traffic_matrix)[0]
        # routing_array = routing_dict_2_array(routing_dict)
        # routing_noisy = add_noise(routing_array, 0.3)

    print(err_sum / 1259)

run()