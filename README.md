# reconfigurable_topology

## 目录约定
本工程项目中，各自写的程序新建一个文件夹管理。

utils文件夹下为提供动态拓扑结构基本框架以及通用函数。

config文件夹存放配置文件。

data文件夹存放中间数据，以numpy.save保存npy格式为主，命名统一（尽量体现数据用途和尺寸），如：某种方法产生的流量矩阵，解出来的routing矩阵等。

为避免子文件夹中相对路径引用的混乱，以及不同电脑下调试的绝对路径修改的冗余，统一使用根目录路径常量`ROOT_PATH`。

eg:
```python
# 按照工程约定设置好PYTHONPATH
from root_const import ROOT_PATH
# 可以直接拼接字符串使用，如:
obj.load_init_config(f'{ROOT_PATH}/config/pods_config.csv')
```


## 工程约定
网上虽然有一些在程序中引入搜索路径的方法，如：`sys.path.append()` ，但都需要使用相对路径跳来跳去，不科学。

故在没有将此工程做到python第三方库的情况下，推荐使用PYTHONPATH的方式导入一下工程目录到python路径环境变量里。

Windows添加环境变量方法在高级系统设置的环境变量中。

linux，mac下在用户目录下添加到`~/.bashrc` 或其他配置文件如`.bash_profile`或`.zshrc`

```bash
export PYTHONPATH="/xxx/reconfigurable_topology"
```

xxx换成reconfigurable_topology文件夹父路径。

不重启终端的话，需要`source ~/.bashrc`。

之后不管在各自写的文件夹什么路径想调用utils包里面的框架基类，都可以使用如下语法：
```python
from utils.base import DcnBase
```

## 数据或配置约定
最外层配置文件用csv文件，列字段代表同一种类下的每个属性值，如:

r_ingress | r_egress | bandwidth
---|---|---
128 | 128| 10 
128 | 128 | 100
64 | 64 | 100


纵向可以接纳数据中心规模的扩大，横向可以无干扰拓展输入参数。

各模块间数据对接大多是traffic matrix，代码中使用numpy的二维array，故用numpy.save保存的npy文件对接最方便。全部存储于根目录下的data文件夹，npy文件命名表示出矩阵维度。


### routing计算结果约定
routing流量分配比计算结果，不同于traffic矩阵直接二维array存储，routing可以多跳，若用list或array存，会出现索引偏差或者占位混乱的问题。为了满足未来拓展需要，约定统一用字典存储，键为字符串，如 'w_1_3_2' ，其值表示 pod1 经过 pod3 传到 pod2 的routing流量分配比; 中间出现0表示不经过中转，允许四跳时，'w_1_0_0_0_2' 键对应的值表示 pod1 直传 pod2；一般只用最多两跳即可。

求 link utilization 即可用如下方法，具有较强可读性和可控性:
```python
link_utilization =  [[None] * pods_num for _ in range(pods_num)]
for i in range(1, pods_num + 1):
    for j in range(1, pods_num + 1):
        if i != j:
            w_sum = 0
            for k in range(pods_num + 1):
                if k != i and k != j:
                    if k == 0:
                        w_sum += w_routing[f'w_{i}_0_{j}'] * traffic[i - 1][j - 1]
                    else:
                        w_sum += w_routing[f'w_{k}_{i}_{j}'] * traffic[k - 1][j - 1]
                        w_sum += w_routing[f'w_{i}_{j}_{k}'] * traffic[i - 1][k - 1]
            
            link_utilization[i - 1][j - 1] = w_sum / capacities[i - 1][j - 1]
```

### data 数据约定
单个traffic文件，直接读取即可。

含有连续多个traffic的npy文件，读取需要再取出第一个位置的dict，其中key是时间戳字符串，value即np.array格式的traffic，如：
```python
traffic_path = f'{ROOT_PATH}/data/g_data/8pod_traffic.npy'
traffic_history = np.load(traffic_path, allow_pickle = True)[0]

TRAFFIC_SEQ = []
for timestamp, traffic in traffic_history.items():
    TRAFFIC_SEQ.append(traffic / 300 * 8 / 1000000000)
```

## 仿真文件重点介绍
`caopeirui/LFToE_simulation/preprocess.py` 读取数据，整理traffic，可以按一定周期选取traffic。该文件处理含了对代表性traffic矩阵的求解，和对代表性traffic的scale。

`caopeirui/LFToE_simulation/find_fractional_topology.py` LP + min(sum(w2)) ToE

`caopeirui/LFToE_simulation/find_topo_by_mean_var.py` 利用统计量mean来设计最少links topo后，再依次填满links。

`caopeirui/LFToE_simulation/LP_routing.py` LP + w/capacity 这种 sensitivity 的 routing

`caopeirui/LFToE_simulation/LP_routing_w2.py` LP + min(sum(w2)) 的rouing

`caopeirui/LFToE_simulation/LP_routing_sen.py` LP + 新的引入了 SVD 分解的rouing

`caopeirui/LFToE_simulation/LP_routing_sen_w2.py` LP + 新的引入了 SVD 分解的rouing后，再做 min(sum(w2)) 的rouing

`caopeirui/LFToE_simulation/LP_routing_oldsen_w2.py` LP + w/capacity 这种 sensitivity 的 routing 后，再做 min(sum(w2)) 的rouing

`caopeirui/LFToE_simulation/test` 文件夹中调用
- `caopeirui/LFToE_simulation/test/2ToE_self_TE_fixed1topo.py` 对比几种不同ToE方法，得到topo后，用两周后 t1 的 traffic 做 routing，evaluate t2 的 traffic。
    - 仿真结果输出到`./result/2ToE_self_TE_fixed1topo.py.log`
    - 画图对应`./draw_evaluation.py`
- `caopeirui/LFToE_simulation/test/compareLP.py` 用我们的ToE方法后，用几种不同的rouing方式（`LP_routing_等等`）来做evaluate。
    - 仿真结果输出到`./result/compareLP.py.log`
    - 画图对应`./draw_compareLP.py`
