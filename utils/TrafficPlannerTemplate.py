"""
流量生成器Traffic Planner基本框架
"""
import numpy as np
import pandas as pd 
from traffic.run.TrafficPlanner import addFlow
import os
import random


class TrafficPlanner:
    _name = ""                                              # Module name
    _pairDistribution = 0                                   # Traffic pair distribution
    _pairCollection = {}                                    # Traffic pair collection
    _podid = {}                                             # Table of pod id that each ToR belongs to
    _pod_traffic = np.array([])                             # Pod-level traffic matrix
    _tor_traffic = np.array([])                             # ToR-level traffic matrix
    _podcount = 0                                           # Total number of pods
    _torcount = 0                                           # Total number of ToRs
    _properties = ""                                        # Properties of traffic planner
    _flowSizeDistribution = None                            # Flow size distribution

    def __init__(self, config_file):
        self.load_init_config(config_file)
        pass

    def load_init_config(self, config_file): 
        """
        Desc: Read configurations from .csv file
              And store them in _properties variable
        """
        self._name = os.path.splitext(os.path.split(config_file)[1])[0]
        self._properties = pd.read_csv(config_file, index_col=0)  

        pod_config_file = self._properties.loc['config_file', 'value']
        pod_config = pd.read_csv(pod_config_file)

        # Load Topology configuration
        self.set_pod_config(pod_config)
        
    def setPairProbAllToAllFraction(self):
        print("Generating fractional pair probabilities between nodes...")
        fractionX = 0.19
        prob_idx = 0
        numofChosen = int(round(self._torcount * fractionX))

        rand = np.random.randint(0, self._torcount, size=(100))

        pairProb = 1 / (numofChosen * (numofChosen-1))
        for src in range(numofChosen):
            for dst in range(numofChosen):
                if src != dst:
                    prob_idx += pairProb
                    self._pairCollection[prob_idx] = [rand[src], rand[dst]]

        print("done.")
        return 0
    
    def set_pod_config(self, config):
        tors_per_pod = np.array(config['tors_per_pod'])
        self._podcount = len(tors_per_pod)
        self._torcount = np.sum(tors_per_pod)
        self._pod_traffic = np.zeros((self._podcount, self._podcount))
        self._tor_traffic = np.zeros((self._torcount, self._torcount))

        current_pod = 0
        count = 0
        for i in range(self._torcount):
            if count == tors_per_pod[current_pod]:
                current_pod += 1
                count = 0
            self._podid[i] = current_pod
            count += 1

    def setPairProbAllToAll(self):
        print("Generating all-to-all pair probabilities between all nodes...")
        prob_idx = 0
        # Uniform probability for every server pair
        pdfNumBytes = 1.0 / (self._torcount * (self._torcount - 1))
        
        # Add uniform probability for every pair
        for src in range(self._torcount):
            for dst in range(self._torcount):
                if src != dst:
                    prob_idx += pdfNumBytes
                    self._pairCollection[prob_idx] = [src, dst]
        print("done.")

    def choosePair(self):
        """
        Desc: Function to randomly choose a traffic pair
              The chosen pair will be assigned traffic flow
        """
        pass

    def ceiling_item(self, key):
        sorted_key = sorted(self._pairCollection)
        for i in range(len(sorted_key)):
            target = sorted_key[i]
            if i == len(sorted_key) - 1:
                return self._pairCollection[target]
            if target <= key < sorted_key[i + 1]:
                return self._pairCollection[target]

    def createPlan(self, durationNs, logger):
        """
        Desc: Traffic plan strategy here
        Inputs: 
            - durationNs: int. The duration time in nanosecond
            - logger: file object. Log file to save detailed information
        """
        pass 

    def registerFlow(self, time, srcId, dstId, src_pod, dst_pod, flowSizeByte, logger):
        if src_pod != dst_pod:
            self._pod_traffic[src_pod, dst_pod] += flowSizeByte
        self._tor_traffic[srcId, dstId] += flowSizeByte
        addFlow.addFlow(time, srcId, dstId, flowSizeByte, logger)

    def get_tor_traffic(self):
        return self._tor_traffic

    def get_pod_traffic(self):
        return self._pod_traffic

    def getSimLabel(self):
        return "_"+self._name+"_{}".format(self._torcount)