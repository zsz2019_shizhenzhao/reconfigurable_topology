from utils.base import DcnBase

class TopologyEngineering(DcnBase):
    def topology(self, config_file, *args, **kwargs):
        """
        :description: 计算topology
        :param
        - config_file: 网络的配置文件
        :return: 
        - d_ij: n*n 的矩阵 每两个pod之间连接数
        """

        self.load_init_config(config_file=config_file)
        