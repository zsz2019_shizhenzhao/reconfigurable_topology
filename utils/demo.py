from ortools.linear_solver import pywraplp
from utils.base import DcnBase
import numpy as np

class FindTopology(DcnBase):
    def topology_engineering(self):
        """
        Desc: According to the number of pods and pods' ingress/egress, and bandwidth,
              find the topology of all pods' connection by LP.
        
        Returns: (u.solution_value(), d_ij)
            - u.solution_value(): 1/(max link utilization)
            - d_ij: Denote the number of s_i egress links connected to ingress links of s_j.
        """
        if self._traffic_count == 0:
            raise ValueError('''
                No traffic input has been set yet, 
                please call add_traffic() firstly.
            ''')
        solver = pywraplp.Solver('FindFractionalTopology',
                                pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)

        u = solver.NumVar(0, solver.infinity(), 'u')

        # d_ij denotes the number of links which s_i connected to s_j
        name_d_ij = [
            f'd_{i}_{j}'
            for i in range(1, self._pods_num + 1)
            for j in range(1, self._pods_num + 1)
            if i != j
        ]
        variables_d_ij = {}
        for d in name_d_ij:
            variables_d_ij[d] = solver.NumVar(0, solver.infinity(), d)

        # Add all constraints for every input traffic matrix
        for multiple in range(0, self._traffic_count):
            a_traffic = self._traffic_sequence[multiple]
            # w_ikj denotes the actual traffic routed from pod s_i to pod s_j though s_k.
            # If k=0, w_i0j denotes the traffic routed from pod s_i to pod s_j directly.
            name_w_ikj = [
                f'{multiple}_w_{i}_{k}_{j}'
                for i in range(1, self._pods_num + 1)
                for k in range(self._pods_num + 1)
                for j in range(1, self._pods_num + 1)
                if i != k and i != j and j != k
            ]
            variables_w_ikj = {}
            for w in name_w_ikj:
                variables_w_ikj[w] = solver.NumVar(0, solver.infinity(), w)

            for i in range(1, self._pods_num + 1):
                # Corresponding to the paper formula(1)
                # summation(d_ij) <= r_i_(e/in)gress
                constraint_dr_egress = solver.Constraint(-solver.infinity(), int(self._r_egress[i - 1]))
                constraint_dr_ingress = solver.Constraint(-solver.infinity(), int(self._r_ingress[i - 1]))

                for j in range(1, self._pods_num + 1):
                    if i != j:
                        # Corresponding to the paper formula(1)
                        constraint_dr_egress.SetCoefficient(variables_d_ij[f'd_{i}_{j}'], 1)
                        constraint_dr_ingress.SetCoefficient(variables_d_ij[f'd_{j}_{i}'], 1)

                        # Corresponding to the paper formula(2)->1)
                        # summation(w_p_ij) = u * t_ij  =>  summation(w_p) - u * t_ij
                        constraint =  solver.Constraint(0, 0)
                        constraint.SetCoefficient(u, -a_traffic[i - 1][j - 1])

                        # Corresponding to the paper formula(2)->2)
                        # summation(w_p_sisj) ≤ d_ij * b_ij  =>  d_ij * b_ij - summation(w_p) ≥ 0
                        constraint_db = solver.Constraint(0, solver.infinity())
                        constraint_db.SetCoefficient(variables_d_ij[f'd_{i}_{j}'], int(self._bandwidth[i - 1][j - 1]))   
                        for k in range(0, self._pods_num + 1):
                            if k != i and k != j:
                                # Corresponding to the paper formula(2)->1) 
                                constraint.SetCoefficient(variables_w_ikj[f'{multiple}_w_{i}_{k}_{j}'], 1)

                                # Corresponding to the paper formula(2)->2)
                                if k != 0:
                                    constraint_db.SetCoefficient(variables_w_ikj[f'{multiple}_w_{k}_{i}_{j}'], -1)
                                    constraint_db.SetCoefficient(variables_w_ikj[f'{multiple}_w_{i}_{j}_{k}'], -1)
                                else:
                                    constraint_db.SetCoefficient(variables_w_ikj[f'{multiple}_w_{i}_{0}_{j}'], -1)


        # Create the objective function, maximize u.
        objective = solver.Objective()
        objective.SetCoefficient(u, 1)
        objective.SetMaximization()
        # Call the solver and display the results.
        solver.Solve()
        # print('Solution:')
        # print('u = ', u.solution_value())

        d_ij = [[0] * self._pods_num for _ in range(self._pods_num)]
        for i in range(1, self._pods_num + 1):
            for j in range(1, self._pods_num + 1):
                if i != j:
                    d_ij[i - 1][j - 1] = variables_d_ij[f'd_{i}_{j}'].solution_value()

        return u.solution_value(), d_ij


if __name__ == "__main__":
    # ------demo test------
    pods_num = 4
    first_sample_traffic = np.random.rand(pods_num, pods_num) * 100
    second_sample_traffic = np.random.rand(pods_num, pods_num) * 100
    third_sample_traffic = np.random.rand(pods_num, pods_num) * 100

    obj = FindTopology()
    obj.load_init_config('/Users/cpr/Desktop/project/reconfigurable_topology/config/pods_config.csv')
    
    obj.add_a_traffic(first_sample_traffic)
    obj.add_a_traffic(second_sample_traffic)
    obj.add_a_traffic(third_sample_traffic)
    u, d_ij = obj.topology_engineering()
    print(u)
    print(d_ij)

