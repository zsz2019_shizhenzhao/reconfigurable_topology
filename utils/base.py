"""
提供动态拓扑结构基本框架以及通用函数
"""
import pandas as pd
import numpy as np
import math

def cosine_similarity(x, y, norm=False):
    assert len(x) == len(y), "len(x) != len(y)"
    zero_list = [0] * len(x)
    if x == zero_list or y == zero_list:
        return float(1) if x == y else float(0)
    res = np.array([[x[i] * y[i], x[i] * x[i], y[i] * y[i]] for i in range(len(x))])
    cos = sum(res[:, 0]) / (np.sqrt(sum(res[:, 1])) * np.sqrt(sum(res[:, 2])))

    # 余弦值的范围是 [-1,+1] ，if norm 把值归一化到 [0,1]
    return 0.5 * cos + 0.5 if norm else cos

def data_norm(data):
    tmp = np.array(data)

    return (tmp - tmp.min()) / (tmp.max() - tmp.min())


class DcnBase:
    _traffic_count = 0      # 流量矩阵序列中流量矩阵个数
    _traffic_sequence = []  # 流量矩阵序列（3维），时间顺序存储
    _traffic = []           # 单个流量矩阵（2维）
    _r_ingress = []         # 每个pod的ingress link数
    _r_egress = []          # 每个pod的egress link数
    _port_bandwidth = []    # 每个pod的端口带宽
    _bandwidth = []         # 第i到j个pod的带宽（2维）
    _pods_num = 0           # pod总数
    _topology_pods = []     # 第i个到第j个pod的link数（2维），整数解

    _d_ij = []              # 中间变量，第i个到第j个pod的link数（2维），非整数解
    

    def __init__(self):
        pass


    def load_init_config(self, config_file):
        """
        Desc: 从csv配置文件中读取各属性值，并变换成后续模块需要用到的数据
              转换后的数据，无论几维，统一化为np.ndarray类型
        """
        pd_config = pd.read_csv(config_file)
        # TODO: 错误或无效配置输入处理
        self._r_ingress = np.array(pd_config['r_ingress'])
        self._r_egress = np.array(pd_config['r_egress'])
        self._port_bandwidth = np.array(pd_config['port_bandwidth'])
        self._pods_num = len(self._r_egress)

        # 将读取的配置文件内容，变换为后续模块需用到的数据形式
        # 第i到j个pod的带宽，两者取其小
        all_bandwith = []
        for bi in range(self._pods_num):
            tmp = []
            for bj in range(self._pods_num):
                tmp.append(min(self._port_bandwidth[bi], self._port_bandwidth[bj]))
            all_bandwith.append(tmp)
        self._bandwidth = np.array(all_bandwith)


    def set_traffic(self, traffic):
        """
        Desc: 设置单个流量矩阵
        """
        self._traffic = traffic


    def add_a_traffic(self, traffic):
        """
        Desc: 添加一个流量矩阵到流量矩阵序列里
        Inputs:
            - traffic(numpy.narray): 2维流量矩阵.
        """
        # TODO: exception handling. eg:  raise ValueError('xxx')
        #       For example, traffic matrix dimensions aren't 2,
        #       or the number of rows and columns of traffic matrix is not consistent.
        self._traffic_sequence.append(traffic)
        self._traffic_count += 1


    def set_traffic_sequence(self, traffic_sequence):
        """
        Desc: 设置多个矩阵的集合序列
        Inputs:
            - traffic_sequence: 3维流量矩阵
        """
        self._traffic_sequence = traffic_sequence
        self._traffic_count = len(self._traffic_sequence)


    def set_d_ij(self, d_ij):
        """
        Desc: 设置第i个到第j个pod的link数（2维）中间变量，非整数解
        """
        if not isinstance(d_ij, np.ndarray):
            self._d_ij = np.array(d_ij)
        else:
            self._d_ij = d_ij


    def get_bandwidth(self):
        return self._bandwidth


    def get_pods_egress_capacity(self):
        return self._port_bandwidth * self._r_egress


    def get_pods_ingress_capacity(self):
        return self._port_bandwidth * self._r_ingress


    def get_traffic_sequence(self):
        return self._traffic_sequence


    def get_pods_num(self):
        return self._pods_num


    def traffic_engineering(self):
        """
        Desc: 由子类实现流量工程，解出新的traffic
        """
        pass


    def topology_engineering(self):
        """
        Desc: 由子类实现拓扑工程，解出拓扑结构，如每个pod或ocs中连接数。
        """
        pass

    def routing(self, traffic):
        """
        Desc: 计算routing
        """
        pass

    def satisfy_physical_bound(self, topo_links):
        """当前topo连接情况，满足所有物理约束则返回 True
           否则返回 False
        """
        ingress = topo_links.sum(axis = 0)   # 每列求和
        egress = topo_links.sum(axis = 1)  # 每行求和
        
        flag = True
        for i in range(self._pods_num):
            # links数若超界了，返回 False
            if ingress[i] > self._r_ingress[i] or egress[i] > self._r_egress[i]:
                flag = False
                break
        return flag

    

    def get_capacities(self):
        topology_pods = np.array(self._topology_pods)
        bandwidth = np.array(self._bandwidth)
        capacities = topology_pods * bandwidth
        return capacities



    def calc_link_utilization(self, traffic):
        """
        Desc: 计算当前结构中的链路利用率
        Inputs: 
            - traffic(np.array) 流量矩阵
        Returns:
            (每条link上lu, 最大lu, 平均lu)
        """
        w_routing = self._w_routing
        topology_pods = np.array(self._topology_pods)
        bandwidth = np.array(self._bandwidth)
        capacities = topology_pods * bandwidth

        link_utilization =  [[None] * self._pods_num for _ in range(self._pods_num)]
        alu_numerator = 0
        alu_denominator = 0
        for i in range(1, self._pods_num + 1):
            for j in range(1, self._pods_num + 1):
                if i != j:
                    w_sum = 0
                    for k in range(self._pods_num + 1):
                        if k != i and k != j:
                            if k == 0:
                                w_sum += w_routing[f'w_{i}_0_{j}'] * traffic[i - 1][j - 1]
                            else:
                                w_sum += w_routing[f'w_{k}_{i}_{j}'] * traffic[k - 1][j - 1]
                                w_sum += w_routing[f'w_{i}_{j}_{k}'] * traffic[i - 1][k - 1]
                    
                    if capacities[i - 1][j - 1] > 0:
                        link_utilization[i - 1][j - 1] = w_sum / capacities[i - 1][j - 1]
                        # 此处计算类比于，若有 200g link 和 10g link 同时存在
                        # 将 200g link 看成 20 个 10g link
                        # 故分子为 link_utilization[i - 1][j - 1] * capacities[i - 1][j - 1] 即 w_sum
                        alu_numerator += w_sum
                        alu_denominator += capacities[i - 1][j - 1]
                    else:
                        if w_sum < 0.000001:
                            link_utilization[i - 1][j - 1] = 0.0
                        else:
                            print("Traffic allocated to edges with no capacity!")
                            exit(0)

        all_link_utilization = np.array([
            link_utilization[j][i]
            for j in range(len(link_utilization[0]))
            for i in range(len(link_utilization))
            if link_utilization[j][i] is not None
            # and not math.isnan(link_utilization[j][i])
        ])
        norm_alu = alu_numerator / alu_denominator
        return link_utilization, all_link_utilization.max(), all_link_utilization.mean(), norm_alu


    def calc_normal_link_utilization(self, traffic):
        """
        Desc: 计算当前结构中的归一化的最大链路利用率
        Inputs: 
            - traffic(np.array) 流量矩阵
        Returns:
            (link_utilization, norm_mlu, alu, mlu)
        """

        topology_pods = np.array(self._topology_pods)
        bandwidth = np.array(self._bandwidth)
        capacities = topology_pods * bandwidth

        link_utilization, mlu, alu, norm_alu = self.calc_link_utilization(traffic)
        output = traffic.sum(axis = 1)
        norm_output = output / capacities.sum(axis = 1)
        max_norm_output = norm_output.max()
        in_put = traffic.sum(axis = 0)
        norm_input = in_put / capacities.sum(axis = 0)
        max_norm_input = norm_input.max()
        if max_norm_input < max_norm_output:
            norm_mlu = mlu / max_norm_output
        else:
            norm_mlu = mlu / max_norm_input

        return link_utilization, mlu, alu, norm_mlu, norm_alu

    def ave_hop_count(self, traffic):
        w_routing = self._w_routing
        topology_pods = np.array(self._topology_pods)
        bandwidth = np.array(self._bandwidth)
        capacities = topology_pods * bandwidth
        link_utilization, _, _, _ = self.calc_link_utilization(traffic)
        sum_lu_cap = 0
        sum_traffic = 0
        for i in range(1, self._pods_num + 1):
            for j in range(1, self._pods_num + 1):
                if i != j:
                    sum_lu_cap += link_utilization[i - 1][j - 1] * capacities[i - 1][j - 1]
                    sum_traffic += traffic[i - 1][j - 1]
        
        res = sum_lu_cap / sum_traffic
        return res 


    def get_allocated_traffic(self, traffic):
        w_routing = self._w_routing

        allocated_traffic = np.zeros((self._pods_num, self._pods_num))
        for i in range(1, self._pods_num + 1):
            for j in range(1, self._pods_num + 1):
                if i != j:
                    w_sum = 0
                    for k in range(self._pods_num + 1):
                        if k != i and k != j:
                            if k == 0:
                                w_sum += w_routing[f'w_{i}_0_{j}'] * traffic[i - 1][j - 1]
                            else:
                                w_sum += w_routing[f'w_{k}_{i}_{j}'] * traffic[k - 1][j - 1]
                                w_sum += w_routing[f'w_{i}_{j}_{k}'] * traffic[i - 1][k - 1]
                    
                    allocated_traffic[i - 1][j - 1] = w_sum
        return allocated_traffic


    def to_integer_topo(self, d_ij):
        """取出每个元素的小数部分，从大到小在对应原位置依次向上取整。直到填充满。
        """
        ori_shape = d_ij.shape

        # 先拉平边成list
        flat_topo = d_ij.flatten().tolist()
        # print(flat_topo)
        # 取小数部分，并且用字典存储，用原位置索引作key
        index_frac_dict = {}
        for i in range(len(flat_topo)):
            index_frac_dict[i] = math.modf(flat_topo[i])[0]

        # 字典用sorted排序后输出的结果是list，里面存的是tuple
        index_frac_list = sorted(index_frac_dict.items(), key = lambda x : x[1], reverse = True)
        
        # 所有元素向下取整
        integer_topo = np.floor(d_ij)

        # print(integer_topo)
        egress_floor_sum = np.sum(integer_topo, axis=1)
        ingress_floor_sum = np.sum(integer_topo, axis=0)
        res_r_egress = self._r_egress - egress_floor_sum
        res_r_ingress = self._r_ingress - ingress_floor_sum
        # print(res_r_egress)
        # print(res_r_ingress)

        for (index, _) in index_frac_list:
            row = math.floor(index / self._pods_num)
            column = index % self._pods_num
            if row == column:
                continue
            if res_r_egress[row] > 0 and res_r_ingress[column] > 0:
                res_r_egress[row] -= 1
                res_r_ingress[column] -= 1
                integer_topo[row][column] += 1
        if self.satisfy_physical_bound(integer_topo) == False:
            print('Error when mapping fractional topology to integer topology!')
            exit(0)
        
        # print(d_ij)
        # print(integer_topo)
        # print('row sum', np.sum(integer_topo, axis=1))
        # print('column sum', np.sum(integer_topo, axis=0))

        """
        res = [math.floor(i) for i in flat_topo]

        for (index, frac) in index_frac_list:
            res[index] = res[index] + 1
            # 对角线位置不连link
            for i in range(self._pods_num):
                if index == i * self._pods_num + i:
                    res[index] = 0
            # 若不满足物理egress和ingress约束，减回去。看别的位置。
            tmp = np.array(res).reshape(ori_shape)
            if self.satisfy_physical_bound(tmp) == False:
                res[index] = res[index] - 1
        
        integer_topo = np.array(res).reshape(ori_shape)
        """

        self._d_ij = integer_topo
        return integer_topo


    def fill_residual_links(self, topo_links):
        while self.satisfy_physical_bound(topo_links) == True:
            # 想要让让两个np.array独立，必须用 copy 或 deepcopy。
            last_topo_links = topo_links.copy()
            for i in range(self._pods_num):
                for j in range(self._pods_num):
                    # 单条link能加的情况才加
                    if i != j:
                        topo_links[i][j] += 1
                        if self.satisfy_physical_bound(topo_links) == False:
                            # 加之后不满足条件，则减回去
                            topo_links[i][j] -= 1            
            have_change = False
            for i in range(self._pods_num):
                for j in range(self._pods_num):
                    if topo_links[i][j] != last_topo_links[i][j] and i != j:
                        have_change = True
            if have_change == False:
                break
        self._d_ij = topo_links
        return topo_links


if __name__ == "__main__":
    obj = DcnBase()
    obj.load_init_config('~/Desktop/project/reconfigurable_topology/config/pods_config.csv')
    
