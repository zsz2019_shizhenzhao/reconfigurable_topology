import matplotlib
import matplotlib.pyplot as plt
import random
import numpy as np

COLORS = ['b', 'g', 'y', 'c', 'm', 'y', 'k', 'w', 'r']
MARKERS = ['.', ',', 'o', 'v', '^', '<', '>', '1', '2', '3', '4', '8', 's', 'p', 'P', '*', 'h', 'H', '+', 'x', 'X', 'D',
           'd', '|', '_']
LINE_SYTPES = ['-', '--', '-.', ':']
# 固定随机种子，选到好看的组合可以记下了
random.seed(24)
FMT = ['g-', 'r--', 'm-.', 'k:', '#0D0D0D', '#9400D3']
# FMT = ['g-', 'r--', 'b-.', 'k:']
# FMT = [random.choice(COLORS) + LINE_SYTPES[i % 4] for i in range(10)]

def draw_cdf_hist(**kwargs):
    """借助直方图hist绘制CDF图
    Input: 接受任意数量的关键字参数，key充当画图的图例，value是画图用的原始数据
    """
    plt.figure()

    for k, data in kwargs.items():
        plt.hist(x = data, label = k, cumulative = True, normed = True, histtype = 'step')
    
    plt.legend()
    plt.title('CDF hist')
    # plt.show()
    return plt


def draw_cdf(**kwargs):
    """绘制CDF图
    Input: 接受任意数量的关键字参数，key充当画图的图例，value是画图用的原始数据
    """
    plt.figure()

    select = 0
    # 适配曲线数量
    curve_num = len(kwargs.keys())
    for k, data in kwargs.items():
        x = sorted(data)
        y = []
        size = len(x)
        for i in range(size):
            y.append((i + 1) / size)
        plt.plot(x, y, FMT[select % curve_num], label = k)

        # delta = random.random() * 0.2
        # ran_index = int(random.random() * size)
        # plt.annotate(
        #     k, 
        #     xy = (x[ran_index], y[ran_index]),
        #     xytext = (x[ran_index] + delta, y[ran_index] + delta),
        #     arrowprops = dict(arrowstyle='->')
        # )
        select += 1

    plt.legend()
    plt.title('CDF')
    return plt


def draw_line_graph(**kwargs):
    """绘制line图
    Input: 接受任意数量的关键字参数，key充当画图的图例，value是画图用的原始数据
    value 如果是一维的，直接充当纵坐标值来画图
    value 如果是二维的，第一行充当横坐标，第二行充当纵坐标
    """
    plt.figure()
    select = 0
    for k, data in kwargs.items():
        data = np.array(data)
        if len(data.shape) == 1:
            plt.plot(data, LINE_SYTPES[select % 4], label = k)
        if len(data.shape) == 2:
            plt.plot(data[0], data[1], LINE_SYTPES[select % 4], label = k)
        select += 1
    plt.legend()
    plt.title('line graph')
    return plt

def draw_point_graph(**kwargs):
    """绘制散点图
    Input: 接受任意数量的关键字参数，key充当画图的图例，value是画图用的原始数据
    value 如果是一维的，直接充当纵坐标值来画图
    value 如果是二维的，第一行充当横坐标，第二行充当纵坐标
    """
    plt.figure()
    select = 0
    for k, data in kwargs.items():
        data = np.array(data)
        if len(data.shape) == 1:
            plt.plot(data, MARKERS[select % 20], label = k)
        if len(data.shape) == 2:
            plt.plot(data[0], data[1], MARKERS[select // 10 + 2], markersize=15, alpha=0.5, label = k)
        select += 1
    plt.legend()
    plt.title('point graph')
    return plt


if __name__ == "__main__":
    a = [1,2,3,4]
    c = [0.5598668347324363,3,2,1.23,1.23,0.12,1]
    b = [5,5,2,3]
    ret = draw_line_graph(haha=[a,b], mlu=a, again_mlu=b)
    # ret = draw_cdf(c=c, mlu=a, again_mlu=b)
    ret.show()
    pass