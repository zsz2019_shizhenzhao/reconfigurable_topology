from utils.base import DcnBase

class TrafficEngineering(DcnBase):
    def routing(self, config_file, traffic, *args, **kwargs):
        """
        :description: 计算路由权重
        :param
        - traffic: 流量矩阵
        :return: 
        - w_ij: 链路流量占比的字典
        """
        
        # e.g. start_index = kwargs['start_index']
        self.load_init_config(config_file=config_file)
