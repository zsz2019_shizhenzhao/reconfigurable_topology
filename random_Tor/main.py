import random
import csv


class Tor:
    def __init__(self, port_num: int):
        self.degree = 0
        self.edges = []
        self.max_degree = port_num

    def add_edge(self, node):
        self.degree += 1
        self.edges.append(node)

    def is_full(self):
        return self.degree < self.max_degree

    def clear(self):
        self.degree = 0
        self.edges = []

    def check(self, node):
        return node in self.edges


class Net:
    def __init__(self, size: int, port_num: int):
        self.graph = [Tor(port_num) for i in range(size)]
        self.size = size
        self.port_num = port_num
        self.random()

    def random(self):
        node_set = list(range(self.size))
        graph = self.graph
        for x in range(self.port_num // 2):
            random.shuffle(node_set)
            random.shuffle(node_set)
            for i in range(self.size - 1):
                graph[node_set[i]].add_edge(node_set[i + 1])
                graph[node_set[i + 1]].add_edge(node_set[i])
            graph[node_set[0]].add_edge(node_set[self.size - 1])
            graph[node_set[self.size - 1]].add_edge(node_set[0])

    def jump_num(self):
        graph = self.graph
        size = self.size
        res = []
        for i in range(size):
            if graph[i].check(i):
                print('-------------------------')
        for i in range(size):
            new = graph[i].edges
            reach = set()
            hop = [len(reach)]
            while len(reach) < size - 1:
                tmp_new = []
                for node in new:
                    if node not in reach and node != i:
                        tmp_new += graph[node].edges
                        reach.add(node)
                new = set(tmp_new)
                hop.append(len(reach))
                if len(new) == 0:
                    break
            res.append(hop)
            print('\r' + f'{i+1}/{size}', end='', flush=True)
        return res

size = 10000
port_num = 8
a = Net(size, port_num)
res = a.jump_num()

with open(f'./{size}_{port_num}.csv','w',newline='')as f:
    f_csv = csv.writer(f)
    f_csv.writerows(res)
