import utils.paint as pt
import pandas as pd
from root_const import ROOT_PATH 
import numpy as np

def draw_mlu_cdf_from_log(file_name):
    pd_data = pd.read_csv(file_name)

    plt = pt.draw_cdf(
        mlu = pd_data['mlu'],
        mesh_mlu = pd_data['mesh_mlu'],
        random_mlu = pd_data['random_mlu'],
    )
    # plt.savefig(file_name + '_mlu.jpg')
    return plt

def draw_alu_cdf_from_log(file_name):
    pd_data = pd.read_csv(file_name)

    plt = pt.draw_cdf(
        alu = pd_data['alu'],
        mesh_alu = pd_data['mesh_alu'],
        random_alu = pd_data['random_alu'],
    )
    # plt.savefig(file_name + '_alu.jpg')
    return plt


def draw_mlualu_cdf_from_log(file_name):
    pd_data = pd.read_csv(file_name)
    plt = pt.draw_cdf(
        mlu = pd_data['mlu'],
        again_mlu = pd_data['again_mlu'],
        mesh_mlu = pd_data['mesh_mlu'],
        mesh_again_mlu = pd_data['mesh_again_mlu'],
    
        alu = pd_data['alu'],
        again_alu = pd_data['again_alu'],
        mesh_alu = pd_data['mesh_alu'],
        mesh_again_alu = pd_data['mesh_again_alu'],

    )
    plt.show()
    # plt.savefig(file_name + '_mlualuahc.jpg')
    return plt


def draw_line_graph_from_log(file_name):
    pd_data = pd.read_csv(file_name, header = 0)

    plt = pt.draw_line_graph(
        mlu = pd_data['mlu'],
        again_mlu = pd_data['again_mlu'],
        mesh_mlu = pd_data['mesh_mlu'],
        mesh_again_mlu = pd_data['mesh_again_mlu'],
        # random_mlu = pd_data['random_mlu'],
        # random_again_mlu = pd_data['random_again_mlu'],
        # ftmv_mlu = pd_data['ftmv_mlu'],
        # ftmv_again_mlu = pd_data['ftmv_again_mlu'],
        alu = pd_data['alu'],
        again_alu = pd_data['again_alu'],
        mesh_alu = pd_data['mesh_alu'],
        mesh_again_alu = pd_data['mesh_again_alu'],
        # random_alu = pd_data['random_alu'],
        # random_again_alu = pd_data['random_again_alu'],
        # ftmv_alu = pd_data['ftmv_alu'],
        # ftmv_again_alu = pd_data['ftmv_again_alu'],
    )
    plt.show()
    # plt.savefig(file_name + 'linegraph.jpg')
    return plt


def draw_evaluation():
    compare_dir = f'{ROOT_PATH}/simulation/result/compare/8pod_main_all_noscale2/'
    # mesh_data1 = pd.read_csv(compare_dir + 'ToE-mesh,.log', header = 0)
    # fairness_data1 = pd.read_csv(compare_dir + 'ToE-max_min_fairness,.log', header = 0)

    mesh_data = []
    fairness_data = []
    for i in range(5):
        mesh_data.append(pd.read_csv(compare_dir + f'start_index-{i*4032},ToE-mesh,.log', header = 0))
        fairness_data.append(pd.read_csv(compare_dir + f'start_index-{i*4032},ToE-max_min_fairness,.log', header = 0))
    
   
    all_mesh_data = pd.concat(mesh_data, axis = 0)
    all_fairness_data = pd.concat(fairness_data , axis = 0)

    print(all_mesh_data.shape)
    print(all_fairness_data.shape)
    plt = pt.draw_cdf(
        mlu = all_fairness_data['mlu'],
        alu = all_fairness_data['alu'],
        mesh_mlu = all_mesh_data['mlu'],
        mesh_alu = all_mesh_data['alu'],
    )
    plt.show()
    # plt.savefig(file_name + '_mlualuahc.jpg')
    return plt


if __name__ == "__main__":
    draw_evaluation()

