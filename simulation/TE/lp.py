import numpy as np
from utils.TE import TrafficEngineering
import math
import gurobipy as grb
from root_const import ROOT_PATH
from utils.linear_programming import LinearProgramming

class Routing(TrafficEngineering):
    """
    Desc: 流量工程做routing，顺便计算link utilization
    """
 
    _w_routing = {}
    
    def __init__(self, topology_pods):
        """
        Desc: 做routing前，除了需要加载基类的配置读取，配置其他参数
              还需要添加前面的拓扑工程计算出来的pods间连接数
        Inputs:
            - topology_pods(2-dimension list): Number of s_i egress links connected to igress links of s_j.
        """
        self._topology_pods = topology_pods


    
    def traffic_engineering(self, actual_traffic):
        """用gurobi来解
        """
        if not isinstance(actual_traffic, np.ndarray):
            traffic = np.array(actual_traffic)
        else:
            traffic = actual_traffic
    
        if not isinstance(self._bandwidth, np.ndarray):
            bandwidth = np.array(self._bandwidth)
        else:
            bandwidth = self._bandwidth

        pods_num = self._pods_num
        capacity = self._topology_pods * bandwidth

        m = grb.Model('traffic_engineering_grb')
        m.Params.OutputFlag = 0
        mlu = m.addVar(lb = 0, vtype = grb.GRB.CONTINUOUS, name='mlu')

        name_w_ikj = [
            f'w_{i}_{k}_{j}'
            for i in range(1, pods_num + 1)
            for k in range(pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != k and i != j and j != k
        ]
        w = m.addVars(name_w_ikj, lb = 0, ub = 1, vtype = grb.GRB.CONTINUOUS, name='w')

        # 没有链路的时候不能用流量
        m.addConstrs(
                grb.quicksum(
                    w[f'w_{k}_{i}_{j}'] + w[f'w_{i}_{j}_{k}']
                    if k != 0 else w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) == 0
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j and capacity[i - 1][j - 1] == 0 
            )

        # summation(w_ikj) = 1
        m.addConstrs(
            (grb.quicksum(
                w[f'w_{i}_{k}_{j}'] for k in range(0, pods_num + 1)
                if k != i and k != j
            ) == 1
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j),
            name = 'sumOneConstrs'
        )
        # summation(T_ij*w_) <= u * capacity
        if len(traffic.shape) == 2:
            # 单个流量矩阵下的约束
            m.addConstrs(
                grb.quicksum(
                    traffic[k - 1][j - 1] * w[f'w_{k}_{i}_{j}']
                    + traffic[i - 1][k - 1] * w[f'w_{i}_{j}_{k}']
                    if k != 0 else traffic[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) <= mlu * capacity[i - 1][j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j
            )
        elif len(traffic.shape) == 3:
            # 用多个流量矩阵来约束
            for t in traffic:
                m.addConstrs(
                    grb.quicksum(
                        t[k - 1][j - 1] * w[f'w_{k}_{i}_{j}']
                        + t[i - 1][k - 1] * w[f'w_{i}_{j}_{k}']
                        if k != 0 else t[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                        for k in range(0, pods_num + 1)
                        if k != i and k != j
                    ) <= mlu * capacity[i - 1][j - 1]
                    for i in range(1, pods_num + 1)
                    for j in range(1, pods_num + 1)
                    if i != j
                )
        print(capacity)
        print(traffic)
        print(traffic.sum(axis=1))
        print(traffic.sum(axis=0))
        m.setObjective(mlu, grb.GRB.MINIMIZE)
        # m.write('debug.lp')
        m.optimize()
        if m.status == grb.GRB.Status.OPTIMAL:
            # print(m.objVal)
            solution = m.getAttr('X', w)
            w_routing = {}
            for w_name in name_w_ikj:
                w_routing[w_name] = solution[w_name]
            self._w_routing = w_routing

            print(m.objVal) # test
            exit()# test
            return w_routing, m.objVal
        else:
            print('No solution')

    def again_routing(self, actual_traffic, max_link_utilization):
        """利用上一步解出来的u来重新优化routing结果
        """
        if not isinstance(actual_traffic, np.ndarray):
            traffic = np.array(actual_traffic)
        else:
            traffic = actual_traffic
    
        if not isinstance(self._bandwidth, np.ndarray):
            bandwidth = np.array(self._bandwidth)
        else:
            bandwidth = self._bandwidth

        pods_num = self._pods_num
        capacity = self._topology_pods * bandwidth

        m = grb.Model('again_routing')
        m.Params.OutputFlag = 0

        name_w_ikj = [
            f'w_{i}_{k}_{j}'
            for i in range(1, pods_num + 1)
            for k in range(pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != k and i != j and j != k
        ]
        w = m.addVars(name_w_ikj, lb = 0, ub = 1, vtype = grb.GRB.CONTINUOUS)


        # 没有链路的时候不能用流量
        m.addConstrs(
                grb.quicksum(
                    w[f'w_{k}_{i}_{j}'] + w[f'w_{i}_{j}_{k}']
                    if k != 0 else w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) == 0
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j and capacity[i - 1][j - 1] == 0 
            )
        
        # summation(w_ikj) = 1
        m.addConstrs(
            grb.quicksum(
                w[f'w_{i}_{k}_{j}'] for k in range(0, pods_num + 1)
                if k != i and k != j
            ) == 1
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j
        )
        # summation(T_ij*w_) <= u * capacity
        if len(traffic.shape) == 2:
            # 单个流量矩阵下的约束
            m.addConstrs(
                grb.quicksum(
                    traffic[k - 1][j - 1] * w[f'w_{k}_{i}_{j}']
                    + traffic[i - 1][k - 1] * w[f'w_{i}_{j}_{k}']
                    if k != 0 else traffic[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) <= max_link_utilization * capacity[i - 1][j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j
            )
        elif len(traffic.shape) == 3:
            # 用多个流量矩阵来约束
            for t in traffic:
                m.addConstrs(
                    grb.quicksum(
                        t[k - 1][j - 1] * w[f'w_{k}_{i}_{j}']
                        + t[i - 1][k - 1] * w[f'w_{i}_{j}_{k}']
                        if k != 0 else t[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                        for k in range(0, pods_num + 1)
                        if k != i and k != j
                    ) <= max_link_utilization * capacity[i - 1][j - 1]
                    for i in range(1, pods_num + 1)
                    for j in range(1, pods_num + 1)
                    if i != j
                )
        
        max_w = m.addVar(lb = 0, ub = 1, vtype = grb.GRB.CONTINUOUS)

        # m.addConstrs(w[w_ikj] <= max_w for w_ikj in name_w_ikj)
        m.addConstrs(
            w[f'w_{i}_{0}_{j}'] <= max_w * capacity[i - 1][j - 1]
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j
        )
        m.addConstrs(
            w[f'w_{i}_{k}_{j}'] <= max_w * min(
                capacity[i - 1][k - 1], capacity[k - 1][j - 1])
            for k in range(1, pods_num + 1)
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j and k != i and k != j
        )
        
        m.setObjective(max_w, grb.GRB.MINIMIZE)

        m.optimize()
        if m.status == grb.GRB.Status.OPTIMAL:
            # print('agaim routing, max derivative_w:', m.objVal)
            solution = m.getAttr('X', w)
            w_routing = {}
            for w_name in name_w_ikj:
                w_routing[w_name] = solution[w_name]
            self._w_routing = w_routing
            return m.objVal
        else:
            print('again_routing No solution')



    def routing(self, config_file, traffic, *args, **kwargs):
        super().routing(config_file, traffic, *args, **kwargs)
        # w_routing, mlu = self.traffic_engineering(traffic)
        w_routing, mlu = self.linear_programming_routing(traffic)
        # w_routing, mlu = self.again_routing(traffic, mlu)
        return w_routing


    def linear_programming_routing(self, actual_traffic):
        """用自己封装的类来解
        """
        if not isinstance(actual_traffic, np.ndarray):
            traffic = np.array(actual_traffic)
        else:
            traffic = actual_traffic
    
        if not isinstance(self._bandwidth, np.ndarray):
            bandwidth = np.array(self._bandwidth)
        else:
            bandwidth = self._bandwidth

        pods_num = self._pods_num
        capacity = self._topology_pods * bandwidth

        lp = LinearProgramming()
        lp.add_var('mlu', 0, None)
        name_w_ikj = [
            f'w_{i}_{k}_{j}'
            for i in range(1, pods_num + 1)
            for k in range(pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != k and i != j and j != k
        ]
        for name in name_w_ikj:
            lp.add_var(name, 0, 1)
        
        for i in range(1, pods_num + 1):
            for j in range(1, pods_num + 1):
                if i != j:
                    constraint_str = f'1*w_{i}_{0}_{j}'
                    for k in range(1, pods_num + 1):
                        if k != i and k != j:
                            constraint_str += f'+ 1*w_{i}_{k}_{j}'
                    constraint_str += '== 1'
                    lp.add_constraint(constraint_str)
                    print(constraint_str)

        for j in range(1, pods_num + 1):
            for j in range(1, pods_num + 1):
                if i != j:
                    constraint_str = f'{traffic[i - 1][j - 1]} * w_{i}_{0}_{j}'
                    for k in range(1, pods_num + 1):
                        if k != i and k != j:
                            constraint_str += f'+ {traffic[k - 1][j - 1]} * w_{k}_{i}_{j} + {traffic[i - 1][k - 1]} * w_{i}_{j}_{k}'
                    constraint_str += f'+ -{capacity[i - 1][j - 1]}*mlu <= 0'
                    lp.add_constraint(constraint_str)
                    print(constraint_str)
        print(traffic)
        lp.add_objective('1*mlu')
        # print(lp._var_dict)
        print('_A_ub: ', lp._A_ub)
        print('_b_ub: ', lp._b_ub)
        print('_A_eq: ', lp._A_eq)
        print('_b_eq: ', lp._b_eq)
        print('_c: ', lp._c)
        print('_bounds_list: ', lp._bounds_list)
        # res = lp.solve('revised simplex')
        res = lp.solve()
        print(res)
        exit()
        

if __name__ == "__main__":
    pass
