# Simulation

## 目录结构

- TE 中是不同的 Traffic Engineering 类
    - routing 是 TE 的统一接口
        - 读取 config 配置文件获取网络信息
        - 根据给定的 traffic 及其他参数计算路由
    
- ToE 中是不同的 Topology Engineering 方法
    - topology 是 TE 的统一接口
        - 读取 config 配置文件获取网络信息
        - 根据给定的参数计算网络拓扑

- main 中是仿真的主程序
    - config.py 存储仿真参数
        - CONFIG  变量中存放仿真基本参数
        - LIST 中存放对比仿真时，依次组合变化的参数
    - main.py 的运行:
```python
    -m / --many 运行对比仿真
        无此参数运行单一仿真
    
    -f 'filename' / --filename='filename' 仿真结果的命名前缀
        若运行的是对比仿真，则是文件夹的名字
        若不指定文件名，按照系统时间生成文件名

eg:
    python3 main.py -f test
    将会在 simulation/result/single 中生成 test.log 存仿真结果 和 test.json 存当前仿真参数，
    这里的 test 仅仅用于命名，具体调用模型需在 config.py 中配置 CONFIG 字典。

    python3 main.py -m -f test 
    这样是执行的是CONFIG字典中单一配置，和 LIST 中的依次组合配置。
    会生成 simulation/result/compare/test 文件夹，里面存放不同组合配置下的仿真结果。
```


- result 中是仿真的结果
    - single: 单一仿真每次生成两个文件
        - .log是仿真的结果
        - .json是仿真时的参数
    - compare: 对比仿真会在该文件夹下生成一个文件夹,内有
        - BASE.json是对比中不变的参数
        - CHANGE.json是对比的参数
        - 一组.log文件,按照对比的参数命名

- figure 中存放仿真结果的绘图

- draw 存放绘图程序

- preprocess 预处理数据

## 代码接口

- TE
routing方法首先应调用基类的routing读取配置文件等操作
由于不同的TE类所需的参数不同,用**kwargs统一接口,调用参数时可用如下方法:

```python
def routing(self, config_file, traffic, *args, **kwargs):
    start_index = kwargs['start_index']
    window_size = kwargs['window_size']
    # do something with start index and window_size
```

- ToE
topology方法先调用基类的topology读取配置文件等操作
