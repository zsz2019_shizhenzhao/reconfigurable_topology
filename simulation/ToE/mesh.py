import numpy as np
from utils.ToE import TopologyEngineering
from root_const import ROOT_PATH

class Topology(TopologyEngineering):
    """
    Desc: 类似均匀连接的mesh topology结构
    """
    def get_connects_matrix(self):
        """
        Desc: 每个pod的物理连线相同情况下
        """
        pods_num = self._pods_num
        d_ij = np.ones((pods_num, pods_num))
        d_ij = d_ij * self._r_egress[0] / (pods_num - 1)
        for i in range(pods_num):
            d_ij[i][i] = 0

        d_ij = self.to_integer_topo(d_ij)
        return d_ij

    def get_connects_matrix_unevenly(self):
        """
        Desc: 得到mesh topology的pods间连接数矩阵
        """
        x_ij = np.zeros((self._pods_num, self._pods_num), dtype=np.int)
        pods_links = self._r_egress + self._r_ingress
        # 在list中用tuple存放每个pods对应links数，方便后续排序
        pods_num_links = [(i, pods_links[i]) for i in range(len(pods_links))]
        # 端口数从小到大排序
        pods_num_links = sorted(pods_num_links, key = lambda t: t[1])

        # 先从端口数量少的pod向其他pod分配连接数
        for i in range(self._pods_num - 1):
            current_pod = pods_num_links[i][0]
            # 当前pod列取出来，计算已占用端口数
            used_links = x_ij[:, current_pod].sum() + x_ij[current_pod, :].sum()
            current_links = pods_num_links[i][1] - used_links

            # 这个pod连到其他pod的连接数向下取整
            remain_cnt = self._pods_num - 1 - i
            mean_links = current_links // remain_cnt
            # 存入结果矩阵中
            for j in range(i + 1, self._pods_num):
                # 剩下的pod编号
                dst_pod = pods_num_links[j][0]
                # print(dst_pod)
                x_ij[current_pod][dst_pod] += mean_links // 2
                x_ij[dst_pod][current_pod] += mean_links // 2

        return x_ij


    def topology(self, config_file, *args, **kwargs):
        super().topology(config_file, *args, **kwargs)
        return self.get_connects_matrix()



if __name__ == "__main__":
    pass