import numpy as np

def traffic_scale(traffic: np.ndarray, decay_factor: int = 1):
    tm = traffic.copy()
    row_num, col_num = traffic.shape
    res = np.zeros((row_num, col_num), dtype=np.float)

    row_sum = np.sum(tm, axis=1)
    col_sum = np.sum(tm, axis=0)

    target_value = max(np.max(row_sum), np.max(col_sum))

    row_target = np.ones(row_num, dtype=np.float) * target_value
    col_target = np.ones(col_num, dtype=np.float) * target_value

    while np.sum(row_sum) > 0.0001 and np.sum(col_sum) > 0.0001:
        row_scale = row_sum / row_target
        col_scale = col_sum / col_target
        max_row = np.argmax(row_scale)
        max_col = np.argmax(col_scale)

        if row_scale[max_row] > col_scale[max_col]:
            row_sum[max_row] = 0
            row_target[max_row] = -1
            item = tm[max_row].copy()
            scaled_item = tm[max_row] / row_scale[max_row]
            for j in range(col_num):
                if col_target[j] > 0:
                    col_sum[j] -= item[j]
                    col_target[j] -= scaled_item[j]
            
            res[max_row] += scaled_item * decay_factor

            tm[max_row] = 0
        else:
            col_sum[max_col] = 0
            col_target[max_col] = -1
            item = tm[:, max_col].copy()
            scaled_item = tm[:, max_col] / col_scale[max_col]
            for i in range(row_num):
                if row_target[i] > 0:
                    row_sum[i] -= item[i]
                    row_target[i] -= scaled_item[i]
            
            res[:, max_col] += scaled_item * decay_factor

            tm[:, max_col] = 0
    return res



if __name__ == "__main__":
    a = np.random.rand(30,30)
    # print(a)
    res = traffic_scale(a)
    print(np.sum(res, axis=1))
    print(np.sum(res, axis=0))
