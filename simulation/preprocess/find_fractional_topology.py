from ortools.linear_solver import pywraplp
from utils.base import DcnBase
import numpy as np
import gurobipy as grb
import math


class FindFractionalTopology(DcnBase):
    def get_fractional_topology(self):
        """
        Desc: According to the number of pods and pods' ingress/egress, and bandwidth,
              find the topology of all pods' connection by LP.
        
        Returns: (u.solution_value(), d_ij)
            - u.solution_value(): 1/(max link utilization)
            - d_ij: Denote the number of s_i egress links connected to ingress links of s_j.
        """
        if self._traffic_count == 0:
            raise ValueError('''
                No traffic input has been set yet, 
                please call add_traffic() firstly.
            ''')
        pods_num = self._pods_num
        bandwidth = self._bandwidth
        
        m = grb.Model('get_fractional_topology')
        m.Params.OutputFlag = 0
        alpha = m.addVar(lb = 0, vtype = grb.GRB.CONTINUOUS, name='alpha')
        # topology_pods denotes the number of links which s_i connected to s_j
        topology_pods = m.addVars(pods_num, pods_num, vtype = grb.GRB.CONTINUOUS, name = 'd_ij')
        # summation(d_ij) <= r_i_(e/in)gress
        m.addConstrs(
            grb.quicksum(
                topology_pods[i, j] for j in range(pods_num) if i != j
            ) <= self._r_egress[i]
            for i in range(pods_num)
        )
        m.addConstrs(
            grb.quicksum(
                topology_pods[j, i] for j in range(pods_num) if i != j
            ) <= self._r_ingress[i]
            for i in range(pods_num)
        )
        m.addConstrs(
            topology_pods[i, j] == 0
            for i in range(pods_num)
            for j in range(pods_num)
            if i == j
        )

        # Add all constraints for every input traffic matrix
        for multiple in range(0, self._traffic_count):
            a_traffic = self._traffic_sequence[multiple]
            m.addConstrs(
                alpha * a_traffic[i][j] <= topology_pods[i, j] * bandwidth[i][j]
                for i in range(pods_num)
                for j in range(pods_num)
                if i != j
            )

        m.setObjective(alpha, grb.GRB.MAXIMIZE)
        # m.write('topotest.lp')
        m.optimize()
        if m.status == grb.GRB.Status.OPTIMAL:
            solution = m.getAttr('X', topology_pods)
            d_ij = np.zeros((self._pods_num, self._pods_num), dtype=np.float)
            for i in range(pods_num):
                for j in range(pods_num):
                    d_ij[i][j] = solution[i, j]
            return m.objVal, d_ij
        else:
            print('fractional topology No solution')
            exit()


    def again_ToE(self, max_link_utilization, uncertatinty_constrs = True, small_flag = False):
        """利用上次的max link utilization重新解d_ij
           二分迭代求解，直到 y 不能再减小
        """
        upper_bound = 1 / self._bandwidth.max()
        lower_bound = 0
        res = None
        while upper_bound - lower_bound > 0.00001:
            y = (upper_bound + lower_bound) / 1
            # y = 1
            state, tmp_res = self._again_LP(max_link_utilization, y, uncertatinty_constrs, small_flag)
            # 后来不做多次二分!
            self._d_ij = tmp_res
            return tmp_res

            if state == 'No solution':
                lower_bound = y
                print('lower_bound', y)
            else:
                upper_bound = y
                res = tmp_res
                print('upper_bound', y)
        return res


    def _again_LP(self, max_link_utilization, y, uncertatinty_constrs, small_flag):
        """利用上次的max link utilization重新解d_ij
        objective: maxmize(summation(topology_pods[i, j]))
        """
        pods_num = self._pods_num
        bandwidth = self._bandwidth
        # # test
        # y = 0.0000002

        m = grb.Model('again_ToE')
        m.Params.OutputFlag = 0
        
        name_w_ikj = [
            f'w_{i}_{k}_{j}'
            for i in range(1, pods_num + 1)
            for k in range(pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != k and i != j and j != k
        ]
        # 此处w变量视为分配routing比例，充当约束，但解ToE时无需知道结果
        w = m.addVars(name_w_ikj, lb = 0, ub = 1, vtype = grb.GRB.CONTINUOUS, name = 'w')

        # summation(w_ikj) = 1
        m.addConstrs(
            grb.quicksum(
                w[f'w_{i}_{k}_{j}'] for k in range(0, pods_num + 1)
                if k != i and k != j
            ) == 1
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j 
        )

        topology_pods = m.addVars(pods_num, pods_num, vtype = grb.GRB.CONTINUOUS, name = 'x')
        # topology_pods = m.addVars(pods_num, pods_num, vtype = grb.GRB.INTEGER, name = 'x')
        # summation(d_ij) <= r_i_(e/in)gress
        m.addConstrs(
            grb.quicksum(
                topology_pods[i, j] for j in range(pods_num)
            ) <= self._r_egress[i]
            for i in range(pods_num)
        )
        m.addConstrs(
            grb.quicksum(
                topology_pods[j, i] for j in range(pods_num)
            ) <= self._r_ingress[i]
            for i in range(pods_num)
        )

        for traffic in self._traffic_sequence:
            # summation(T_ij*w_) <= u * capacity
            # w下标直接对应pods编号，bandwidth和topology_pods下标只有二维所以从0开始，对应w的下标减1
            m.addConstrs(
                grb.quicksum(
                    traffic[k - 1][j - 1] * w[f'w_{k}_{i}_{j}']
                    + traffic[i - 1][k - 1] * w[f'w_{i}_{j}_{k}']
                    if k != 0 else traffic[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) <= max_link_utilization * bandwidth[i - 1][j - 1] * topology_pods[i - 1, j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j
            )

        # m.addConstrs(
        #     w[f'w_{i}_{k}_{j}'] <= y * bandwidth[i - 1][j - 1] * topology_pods[i - 1, j - 1]
        #     for k in range(0, pods_num + 1)
        #     for i in range(1, pods_num + 1)
        #     for j in range(1, pods_num + 1)
        #     if i != j and i != k and k != j
        # )
        
        if uncertatinty_constrs == True:
            m.addConstrs(
                w[f'w_{i}_{0}_{j}'] <= y * bandwidth[i - 1][j - 1] * topology_pods[i - 1, j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j
            )
            m.addConstrs(
                w[f'w_{i}_{k}_{j}'] <= y * bandwidth[k - 1][j - 1] * topology_pods[k - 1, j - 1]
                for k in range(1, pods_num + 1)
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j and i != k and k != j
            )
            m.addConstrs(
                w[f'w_{i}_{k}_{j}'] <= y * bandwidth[i - 1][k - 1] * topology_pods[i - 1, k - 1]
                for k in range(1, pods_num + 1)
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j and i != k and k != j
            )
        
        # 对角线置0，其他位置不能为0，会让求解变慢
        # m.addConstrs(
        #     topology_pods[i, j] == 0
        #     if i == j else topology_pods[i, j] >= 1
        #     for i in range(pods_num)
        #     for j in range(pods_num)
        # )
        m.addConstrs(
            topology_pods[i, j] == 0
            for i in range(pods_num)
            for j in range(pods_num)
            if i == j
        )
        # 让链接数尽可能多
        # m.setObjective(grb.quicksum(
        #     topology_pods[i, j]
        #     for i in range(pods_num)
        #     for j in range(pods_num)
        #     if i != j
        # ), grb.GRB.MAXIMIZE)

        # 此约束效果明显，让两跳routing尽量少，单跳links数随之增加
        if small_flag == True:
            m.setObjective(grb.quicksum(
                w[f'w_{i}_{k}_{j}'] * w[f'w_{i}_{k}_{j}']
                for k in range(1, pods_num + 1)
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j and i != k and j != k
            ), grb.GRB.MINIMIZE)
        else:
            m.setObjective(grb.quicksum(
                w[f'w_{i}_{k}_{j}'] * w[f'w_{i}_{k}_{j}']
                for k in range(1, pods_num + 1)
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j and i != k and j != k
            ), grb.GRB.MINIMIZE)

        m.optimize()
        # m.write('againToE.lp')
        if m.status == grb.GRB.Status.OPTIMAL:
            solution = m.getAttr('X', topology_pods)

            # 提供给第三次ToE
            self._w_routing = m.getAttr('X', w)
            
            x_ij = np.zeros((self._pods_num, self._pods_num), dtype=np.float)
            for i in range(pods_num):
                for j in range(pods_num):
                    x_ij[i][j] = solution[i, j]
            return 'Have solution', x_ij
        else:
            print('No solution')
            return 'No solution', None


    def third_ToE(self, max_link_utilization, d_ij):
        w = self._w_routing
        links_sum = d_ij.sum()
        pods_num = self._pods_num
        bandwidth = self._bandwidth

        m = grb.Model('third_ToE')
        m.Params.OutputFlag = 0

        topology_pods = m.addVars(pods_num, pods_num, vtype = grb.GRB.CONTINUOUS, name = 'x')
        # topology_pods = m.addVars(pods_num, pods_num, vtype = grb.GRB.INTEGER, name = 'x')

        # summation(d_ij) <= r_i_(e/in)gress
        m.addConstrs(
            grb.quicksum(
                topology_pods[i, j] for j in range(pods_num)
            ) <= self._r_egress[i]
            for i in range(pods_num)
        )
        m.addConstrs(
            grb.quicksum(
                topology_pods[j, i] for j in range(pods_num)
            ) <= self._r_ingress[i]
            for i in range(pods_num)
        )

        for traffic in self._traffic_sequence:
            # summation(T_ij*w_) <= u * capacity
            # w下标直接对应pods编号，bandwidth和topology_pods下标只有二维所以从0开始，对应w的下标减1
            m.addConstrs(
                grb.quicksum(
                    traffic[k - 1][j - 1] * w[f'w_{k}_{i}_{j}']
                    + traffic[i - 1][k - 1] * w[f'w_{i}_{j}_{k}']
                    if k != 0 else traffic[i - 1][j - 1] * w[f'w_{i}_{0}_{j}']
                    for k in range(0, pods_num + 1)
                    if k != i and k != j
                ) <= max_link_utilization * bandwidth[i - 1][j - 1] * topology_pods[i - 1, j - 1]
                for i in range(1, pods_num + 1)
                for j in range(1, pods_num + 1)
                if i != j
            )
        
        m.addConstrs(
            topology_pods[i, j] == 0
            for i in range(pods_num)
            for j in range(pods_num)
            if i == j
        )

        # m.addConstr(
        #     grb.quicksum(
        #         topology_pods[i, j]
        #         for i in range(pods_num)
        #         for j in range(pods_num)
        #         if i != j
        #     ) >= links_sum
        # )

        name_alpha = [
            f'a_{i}_{k}_{j}'
            for i in range(1, pods_num + 1)
            for k in range(pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != k and i != j and j != k
        ]
        alpha = m.addVars(name_alpha, lb = 0, ub = 10000, vtype = grb.GRB.CONTINUOUS, name = 'a')

        # alpha = m.addVar(lb = 0, vtype = grb.GRB.CONTINUOUS, name = 'a')
        
        allocated_traffic = self.get_allocated_traffic(self._traffic_sequence[0])

        m.addConstrs(
            allocated_traffic[i - 1][j - 1] + alpha[f'a_{i}_{0}_{j}'] * w[f'w_{i}_{0}_{j}'] \
                <= bandwidth[i - 1][j - 1] * topology_pods[i - 1, j - 1]
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j
        )

        m.addConstrs(
            allocated_traffic[k - 1][j - 1] + alpha[f'a_{i}_{k}_{j}'] * w[f'w_{i}_{k}_{j}'] \
                <= bandwidth[k - 1][j - 1] * topology_pods[k - 1, j - 1]
            for k in range(1, pods_num + 1)
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j and i != k and k != j
        )
        m.addConstrs(
            allocated_traffic[i - 1][k - 1] + alpha[f'a_{i}_{k}_{j}'] * w[f'w_{i}_{k}_{j}'] \
                <= bandwidth[i - 1][k - 1] * topology_pods[i - 1, k - 1]
            for k in range(1, pods_num + 1)
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j and i != k and k != j
        )

        m.setObjective(grb.quicksum(
            20000 * alpha[f'a_{i}_{k}_{j}'] - alpha[f'a_{i}_{k}_{j}'] * alpha[f'a_{i}_{k}_{j}']
            for k in range(0, pods_num + 1)
            for i in range(1, pods_num + 1)
            for j in range(1, pods_num + 1)
            if i != j and i != k and j != k
        ), grb.GRB.MAXIMIZE)

        m.optimize()
        # m.write('Third.lp')
        if m.status == grb.GRB.Status.OPTIMAL:
            solution = m.getAttr('X', topology_pods)
            x_ij = np.zeros((self._pods_num, self._pods_num), dtype=np.float)
            for i in range(pods_num):
                for j in range(pods_num):
                    x_ij[i][j] = solution[i, j]
            self._d_ij = x_ij
            return x_ij
        else:
            print('third ToE No solution')
            return None


if __name__ == "__main__":
    # ------demo test------
    pods_num = 8
    first_sample_traffic = np.random.rand(pods_num, pods_num) * 100000
    second_sample_traffic = np.random.rand(pods_num, pods_num) * 100000
    third_sample_traffic = np.random.rand(pods_num, pods_num) * 100000

    obj = FindFractionalTopology()
    obj.load_init_config('../../config/pods_config.csv')
    
    obj.add_a_traffic(first_sample_traffic)
    obj.add_a_traffic(second_sample_traffic)
    obj.add_a_traffic(third_sample_traffic)
    u, d_ij = obj.get_fractional_topology()
    d_ij = obj.to_integer_topo()
    print(u)
    print(d_ij)
    x_ij = obj.again_ToE(1 / u)
    print(x_ij)
    print(np.array(x_ij).sum())

