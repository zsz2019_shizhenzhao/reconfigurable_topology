import numpy as np
from root_const import ROOT_PATH
import simulation.preprocess.find_traffic_cluster as ftc
import simulation.preprocess.find_fractional_topology as fft
from utils.base import cosine_similarity
from utils.base import DcnBase


def traffic_norm(traffic_array, conf_file):
    """
    输入是二维 traffic array
    输出是归一化后的二维 traffic array
    """
    conf_obj = DcnBase()
    conf_obj.load_init_config(conf_file)
    # 每个port的bandwidth
    port_bandwidth = conf_obj.get_bandwidth()
    pod_num = conf_obj.get_pods_num()
    # 每个pod的进出的bandwidth，含了所有进端口或者出端口
    # 目前配置比较均匀，所以每个pod进端口数跟出端口数都相等
    pod_bandwidth = port_bandwidth[0][0] * conf_obj._r_egress[0]
    # -------method1 此法相当于没有针对每个pod差异归一化
    # 每个pod进出总流量除以端口带宽，全局最大的值作为归一化基准
    egress_traffic = traffic_array.sum(axis = 1) / pod_bandwidth
    ingress_traffic = traffic_array.sum(axis = 0) / pod_bandwidth
    egress_max = egress_traffic.max()
    ingress_max = ingress_traffic.max()
    norm_baseline = max(egress_max, ingress_max)

    del conf_obj

    return traffic_array / norm_baseline
    # --------
    
    # ------- method2
    # traffic 每个位置的行列和取最大除以端口带宽，作为归一化基准
    # 每个位置的baseline不同
    """egress_traffic = traffic_array.sum(axis = 1) / pod_bandwidth
    ingress_traffic = traffic_array.sum(axis = 0) / pod_bandwidth
    new_traffic = np.zeros((pod_num, pod_num), dtype = float)
    for i in range(pod_num):
        for j in range(pod_num):
            baseline = max(egress_traffic[i], ingress_traffic[j])
            if baseline != 0:
                new_traffic[i][j] = traffic_array[i][j] / baseline
            else:
                new_traffic[i][j] = traffic_array[i][j]

    return new_traffic"""
    # -------


def get_traffic(**kwargs):
    """
    统一供每个测试程序获取一系列流量矩阵
    从文件获取历史流量矩阵数据
    生成并返回用一周流量序列数据做k-means求出来的代表性矩阵 representative_traffic
    以及WINDOW_SIZE后的矩阵序列 future_traffic
    """
    start_index = kwargs['start_index']
    scale = kwargs['scale']
    kmeans_num = kwargs['kmeans_num']
    overprovision = kwargs['overprovision']
    traffic_file = kwargs['TRAFFIC_FILE']
    conf_file = kwargs['CONF_FILE']
    window_size = kwargs['WINDOW_SIZE']

    traffic_seq = get_ori_traffic_seq(traffic_file)
    # 当 WINDOW_SIZE = 4032，表示取出前两周 (5m * 4032 = 14 days)
    train_traffic = traffic_seq[start_index : start_index + window_size]
    future_traffic = traffic_seq[start_index + window_size : start_index + window_size * 2]

    # 用k-means找代表性流量矩阵
    obj_km = ftc.TrafficKMeans(k = kmeans_num)
    for a_traffic in train_traffic:
        # a_traffic = traffic_norm(a_traffic, conf_file)    # test norm
        obj_km.add_a_traffic(a_traffic)
    representative_traffic = obj_km.get_pred_traffic()

    # if scale == True:
        # 注释可调两种不同的scale方法
        # representative_traffic = scale_traffic(representative_traffic, conf_file, train_traffic)
        
        # from simulation.preprocess.TE_scale import traffic_scale
        # rep_traffic = []
        # for a_traffic in representative_traffic:
        #     ret_traffic = traffic_scale(a_traffic)
        #     rep_traffic.append(ret_traffic)
        # representative_traffic = rep_traffic

    # normlize tarin traffic
    new_train_traffic = []
    for a_traffic in train_traffic:
        a_traffic = traffic_norm(a_traffic, conf_file)
        new_train_traffic.append(a_traffic)

    return new_train_traffic, representative_traffic, future_traffic

def get_ori_traffic_seq(file_name):
    """从某个历史流量矩阵文件中得到矩阵序列
    """
    traffic_history = np.load(file_name, allow_pickle = True)[0]
    traffic_seq = []
    for timestamp, traffic in traffic_history.items():
        # pod自己到自己的流量置0
        pod_num = traffic.shape[0]
        for i in range(pod_num):
            traffic[i][i] = 0
        traffic_seq.append(traffic / 300 * 8 / 1000000000)

    return np.array(traffic_seq)


def scale_traffic(traffic_seq, conf_file, train_traffic):
    scale_traffic_seq = []
    record_shape = train_traffic[0].shape
    max_mat = get_place_max(train_traffic).reshape(record_shape)
    busy_coe = conf_file['busy_coe']
    threshold_den = conf_file['threshold_den']
    for a_traffic in traffic_seq:
        import simulation.ToE.max_min_fairness as mmf
        obj = mmf.Topology()
        obj.load_init_config(conf_file)
        ret = obj._analyze_tm(a_traffic, busy_coe, threshold_den)
        new_traffic = a_traffic.copy()
        new_traffic[ret == 1] = max_mat[ret == 1]
        scale_traffic_seq.append(new_traffic)

    return scale_traffic_seq


# def scale_traffic(traffic_seq, conf_file):
#     obj_frac_topo = fft.FindFractionalTopology()
#     obj_frac_topo.load_init_config(conf_file)
#     scale_traffic_seq = []

#     for i in range(len(traffic_seq)):
#         obj_frac_topo.set_traffic_sequence([traffic_seq[i]])
#         alpha, d_ij = obj_frac_topo.get_fractional_topology()
#         scale_traffic_seq.append(alpha * traffic_seq[i])

#     scale_traffic_seq = np.array(scale_traffic_seq)
#     return scale_traffic_seq


def get_svd_SV(traffic_seq):
    flatten_traffic = []
    for traffic in traffic_seq:
        flatten_traffic.append(traffic.flatten())
    
    flatten_traffic = np.array(flatten_traffic)
    U, S, V = np.linalg.svd(flatten_traffic)
    result = []
    for i in range(20):
        result.append(S[i]*V[i])
    return result


def get_place_var(traffic_seq):
    flatten_traffic = []
    for traffic in traffic_seq:
        tmp = traffic.flatten()
        flatten_traffic.append(tmp)
    
    return np.var(flatten_traffic, axis = 0)

def get_place_cov(traffic_seq):
    flatten_traffic = []
    for traffic in traffic_seq:
        flatten_traffic.append(traffic.flatten())
    
    return np.cov(flatten_traffic, rowvar=False)


def get_place_mean(traffic_seq):
    flatten_traffic = []
    for traffic in traffic_seq:
        tmp = traffic.flatten()
        flatten_traffic.append(tmp)
    
    return np.mean(flatten_traffic, axis = 0)


def get_place_max(traffic_seq):
    flatten_traffic = []
    for traffic in traffic_seq:
        flatten_traffic.append(traffic.flatten())
    
    return np.max(flatten_traffic, axis = 0)


def get_place_min(traffic_seq):
    flatten_traffic = []
    for traffic in traffic_seq:
        flatten_traffic.append(traffic.flatten())
    
    return np.min(flatten_traffic, axis = 0)


def standardize(data, method = 'norm'):
    if method == 'norm':
        tmp = np.array(data)
        return (tmp - tmp.min()) / (tmp.max() - tmp.min())
    elif method == 'zscore':
        tmp = np.array(data)
        return (tmp - tmp.mean()) / (tmp.std())


if __name__ == "__main__":
    test = get_ori_traffic_seq(f'{ROOT_PATH}/data/g_data/8pod_traffic.npy')
    start_index = 12096 + 4032 + 1910
    np.set_printoptions(precision=2, suppress=True, linewidth=200)
    for i in range(start_index, start_index + 20):
        print(test[i])
    pass
