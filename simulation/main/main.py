from simulation.main.simbase import SimBase
import sys
import logging
from utils.log import init_log
import simulation.main.config as config
from utils.base import cosine_similarity
import importlib
import simulation.preprocess.preprocess as pp
import numpy as np

class AddRepTrafficSim(SimBase):
    def run(self, filename):
        print(config.CONFIG)
        logger, handler = init_log(f'{filename}')
        params = config.Params(config.CONFIG)

        # 获取流量数据
        train_traffic, rep_traffic, future_traffic = pp.get_traffic(**params.base_conf)

        # 获取拓扑类型
        m_ToE = importlib.import_module(f'simulation.ToE.{params.ToE}')
        print(m_ToE)
        Topology = getattr(m_ToE, 'Topology')

        # 获取路由类型
        m_TE = importlib.import_module(f'simulation.TE.{params.TE}')
        print(m_TE)
        Routing = getattr(m_TE, 'Routing')

        # 得到拓扑矩阵
        a = Topology()
        params.topology_conf['traffic_seq'] = train_traffic
        d_ij = a.topology(params.CONF_FILE, **params.topology_conf)
        print(d_ij)
        print(d_ij.sum())
        print(d_ij.sum(axis = 0))
        print(d_ij.sum(axis = 1))

        routing_list = []
        for i in range(len(rep_traffic)):
            # 计算出routing
            lp = Routing(d_ij)
            lp.routing(params.CONF_FILE, rep_traffic[i], **params.routing_conf)
            routing_list.append(lp)

        logging.critical('mlu,alu')

        size = len(future_traffic) - 1
        for i in range(size):
            opt_routing = None
            # 用前一时刻traffic去找最相似的代表性矩阵
            opt_index, max_cos_sim = self._find_most_similar(rep_traffic, future_traffic[i])
            # 若相似度都不够，增加新的代表性矩阵和routing
            if max_cos_sim < 0.99:
                lp_traffic = None
                if config.CONFIG['scale'] == True:
                    from simulation.preprocess.TE_scale import traffic_scale
                    lp_traffic = traffic_scale(future_traffic[i])
                else:
                    lp_traffic = future_traffic[i]

                rep_traffic.append(future_traffic[i])
                lpnew = Routing(d_ij)
                lpnew.routing(params.CONF_FILE, lp_traffic, **params.routing_conf)
                routing_list.append(lpnew)
                opt_routing = lpnew
            else:
                opt_routing = routing_list[opt_index]
            # 找到相似度最高的代表性矩阵的 routing 来做 evaluate 下一时刻 traffic
            _, mlu, alu = opt_routing.calc_normal_link_utilization(future_traffic[i + 1])
            res_str = (f'{mlu},{alu}')
            logging.critical(res_str)

        print(len(routing_list))
        print(len(rep_traffic))
        logger.removeHandler(handler)


    def _find_most_similar(self, rep_traffic, com_traffic):
        """
        Input: 代表性流量矩阵序列 rep_traffic, 对比流量矩阵 com_traffic
        Output: 相似度最高的代表性矩阵索引 opt_index, 最高相似度值 max_cos_sim
        """
        max_cos_sim = 0    # 记录原来rep_traffic中相似度的最大值
        opt_index = None
        for t in range(len(rep_traffic)):
            tmp_cos_sim = cosine_similarity(rep_traffic[t].flatten().tolist(), com_traffic.flatten().tolist())
            if tmp_cos_sim > max_cos_sim:
                max_cos_sim = tmp_cos_sim
                opt_index = t
        return opt_index, max_cos_sim


if __name__ == "__main__":
    obj = AddRepTrafficSim()
    obj.main(sys.argv[1:])