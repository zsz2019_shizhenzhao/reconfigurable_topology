from simulation.main.simbase import SimBase
import sys
import logging
from utils.log import init_log
import simulation.main.config as config
from utils.base import cosine_similarity
import importlib
import simulation.preprocess.preprocess as pp
import numpy as np

class AddRepTrafficSim(SimBase):
    def run(self, filename):
        print(config.CONFIG)
        logger, handler = init_log(f'{filename}')
        params = config.Params(config.CONFIG)

        # 获取流量数据
        train_traffic, _, future_traffic = pp.get_traffic(**params.base_conf)

        # 获取拓扑类型
        m_ToE = importlib.import_module(f'simulation.ToE.{params.ToE}')
        print(m_ToE)
        Topology = getattr(m_ToE, 'Topology')

        # 获取路由类型
        m_TE = importlib.import_module(f'simulation.TE.{params.TE}')
        print(m_TE)
        Routing = getattr(m_TE, 'Routing')

        # 得到拓扑矩阵
        a = Topology()
        params.topology_conf['traffic_seq'] = train_traffic
        d_ij = a.topology(params.CONF_FILE, **params.topology_conf)
        print(d_ij)
        print(d_ij.sum())
        print(d_ij.sum(axis = 0))
        print(d_ij.sum(axis = 1))

        routing_list = []
        rep_traffic = []

        logging.critical('mlu,alu')

        size = len(future_traffic) - 1
        ele_num = future_traffic[0].shape[0] * future_traffic[0].shape[1]
        for i in range(size):
            opt_routing = None
            # 用前一时刻traffic去找最相似的代表性矩阵
            opt_index, max_cover_num = self._find_most_similar(rep_traffic, future_traffic[i])
            # 若相似度都不够，增加新的代表性矩阵和routing
            if max_cover_num < ele_num - 2:
                lp_traffic = None
                if config.CONFIG['scale'] == True:
                    from simulation.preprocess.LP_scale import LPscale
                    obj = LPscale(d_ij, params.CONF_FILE)
                    lp_traffic = obj.scale_traffic(future_traffic[i])
                else:
                    lp_traffic = future_traffic[i]

                rep_traffic.append(lp_traffic)   # test
                lpnew = Routing(d_ij)
                lpnew.routing(params.CONF_FILE, lp_traffic, **params.routing_conf)
                routing_list.append(lpnew)
                opt_routing = lpnew
            else:
                opt_routing = routing_list[opt_index]
            # 找到相似度最高的代表性矩阵的 routing 来做 evaluate 下一时刻 traffic
            _, mlu, alu = opt_routing.calc_normal_link_utilization(future_traffic[i + 1])
            res_str = (f'{mlu},{alu}')
            logging.critical(res_str)

        print(len(routing_list))
        print(len(rep_traffic))
        logger.removeHandler(handler)


    def _find_most_similar(self, rep_traffic, com_traffic):
        """
        Input: 代表性流量矩阵序列 rep_traffic, 对比流量矩阵 com_traffic
        Output: 相似度最高的代表性矩阵索引 opt_index, 最高覆盖值 max_cover_num
        """
        max_cover_num = 0    # 记录原来rep_traffic中相似度的最大值
        opt_index = None

        for t in range(len(rep_traffic)):
            cover_num = cover_similarity(rep_traffic[t], com_traffic)
            if cover_num > max_cover_num:
                max_cover_num = cover_num
                opt_index = t
        return opt_index, max_cover_num


def cover_similarity(rep: np.ndarray, com: np.ndarray):
    # 测试 normalize 后再去找覆盖
    params = config.Params(config.CONFIG)
    rep_traffic = pp.traffic_norm(rep, params.CONF_FILE)
    com_traffic = pp.traffic_norm(com, params.CONF_FILE)

    cover_traffic = rep_traffic >= com_traffic
    cover_num = np.sum(cover_traffic == True)
    
    return cover_num


if __name__ == "__main__":
    obj = AddRepTrafficSim()
    obj.main(sys.argv[1:])