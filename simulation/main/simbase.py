import os
from utils.base import cosine_similarity
import simulation.preprocess.preprocess as pp
import logging
from utils.log import init_log
from root_const import ROOT_PATH
import simulation.main.config as config
import importlib
import json
import datetime
import sys, getopt

ROUTING_LIST = []

class SimBase():
    """
    Desc: 仿真基类，原始仿真方法是用前面时间窗口的流量去做ToE。
    然后去evaluate后面时间窗口的流量，做普通LP routing。
    要改变仿真方法，重写其中某几个函数即可，如run(), test_LP_TE() 
    """
    def run(self, filename):
        print(config.CONFIG)
        logger, handler = init_log(f'{filename}')
        params = config.Params(config.CONFIG)

        # 获取流量数据
        train_traffic, rep_traffic, future_traffic = pp.get_traffic(**params.base_conf)

        # 获取拓扑类型
        m_ToE = importlib.import_module(f'simulation.ToE.{params.ToE}')
        print(m_ToE)
        Topology = getattr(m_ToE, 'Topology')

        # 获取路由类型
        m_TE = importlib.import_module(f'simulation.TE.{params.TE}')
        print(m_TE)
        Routing = getattr(m_TE, 'Routing')

        # 得到拓扑矩阵
        a = Topology()
        params.topology_conf['traffic_seq'] = train_traffic
        d_ij = a.topology(params.CONF_FILE, **params.topology_conf)
        print(d_ij)
        print(d_ij.sum())
        print(d_ij.sum(axis = 0))
        print(d_ij.sum(axis = 1))

        for i in range(len(rep_traffic)):
            # 计算出routing
            lp = Routing(d_ij)
            lp.routing(params.CONF_FILE, rep_traffic[i], **params.routing_conf)
            ROUTING_LIST.append(lp)

        logging.critical('mlu,alu')

        size = len(future_traffic) - 1
        for i in range(size):
            self.test_LP_TE(train_traffic, rep_traffic, future_traffic, i)

        logger.removeHandler(handler)


    def test_LP_TE(self, train_traffic, rep_traffic, future_traffic, i):
        cos_sim = 0

        opt_index = None
        for t in range(len(rep_traffic)):
            tmp_cos_sim = cosine_similarity(rep_traffic[t].flatten().tolist(), future_traffic[i].flatten().tolist())
            if tmp_cos_sim > cos_sim:
                cos_sim = tmp_cos_sim
                opt_index = t

        _, mlu, alu = ROUTING_LIST[opt_index].calc_normal_link_utilization(future_traffic[i + 1])

        res_str = (f'{mlu},{alu}')

        logging.critical(res_str)


    def run_one_time(self, file_name = None):
        param = config.Params(config.CONFIG)

        if file_name is not None:
            file_name = file_name
        else:
            file_name = str(datetime.datetime.now())[:19].replace(':','_').replace(' ','_')

        file_path = f'{ROOT_PATH}/simulation/result/single/{file_name}'
        self.run(file_path)
        with open(f'{file_path}.json',"w") as f:
            json.dump(config.CONFIG, f, indent=4)


    def run_many_time(self, dir_name = None):
        from itertools import product
        keys_list = []
        values_list = []
        
        for key, value in config.LIST.items():
            # if len(value) <= 1:
            #     continue
            keys_list.append(key)
            values_list.append(value)
            config.CONFIG[key] = 'change'

        # Todo: 对比文件夹名称
        if dir_name:
            dirname = dir_name
        else:
            dirname = str(datetime.datetime.now())[:19].replace(':','_').replace(' ','_')
            
        filepath = f'{ROOT_PATH}/simulation/result/compare/{dirname}/'
        if not os.path.exists(filepath):
            os.makedirs(filepath)

        with open(f'{filepath}BASE.json',"w") as f:
            json.dump(config.CONFIG, f, indent=4)

        with open(f'{filepath}CHANGE.json',"w") as f:
            json.dump(config.LIST, f, indent=4)


        for values in product(*values_list):
            name = ''
            for index, key in enumerate(keys_list):
                config.CONFIG[key] = values[index]
                name += f'{key}-{values[index]},'
            self.run(filepath + name)


    def main(self, argv): 
        try: 
            opts, args = getopt.getopt(argv,"mf:",["many","filename="])       
        except getopt.GetoptError: 
            print('error')
            sys.exit(2) 
        is_many = False
        file_name = None
        for opt, arg in opts: 
            if opt in ("-m","--many"): 
                is_many = True
            elif opt in ("-f", "--filename"): 
                file_name = arg
        if is_many:
            self.run_many_time(file_name)
        else:
            self.run_one_time(file_name)


if __name__ == "__main__":
    obj = SimBase()
    obj.main(sys.argv[1:])