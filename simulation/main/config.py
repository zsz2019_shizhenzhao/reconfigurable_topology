from root_const import ROOT_PATH

CONFIG = {
    # 'WINDOW_SIZE': 4032,
    'WINDOW_SIZE': 5,

    # 8个pod的情况
    # 'CONF_FILE': f'{ROOT_PATH}/config/g_config_8.csv',
    # 'TRAFFIC_FILE': f'{ROOT_PATH}/data/g_data/8pod_traffic.npy',

    # 19个pod的情况
    # 'CONF_FILE': f'{ROOT_PATH}/config/g_config_19.csv',
    # 'TRAFFIC_FILE': f'{ROOT_PATH}/data/g_data/19pod_traffic.npy',

    # 4pod
    'CONF_FILE': f'{ROOT_PATH}/config/4pod_config.csv',
    'TRAFFIC_FILE': f'{ROOT_PATH}/data/self_gen/4pod_traffic.npy',

    # fb data 9pod
    # 'CONF_FILE': f'{ROOT_PATH}/config/fb_cluster2.csv',
    # 'TRAFFIC_FILE': f'{ROOT_PATH}/data/fb_data/cluster2.npy',

    'ToE': 'mesh',
    # 'ToE': 'max_min_fairness',
    'TE': 'lp',
    # 'TE': 'threshold_TE',
    # 'TE': 'virtual_r_TE',

    'start_index': 0,
    'scale': True,
    'kmeans_num': 4,
    'overprovision': False,

    # topology
    'busy_coe': 0.6,
    'base_scale': 0,
    'threshold_den': 2,

    # routing

    # routing&topology共用
}

# 用于对比仿真
LIST = {
    'start_index':[
        0, 1 * CONFIG['WINDOW_SIZE'], 2 * CONFIG['WINDOW_SIZE'],
        3 * CONFIG['WINDOW_SIZE'], 4 * CONFIG['WINDOW_SIZE'],
        5 * CONFIG['WINDOW_SIZE'], 6 * CONFIG['WINDOW_SIZE'],
        7 * CONFIG['WINDOW_SIZE'],
        # 0 * CONFIG['WINDOW_SIZE'] # test
    ],
    # 'ToE': ['mesh', 'max_min_fairness'],
    # 'scale': [True, False],
    # 'base_scale': [0, 4],
}


class Params():

    # 将该类变为单例模式，避免重复创建实例对象占用内存
    __instance = None
    def __new__(cls, *args, **kwargs):
        if cls.__instance is None:
            obj = object.__new__(cls)
            cls.__instance = obj
        return cls.__instance


    def __init__(self, config):
        self.base_conf = {
            'start_index': config['start_index'],
            'scale': config['scale'],
            'kmeans_num': config['kmeans_num'],
            'overprovision': config['overprovision'],
            'WINDOW_SIZE': config['WINDOW_SIZE'],
            'CONF_FILE': config['CONF_FILE'],
            'TRAFFIC_FILE': config['TRAFFIC_FILE'],
        }

        self.WINDOW_SIZE = config['WINDOW_SIZE']
        self.CONF_FILE = config['CONF_FILE']
        self.TRAFFIC_FILE = config['TRAFFIC_FILE']


        self.TE = config['TE']
        self.ToE = config['ToE']

        # 存放做routing专用配置
        self.routing_conf = {}
        # 存放求topo专用配置，如某些ToE方法需要用到 traffic_seq 历史流量序列
        self.topology_conf = {
            'busy_coe': config['busy_coe'],
            'base_scale': config['base_scale'],
            'threshold_den': config['threshold_den'],
        }
