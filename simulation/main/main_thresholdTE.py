from simulation.main.simbase import SimBase
import sys
import logging
from utils.log import init_log
import simulation.main.config as config
from utils.base import cosine_similarity
import importlib
import simulation.preprocess.preprocess as pp
import numpy as np
import simulation.preprocess.find_new_traffic as fnt

class AddRepTrafficSim(SimBase):
    def run(self, filename):
        print(config.CONFIG)
        logger, handler = init_log(f'{filename}')
        params = config.Params(config.CONFIG)

        # 获取流量数据
        train_traffic, _, future_traffic = pp.get_traffic(**params.base_conf)
        
        # --- test new topology design traffic
        new_traffic, Sij, virtual_r = fnt.get_new_traffic(train_traffic, use_max_capacity=True, **params.base_conf)
        # _, Sij, virtual_r = fnt.get_new_traffic(train_traffic, use_max_capacity=False, **params.base_conf)
        train_traffic = [new_traffic]
        # ------

        # 获取拓扑类型
        m_ToE = importlib.import_module(f'simulation.ToE.{params.ToE}')
        print(m_ToE)
        Topology = getattr(m_ToE, 'Topology')

        # 获取路由类型
        m_TE = importlib.import_module(f'simulation.TE.{params.TE}')
        print(m_TE)
        Routing = getattr(m_TE, 'Routing')

        # 得到拓扑矩阵
        a = Topology()
        params.topology_conf['traffic_seq'] = train_traffic
        d_ij = a.topology(params.CONF_FILE, **params.topology_conf)
        print(d_ij)
        print(d_ij.sum())
        print(d_ij.sum(axis = 0))
        print(d_ij.sum(axis = 1))

        logging.critical('mlu,alu,ahc,norm_mlu,norm_alu')

        params.routing_conf['Sij'] = Sij
        params.routing_conf['virtual_r'] = virtual_r

        size = len(future_traffic) - 1
        for i in range(size):
            opt_routing = None
            lp_traffic = future_traffic[i]

            lpnew = Routing(d_ij)
            lpnew.routing(params.CONF_FILE, lp_traffic, future_traffic[i + 1], **params.routing_conf)
            opt_routing = lpnew

            every_lu, mlu, alu, norm_mlu, norm_alu = opt_routing.calc_normal_link_utilization(future_traffic[i + 1])
            ahc = opt_routing.ave_hop_count(future_traffic[i + 1])
            res_str = (f'{mlu},{alu},{ahc},{norm_mlu},{norm_alu}')
            logging.critical(res_str)

        logger.removeHandler(handler)


if __name__ == "__main__":
    obj = AddRepTrafficSim()
    obj.main(sys.argv[1:])