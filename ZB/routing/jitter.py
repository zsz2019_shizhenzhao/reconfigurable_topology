import random
import numpy as np


def jitter(pod_num, traffic, scope=0.2, odds=1):
    low = 1 - scope
    high = 1 + scope
    for i in range(pod_num):
        for j in range(pod_num):
            # TODO: 加入odd
            change = random.uniform(low, high)
            traffic[i][j] *= change
    # traffic = gauss_noise(traffic)
    return traffic


def jitter1(pod_num, traffic, scope=0.2, odds=1):

    return traffic


def gauss_noise(matrix, mu=0, sigma=0.1):
    '''
    Add gauss noise as disturbing factor
    '''
    noise = np.random.normal(mu, sigma, matrix.shape)
    out_matrix = matrix + noise

    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            if matrix[i][j] < 0:
                matrix[i][j] = 0
    
    return matrix
