import numpy as np
import network
from jitter import *



pod_num = 100
list_len = 7


for file_order in range(1):
    traffic = np.load("../data" + str(pod_num) + "/out" + str(file_order) + ".npy")
    traffic_list = []
    for i in range(list_len):
        traffic_list.append(jitter(pod_num, traffic))
    net = network.Net(pod_num,traffic_list)
    # traffic_list:一个保存流量矩阵的列表,长度为list_len
    # jitter 为抖动函数,用一个矩阵生成相似的矩阵,现在写的比较简单可以修改
    # 用traffic_list生成topology:

    
    topology = []


    net.get_topology(topology)
    net.generate_paths()
    net.get_route()
    net.auto_simulation()
    # 输出topology对这些流量矩阵的表现
    print(net.result)