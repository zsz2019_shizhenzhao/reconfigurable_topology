import numpy as np
import networkx as nx
import routing
import flow_simulation
from topo_gen import *
from jitter import *
class Net:
    """"
    simulate for data center routing.
    """
    _traffic_list = None
    _topology = None
    _graph = None
    _pod_num = 0
    result = None

    # var list for linear programming


    def __init__(self, pod_num, traffic_list, topology = None):
        
        self._topology = topology
        self._traffic_list = traffic_list
        self._graph = None
        self._pod_num = pod_num
        self._path_list = [[[] for x in range(pod_num)] for y in range(pod_num)] #记录k条最短路径的信息
        self._route = [[[] for x in range(pod_num)] for y in range(pod_num)]  #记录通过每条边的路径 (i,j,num)代表(i,j)间的第num条最短路径
        self._path_weight_value = []
        self.result = {'ideal_mu': 0, 'real_mu': 0}

    # def get_topology(self, ocs_num):
    #     self._topology = mincost_topo(self._pod_num, ocs_num, self._traffic_list)

    def get_topology(self, topology):
        self._topology = topology

    
    @staticmethod
    def get_path_link_pair(path):
        path_list = []
        if len(path) > 1:
            for i in range(len(path)-1):
                path_list.append((path[i], path[i+1]))
        return path_list 

    def _generate_graph(self):
        self._graph = nx.DiGraph()
        for i in range(self._pod_num):
            for j in range(self._pod_num):
                if self._topology[i][j] > 0:
                    self._graph.add_edge(i,j)
    
    def generate_paths(self):
        """
        generate all the direct and indirect paths
        path_list[i][j] include all the path that link i to j
        path formulate: [(i,j)] or [(i,k),(k,j)]
        """
        self._generate_graph()
        for i in range(self._pod_num):
            for j in range(self._pod_num):
                if i != j:
                    k_shortest = nx.shortest_simple_paths(self._graph, i, j)#计算出(i,j)的所有简单路径,由短到长
                    num = 0
                    for path in k_shortest:
                        if num > 1 and len(path) > 3:
                            break
                        path_link_pair = self.get_path_link_pair(path)
                        self._path_list[i][j].append(path_link_pair)
                        for pair in path_link_pair:
                            self._route[pair[0]][pair[1]].append((i , j, num))#如果这条路径经过一条边,记录在route数组中
                        num += 1



    def get_route(self):
        route = routing.Routing(self._pod_num, self._traffic_list, self._topology, self._path_list, self._route)
        self.result['ideal_mu'], self._path_weight_value = route.routing()
        # print(self.result['ideal_mu'])
        # print(self._path_weight_value)
        # route.routing()

    def real_traffic_simulation(self,real_traffic):
        sim = flow_simulation.FlowSimulation(self._pod_num, self._path_weight_value, self._path_list, real_traffic)
        self.result["real_mu"] = sim.simulate()

    def auto_simulation(self):
        sim = flow_simulation.FlowSimulation(self._pod_num, self._path_weight_value, self._path_list, self._topology)
        self.result["real_mu"] = []
        for traffic in self._traffic_list:
            # sim.get_traffic(jitter1(self._pod_num, traffic))
            sim.get_traffic(traffic)
            result = sim.simulate()
            self.result["real_mu"].append(result)

