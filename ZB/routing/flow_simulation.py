from ortools.linear_solver import pywraplp
import numpy as np

class FlowSimulation:

    def __init__(self, pod_num, path_weight_value, path_list, topology, traffic = None):
        self._pod_num = pod_num
        self._traffic = traffic
        self._path_weight_value = path_weight_value
        self._path_list = path_list
        self._topology = topology

    def get_traffic(self, traffic):
        self._traffic = traffic

    def simulate(self):
        # self.solver = pywraplp.Solver('LinearProgrammingExample', pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)
        link_flow = np.zeros((self._pod_num, self._pod_num))
        for i in range(self._pod_num):
            for j in range(self._pod_num):
                if i!=j and self._traffic[i][j] != 0:
                    for k in range(len(self._path_list[i][j])):
                        path = self._path_list[i][j][k]
                        flow_value = self._traffic[i][j] * self._path_weight_value[i][j][k]
                        for edge in path:
                            link_flow[edge[0]][edge[1]] += flow_value
        for i in range(self._pod_num):
            for j in range(self._pod_num):
                if link_flow[i][j] != 0:
                    link_flow[i][j] /= self._topology[i][j]
        return(np.max(link_flow))

