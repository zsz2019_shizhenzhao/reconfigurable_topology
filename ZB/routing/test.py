import numpy as np
import network



pod_num = 10
traffic_list = []
for i in range(3):
    traffic_list.append(np.random.random((pod_num, pod_num)))
topology = []
for i in range(pod_num):
    l = []
    for j in range(pod_num):
        if i != j:
            l.append(1)
        else:
            l.append(0)
    topology.append(l)
# print(topology)
net = network.Net(pod_num,traffic_list)
net.get_topology(4)
net.generate_paths()
net.get_route()
net.auto_simulation()
print(net.result)