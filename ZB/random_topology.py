import numpy as np
from utils.base import DcnBase
import random

class RandomTopology(DcnBase):
    """
    Desc: 随机连接的random topology结构
    """
    def get_connects_matrix(self, link = 1):
        """
        Desc: 得到random topology的pods间连接数矩阵
        link: 每次随机画圈使用的link数,默认为1
        """
        r_ingress = [self._r_ingress[x] for x in range(self._pods_num)]
        r_egress = [self._r_egress[x] for x in range(self._pods_num)]
        link_list = list(range(self._pods_num))
        matrix = np.zeros((self._pods_num, self._pods_num), int)
        
        while(len(link_list) > 0):
            tmp_list = [link_list[x] for x in range(len(link_list))]
            for i in tmp_list:
                if(r_ingress[i] < link or r_egress[i] < link):
                    link_list.remove(i)
            random.shuffle(link_list)
            if(len(link_list) == 0):
                break
            for j in range(len(link_list) - 1):
                matrix[link_list[j]][link_list[j+1]] += link
                r_ingress[link_list[j]] -= link
                r_egress[link_list[j]] -= link
            matrix[link_list[len(link_list) - 1]][link_list[0]] += link
            r_ingress[link_list[len(link_list) - 1]] -= link
            r_egress[link_list[len(link_list) - 1]] -= link
        return matrix

if __name__ == "__main__":
    obj = RandomTopology()
    obj.load_init_config('../config/pods_config.csv')
    obj.get_connects_matrix(1)