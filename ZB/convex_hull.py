import numpy as np
from sklearn.decomposition import PCA
from utils.base import DcnBase
# import cv2
from scipy.spatial import ConvexHull

class TrafficConvex(DcnBase):


    def _get_pca(self, d = 2):
        data_mat = []
        self.record_shape = self._traffic_sequence[0].shape
        for a_traffic in self._traffic_sequence:
            traffic_to_one_dimension = a_traffic.reshape(1, np.prod(np.shape(a_traffic)))[0]
            data_mat.append(traffic_to_one_dimension)
        self.pca = PCA(n_components=2)
        data_mat = np.array(data_mat)
        # print(data_mat)
        self.pca.fit(data_mat)
        self.pca_data = self.pca.fit_transform(data_mat)
        
        # for i in range(10):
        #     vector_a = np.mat(self.pca.inverse_transform(self.pca_data[1]))
        #     vector_b = np.mat(data_mat[1])
        #     num = float(vector_a * vector_b.T)
        #     denom = np.linalg.norm(vector_a) * np.linalg.norm(vector_b)
        #     cos = num / denom
        #     print(cos)

    def get_convex_traffic(self):
        self._get_pca()
        hull = ConvexHull(self.pca_data)
        index_set = set()
        for i in hull.simplices:
            index_set.add(i[0])
            index_set.add(i[1])
        convex_traffic = []
        for i in index_set:
            convex_traffic.append(self._traffic_sequence[i])
        return convex_traffic
        
        


if __name__ == "__main__":
    obj = TrafficConvex()
    for i in range(10):
        traffic = np.load(f"../data/data_properties_100/out_pod{i}.npy")
        obj.add_a_traffic(traffic)
        # print(traffic)
    obj.get_convex_traffic()
