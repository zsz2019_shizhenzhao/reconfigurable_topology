import numpy as np
import kspoptimal
import mincost



class DataCenter:
    ocs_num = 0
    tor_num = 0
    min_flow = 0
    traffic_demand = []
    value_matrix = []
    topology = []

    def __init__(self,ocs_num,tor_num):
        self.ocs_num = ocs_num
        self.tor_num = tor_num
        self.min_flow = 0
        self.traffic_demand = np.zeros((tor_num, tor_num))
        self.value_matrix = np.zeros((tor_num, tor_num), int)
        self.topology = []

    def clear(self):
        self.min_flow = 0
        self.traffic_demand = np.zeros((self.tor_num, self.tor_num))
        self.value_matrix = np.zeros((self.tor_num, self.tor_num), int)
        self.topology = []

    def get_demand(self, demand):
        """
        get traffic demand from other program
        :param demand: the traffic demand matrix
        :return:
        """
        if len(demand) >= self.tor_num and len(demand[0]) >= self.tor_num:
            for i in range(self.tor_num):
                for j in range(self.tor_num):
                    if i != j:
                        self.traffic_demand[i][j] = demand[i][j]
        self.generate_value()
        # print(self.traffic_demand)

    def generate_value(self):
        """
        turn the traffic demand to int for the min cost program
        min_flow is the minimum flow in the matrix
        """
        min = 1
        for i in range(self.tor_num):
            for j in range(self.tor_num):
                if self.traffic_demand[i][j] != 0 and self.traffic_demand[i][j] < min:
                    min = self.traffic_demand[i][j]
        self.min_flow = min
        self.value_matrix = np.ceil(self.traffic_demand/min).astype(int)
        # print(self.value_matrix)

    def change_ocs_num(self, ocs_num):
        self.ocs_num = ocs_num

    def get_traffic_scale(self):
        return self.traffic_demand.sum()

    def min_cost_topology(self, attenuation_num=2):
        self.topology = mincost.mincost_topo(self.tor_num, self.ocs_num, self.value_matrix, attenuation=attenuation_num)
        # print("generate mincost configure")

    def full_link_topology(self):
        self.topology = mincost.full_link(self.tor_num)
        # print("generate full link configure")

    def random_link_topology(self):
        self.topology = mincost.random_graph(self.tor_num, self.ocs_num)

    def simulate_time(self, k_path_num):
        link_num = self.topology[0]
        configure = self.topology[1]
        # print("start kps optimal")
        opt_time = kspoptimal.ksp_optimal(self.tor_num, k_path_num, configure, self.traffic_demand)
        # print(opt_time)
        return opt_time