import routing
import numpy as np
pod_num = 30
traffic_list = []
for i in range(3):
    traffic_list.append(np.random.random((pod_num, pod_num)))
topology = []
for i in range(pod_num):
    l = []
    for j in range(pod_num):
        if i != j:
            l.append(1)
        else:
            l.append(0)
    topology.append(l)
# print(topology)
test = routing.Routing(pod_num,traffic_list,topology)
print("generate_graph")
test.generate_graph()
print("generate_path")
test.generate_paths()
print("set")
test.set_constraint()