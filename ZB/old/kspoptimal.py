import numpy as np
import networkx as nx
from ortools.linear_solver import pywraplp

def edge_path(path):
    path_list = []
    if len(path) > 1:
        for i in range(len(path)-1):
            path_list.append((path[i],path[i+1]))
    return path_list


def ksp_optimal(n, k, configure, traffic_demand):
    topology = configure.tolist()
    solver = pywraplp.Solver('LinearProgrammingExample', pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)
    G = nx.DiGraph()
    flow_weight = [[[] for i in range(n)] for j in range(n)] #包含(i,j)流量的k条路径的权重信息
    flow_path = [[[] for x in range(n)] for y in range(n)] #记录k条最短路径的信息
    route = [[[] for x in range(n)] for y in range(n)]  #记录通过每条边的路径 (i,j,num)代表(i,j)间的第num条最短路径
    flow_constraint = [[[] for x in range(n)] for y in range(n)]  #k条路径满足流量需求的限制条件
    link_constraint = [[[] for x in range(n)] for y in range(n)]  #链接间流量小于链接能力的限制条件


    #初始化拓扑图
    for i in range(n):
        for j in range(n):
            if topology[i][j] > 0:
                G.add_edge(i,j)



    for i in range(n):
        # print(i)
        for j in range(n):
            if i != j and traffic_demand[i][j] > 0:
            # print(i, j)
                k_shortest = nx.shortest_simple_paths(G, i, j) #计算出(i,j)的所有简单路径,由短到长
                num = 0
                flow_constraint[i][j] = solver.Constraint(traffic_demand[i][j], traffic_demand[i][j]) #(i,j)的流量由k条路径平分
                for path in k_shortest:
                    if num > 1 and len(path) > 3:
                        # print(num)
                        break
                    flow_weight[i][j].append(solver.NumVar(0, solver.infinity(), str((i, j)))) #第num条路径的变量
                    flow_constraint[i][j].SetCoefficient(flow_weight[i][j][num], 1)   #求和系数均为1
                    flow_path[i][j] = path
                    # print(edge_path(path))
                    for edge in edge_path(path):  #如果这条路径经过一条边,记录在route数组中
                        route[edge[0]][edge[1]].append((i , j, num))
                    num += 1
                    # if num >= k: #搜索到k条路径截止
                    #     break

    mu = solver.NumVar(0, solver.infinity(), 'mu')  #完成流量需要的时间mu

    print("set capability")
    #对于每一条边,走过他的流量小于mu*link能力
    for i in range(n):
        # print("x",i)
        for j in range(n):
            link_flow = route[i][j]
            if len(link_flow) > 0: #如果有流过该链接的流量
                link_constraint[i][j] = solver.Constraint(-solver.infinity(), 0) # 流量 - capability*mu < 0
                link_constraint[i][j].SetCoefficient(mu, -topology[i][j])
                for flow in link_flow:
                    # print(type(flow_weight[flow[0]][flow[1]][flow[2]]))
                    link_constraint[i][j].SetCoefficient(flow_weight[flow[0]][flow[1]][flow[2]],1)

    # Objective function: mu
    objective = solver.Objective()
    objective.SetCoefficient(mu, 1)
    print("start solve")
    objective.SetMinimization()

    # Solve the system.
    solver.Solve()
    print("solve the lp problem")
    opt_solution = mu.solution_value()
    print('Number of variables =', solver.NumVariables())
    print('Number of constraints =', solver.NumConstraints())
    print('Optimal objective value =', opt_solution)
    return opt_solution