from ortools.graph import pywrapgraph
import numpy as np


def mincost_topo(n, m, value_matrix, attenuation=2):
    # 输出的拓扑结构
    topology = np.zeros((n, n), int)
    # 计算时使用的权值矩阵
    matrix = np.array(value_matrix)
    # 为了保证每两个tor之间至少存在一条路径,固定两个拓扑结构
    for i in range(n):
        topology[i,(i+1)%n] += 1
        topology[(i+1)%n,i] += 1

    for loop in range(m-2):
        "generate the min cost program"
        min_cost_flow = pywrapgraph.SimpleMinCostFlow()
        for i in range(n):
            min_cost_flow.AddArcWithCapacityAndUnitCost(0, i+1, 1, 0)
        for i in range(n):
            for j in range(n):
                if i != j:
                    min_cost_flow.AddArcWithCapacityAndUnitCost(i+1, j + 1 + n, 1, -int(matrix[i][j]))
        for i in range(n):
            min_cost_flow.AddArcWithCapacityAndUnitCost(i + 1 + n, 2 * n + 1, 1, 0)
        min_cost_flow.SetNodeSupply(0, n)
        min_cost_flow.SetNodeSupply(2 * n + 1, -n)

        permutation = np.zeros((n, n), int)
        min_flow = 0
        if min_cost_flow.Solve() == min_cost_flow.OPTIMAL:
            # print('Minimum cost:', min_cost_flow.OptimalCost())
            # print('')
            # print('  Arc    Flow / Capacity  Cost')
            for i in range(min_cost_flow.NumArcs()):
                if min_cost_flow.Flow(i) != 0:
                    if min_cost_flow.Tail(i)!= 0 and min_cost_flow.Head(i) != 2*n+1:
                        row = min_cost_flow.Tail(i)-1
                        col = min_cost_flow.Head(i)-n-1
                        permutation[row,col] = 1
                        if matrix[row,col] > 0:
                            if min_flow == 0 or matrix[row,col] < min_flow:
                                min_flow = matrix[row,col]
                            matrix[row,col] -= matrix[row,col] // attenuation
                        # cost = min_cost_flow.Flow(i) * min_cost_flow.UnitCost(i)
                        # print('%1s -> %1s   %3s  / %3s       %3s' % (
                        #     min_cost_flow.Tail(i)-1,
                        #     min_cost_flow.Head(i)-n-1,
                        #     min_cost_flow.Flow(i),
                        #     min_cost_flow.Capacity(i),
                        #     cost))
            min_flow -= min_flow // attenuation
            matrix = matrix - min_flow * permutation
            topology += permutation
            # print(permutation)
            # print(topology)
            # print(matrix)
        else:
            print('There was an issue with the min cost flow input.')
    return m*n, topology
    # print(topology)


def full_link(n):
    topology = np.zeros((n, n), int)
    for i in range(n):
        for j in range(n):
            if i != j:
                topology[i][j] = 1
    return n*(n-1), topology