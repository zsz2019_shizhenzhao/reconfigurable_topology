import numpy as np
import networkx as nx
from ortools.linear_solver import pywraplp

class Routing:
    """"
    simulate for data center routing.
    """
    _traffic_list = None
    _topology = None
    _graph = None
    _pod_num = 0

    # var list for linear programming


    def __init__(self, pod_num, traffic_list, topology):
        self.ideal_mu = 0
        self._topology = topology
        self._traffic_list = traffic_list
        self._graph = nx.DiGraph()
        self._pod_num = pod_num
        self._solver = pywraplp.Solver('LinearProgrammingExample', pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)
        self._path_weight = [[[] for i in range(pod_num)] for j in range(pod_num)] #包含(i,j)流量的k条路径的权重信息
        self._path_list = [[[] for x in range(pod_num)] for y in range(pod_num)] #记录k条最短路径的信息
        self._route = [[[] for x in range(pod_num)] for y in range(pod_num)]  #记录通过每条边的路径 (i,j,num)代表(i,j)间的第num条最短路径
        self._flow_constraint = [[[] for x in range(pod_num)] for y in range(pod_num)]  #k条路径满足流量需求的限制条件
        self._link_constraint = []  #链接间流量小于链接能力的限制条件
        self._path_weight_value = None

    @staticmethod
    def get_path_link_pair(path):
        path_list = []
        if len(path) > 1:
            for i in range(len(path)-1):
                path_list.append((path[i], path[i+1]))
        return path_list 

    def generate_graph(self):
        for i in range(self._pod_num):
            for j in range(self._pod_num):
                if self._topology[i][j] > 0:
                    self._graph.add_edge(i,j)
    

    def generate_paths(self):
        """
        generate all the direct and indirect paths
        path_list[i][j] include all the path that link i to j
        path formulate: [(i,j)] or [(i,k),(k,j)]
        """
        for i in range(self._pod_num):
            print(i)
            for j in range(self._pod_num):
                if i != j:
                    k_shortest = nx.shortest_simple_paths(self._graph, i, j)#计算出(i,j)的所有简单路径,由短到长
                    num = 0
                    for path in k_shortest:
                        if num > 1 and len(path) > 3:
                            break
                        path_link_pair = self.get_path_link_pair(path)
                        self._path_list[i][j].append(path_link_pair)
                        for pair in path_link_pair:
                            self._route[pair[0]][pair[1]].append((i , j, num))#如果这条路径经过一条边,记录在route数组中
                        num += 1

    def set_constraint(self):
        """
        """

        # constraint for flow  
        mu = self._solver.NumVar(0, self._solver.infinity(), 'mu') 
        for i in range(self._pod_num):
            for j in range(self._pod_num):
                if i != j:
                    num = 0
                    self._flow_constraint[i][j] = self._solver.Constraint(1, 1)
                    for path in self._path_list[i][j]:
                        self._path_weight[i][j].append(self._solver.NumVar(0, 1, str((i, j)))) #第num条路径的变量
                        self._flow_constraint[i][j].SetCoefficient(self._path_weight[i][j][num], 1)   #求和系数均为1
                        num += 1

        # constraint for link
        traffic_ord = 0
        for traffic_demand in self._traffic_list:
            self._link_constraint.append([[[] for x in range(self._pod_num)] for y in range(self._pod_num)])
            for i in range(self._pod_num):
                for j in range(self._pod_num):
                    link_flow = self._route[i][j]
                    if len(link_flow) > 0: #如果有流过该链接的流量
                        self._link_constraint[traffic_ord][i][j] = self._solver.Constraint(-self._solver.infinity(), 0) # 流量 - capability*mu < 0
                        self._link_constraint[traffic_ord][i][j].SetCoefficient(mu, -self._topology[i][j])
                        for flow in link_flow:
                            self._link_constraint[traffic_ord][i][j].SetCoefficient(self._path_weight[flow[0]][flow[1]][flow[2]], traffic_demand[flow[0]][flow[1]])
            traffic_ord += 1

        # Objective function: mu
        objective = self._solver.Objective()
        objective.SetCoefficient(mu, 1)
        objective.SetMinimization()

        # Solve the system.
        print("Solve the system")
        self._solver.Solve()
        print("solve the lp problem")
        opt_solution = mu.solution_value()
        # print('Number of variables =', solver.NumVariables())
        # print('Number of constraints =', solver.NumConstraints())
        # print('Optimal objective value =', opt_solution)
        self.ideal_mu = opt_solution
        print(opt_solution)
        return opt_solution

    def get_path_weight(self):
        self._path_weight_value = [[[] for i in range(self._pod_num)] for j in range(self._pod_num)]
        for i in self._pod_num:
            for j in self._pod_num:
                for weight in self._path_weight:
                    self._path_weight_value[i][j].append(weight.solution_value())
    

    def route_for_real_traffic(self,traffic):
        # TODO: use the path weight information caculate traffic transform time
        a = 0
