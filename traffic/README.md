# Traffic

This module is used for generating traffic flows for our simulation. 

## Brief introduction

The algorithm to generate DCN traffic flows can be summarized in the following three modules: 

- **Flow Size Distribution**: The simulated flows are randomly generated from the given flow size distribution. 
- **Arrival Time**: The interval between the arrival of flow can be generated from some probability distribution. Currently, we use *Poisson Process* to simulate the traffic flows. 
- **Traffic Pair** : Choose a traffic pair each time according to the particular probability set and assign it with traffic flow. 

Note: *Poisson Arrival*
$$Poisson arrival$$
$$F(x) = 1 - e^{(-\lambda * x)}$$
$$ln( 1 - F(x) ) = -\lambda * x $$
$$x = -ln( Uniform(x) ) / \lambda$$

## Program Structure

The entry of the program is `main.py`
