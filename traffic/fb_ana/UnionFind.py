class UnionFind:

    def __init__(self, n):
        self.count = n
        self.parent = [-1 for i in range(n)]
        
    def get_size(self):

        return self.count

    def find(self, p):
        if self.parent[p] < 0:
            return p
        self.parent[p] = self.find(self.parent[p])
        return self.parent[p]

    def union(self, p, q):
        if p >= len(self.parent) or q >= len(self.parent):
            self.count += len(self.parent)
            self.parent.extend([-1 for i in range(len(self.parent))])
        p_root = self.find(p)
        q_root = self.find(q)
        if p_root == q_root:
            return
        if self.parent[p_root] > self.parent[q_root]:
            self.parent[p_root] += self.parent[q_root]
            self.parent[q_root] = p_root
        else:
            self.parent[q_root] += self.parent[p_root]
            self.parent[p_root] = q_root

        self.count -= 1
