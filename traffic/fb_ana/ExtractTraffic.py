import pickle
import numpy as np
import os
from traffic.test.fbdata_analysis import check_path


FB_PATH = "data/fb_data"

class Extract:

    _label = {}
    _podid = {}
    _flowtable = [0 for i in range(3)]

    def __init__(self, path = "traffic/fb_ana"):
        path1 = os.path.join(path, "cluster_info")
        with open(path1, 'rb') as f:
            clusters = pickle.load(f)
            for i in range(len(clusters)):
                count = 0
                for pod in clusters[i]:
                    self._label[pod[1]] = i
                    self._podid[pod[1]] = count    
                    count += 1

        for i in range(len(clusters)):
            a = len(clusters[i])
            self._flowtable[i] = a

    def timerange(self, datapath = FB_PATH):
        files=os.listdir(datapath)

        low = 1576505694
        high = 0
        filecount = 0

        for f in files:
            with open(os.path.join(datapath, f), "r", encoding="ASCII") as target:
                for line in target:
                    if line == '\n' or line == '':
                        break
                    line = line.strip()
                    columns = line.split('\t')
                    line = target.readline()

                    timestamp = int(columns[0])
                    if timestamp < low:
                        low = timestamp
                    if timestamp > high:
                        high = timestamp
            filecount += 1
            print("Processed files: {}".format(filecount), end='\r')

        f = open("traffic/fb_ana/timerange", "w")
        f.write("{}\t{}".format(low, high))

    def read_data(self, datapath=FB_PATH, filenums='Max', time_interval=60):
        with open('traffic/fb_ana/timerange', 'r') as f:
            tmp = f.readline().split('\t')
            low = int(tmp[0])
            high = int(tmp[1])

        if filenums == 'Max':
            filenums = int((high - low) / time_interval)

        elif filenums > int((high - low) / time_interval):
            raise IndexError

        for i in range(3):
            clus_size = self._flowtable[i]
            self._flowtable[i] = np.zeros((filenums, clus_size, clus_size))

        files=os.listdir(datapath)

        filecount = 0

        for f in files:
            with open(os.path.join(datapath, f), "r", encoding="ASCII") as target:
                for line in target:
                    if line == '\n' or line == '':
                        break
                    line = line.strip()
                    columns = line.split('\t')
                    line = target.readline()

                    timestamp = int(columns[0])
                    if len(columns) != 15 or int(columns[14]) != 0 or int(columns[13]) != 0:
                        continue        
                    
                    timeidx = int((timestamp - low) / time_interval)

                    if timeidx > filenums - 1:
                        continue

                    if columns[11] in self._podid.keys() and \
                        columns[12] in self._podid.keys():
                        clus_idx = self._label[columns[11]]
                        self._flowtable[clus_idx][timeidx, self._podid[columns[11]], \
                            self._podid[columns[12]]] += int(columns[1])
                
            filecount += 1
            print("Processed files: {}".format(filecount), end='\r')
        
        self._save()

    def _save(self):    
        check_path(path1="data/cluster1",\
                path2="data/cluster2",\
                path3="data/cluster3")
        for i in range(3):
            count = 1
            for mat in self._flowtable[i]:
                np.save("data/cluster{}/traffic{}.npy".format(i + 1, count), mat)
                count += 1
            
if __name__ == "__main__":
    example = Extract()
    example.read_data()