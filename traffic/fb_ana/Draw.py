from traffic.test.multi_image import multi_image
import os
import numpy as np


def show_matrix(traffic):

    multi_image(traffic)

if __name__ == "__main__":
    dstpath = "data/cluster3"
    files=os.listdir(dstpath)
    traffic = []
    count = 0
    for f in files:
        if count == 9:
            break
        traffic.append(np.load(os.path.join(dstpath, f)))
        count += 1
    show_matrix(traffic)