from traffic.fb_ana.UnionFind import UnionFind
import pickle, os

class Classify:

    _columnName = ["timestamp", "packet length", "src IP", "dst IP", 
    "src L4 Port", "dst L4 Port", "IP protocol", "src hostprefix", "dst hostprefix", 
    "src Rack", "dst Rack", "src Pod", "dst Pod", "intercluster", "interdatacenter"]
    _podidx = {}
    _podname = {}
    _podcount = 0
    _exclude = {}

    def __init__(self, datapath, filepath = "cluster_info"):
        self._path = datapath
        self._unionfindset = UnionFind(50)
        pass

    def read(self, path):
        with open(path, "rb") as f:
            clusters = pickle.load(f)
        print(clusters)

    def intracluster_pods(self):
        path = self._path
        files=os.listdir(path)

        count = 0
        filecount = 0
        clusters = []
        intercluster = {}
        exclude = {}

        for f in files:
            if filecount == 100:
                break
            with open(os.path.join(path, f), "r", encoding="ASCII") as target:
                for line in target:
                    flag = True
                    if line == '\n' or line == '':
                        break
                    line = line.strip()
                    columns = line.split('\t')
                    if len(columns) != len(self._columnName):
                        continue
                    tmp_dict = {self._columnName[j]:columns[j] for j in range(len(self._columnName))}
                    tmp_dict["timestamp"] = int(tmp_dict["timestamp"])
                    tmp_dict["interdatacenter"] = int(tmp_dict["interdatacenter"])
                    tmp_dict["intercluster"] = int(tmp_dict["intercluster"])
                    
                    line = target.readline()

                    if tmp_dict["interdatacenter"] != 0:
                        # Only consider the datacenter flows
                        continue
                    
                    pod = []
                    for i in range(11, 13):
                        if columns[i] == '\\N':
                            flag = False
                            break
                        if columns[i] not in self._podidx.keys():
                            self._podname[self._podcount] = columns[i]
                            self._podidx[columns[i]] = self._podcount
                            pod.append(self._podcount)
                            self._podcount += 1 
                        else:
                            pod.append(self._podidx[columns[i]])
                    if not flag:
                        continue
                    
                    if tmp_dict["intercluster"] == 0:
                        self._unionfindset.union(pod[0], pod[1])
                    
                    else:
                        if pod[0] not in exclude.keys():
                            exclude[pod[0]] = []
                        if pod[1] not in exclude.keys():
                            exclude[pod[1]] = []
                        exclude[pod[0]].append(pod[1])
                        exclude[pod[1]].append(pod[0])

                    count+=1
            filecount += 1
            print("Processed files: {}".format(filecount), end='\r')
        
        self._exclude = exclude

        print()
        print(self._podcount)

                
        print(self._unionfindset.get_size())
        print(self._unionfindset.parent)

    
    def save(self, path):
        clusters = [[] for i in range(3)]
        label = {}
        count = 0
        flag = True
        for i in range(self._podcount):
            if self._unionfindset.parent[i] < -1:
                label[i] = count
                for j in range(len(self._unionfindset.parent)):
                    if self._unionfindset.parent[j] == i:
                        label[j] = count
                count += 1 

        while flag:
            flag = False
            for key, value in self._exclude.items():
                if key not in label.keys():
                    candidate = [1 for i in range(3)]
                    for v in value:
                        if v in label.keys():
                            candidate[label[v]] = 0
                    if sum(candidate) == 1:
                        flag = True
                        for opt in range(3):
                            if candidate[opt] == 1:
                                label[key] = opt

        for podid, clusterid in label.items():
            clusters[clusterid].append((podid, self._podname[podid]))


        for i in range(len(clusters)):
            print("Cluster: {}".format(i + 1))
            for item in clusters[i]:
                print("Index: {0[0]} \t ID: {0[1]}".format(item))

        with open(path, "wb") as f:
            pickle.dump(clusters, f)


if __name__ == "__main__":
    c = Classify("D:\\Lab\\reconfigurable_topology\\data\\fb_data")
    c.intracluster_pods()
    c.save("cluster_info")
    # c.read("cluster_info")