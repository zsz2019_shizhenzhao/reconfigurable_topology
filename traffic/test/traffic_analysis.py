import numpy as np 
import pandas as pd 
from traffic.run.TrafficPlanner import FlowTable_pb2
import matplotlib.pyplot as plt

class Analysis:
    _pod_traffic = np.array([])
    _tor_traffic = np.array([])
    _flowtable = []
    _DATA_PATH = "data"

    def __init__(self, pod_file, tor_file, logfile):
        self._pod_traffic = np.load(self._DATA_PATH+pod_file)
        self._tor_traffic = np.load(self._DATA_PATH+tor_file)
        self.__readpb(logfile)

    def __readpb(self, filepath):
        tmp_file = open(self._DATA_PATH+filepath, 'rb')
        log_data = tmp_file.read().rstrip(b"endl")
        flowList = log_data.split(b"endl")

        for flow in flowList:        
            self._flowtable.append(flow)
    
    def print_log(self):
        frame = []
        for flow in self._flowtable:
            flowTable = FlowTable_pb2.Flow()
            flowTable.ParseFromString(flow)
            tmp_flow = np.array([flowTable.src, flowTable.dst, flowTable.time, flowTable.flowSize])
            frame.append(tmp_flow)
        frame = np.array(frame)

        df = pd.DataFrame(frame, columns=["Source", "Destination", "Start Time", "Flow Size"])
        print(df)

    def plot(self):
        bins = []
        for flow in self._flowtable:
            flowTable = FlowTable_pb2.Flow()
            flowTable.ParseFromString(flow)
            bins.append(flowTable.flowSize)

        plt.hist(bins, bins=50, density=0, edgecolor="black")
        print(self._pod_traffic)
        plt.matshow(self._pod_traffic)
        plt.matshow(self._tor_traffic)

        plt.show()


if __name__ == "__main__":
    analysis = Analysis("/data_ConstIncrease_100/out_pod3.npy", "/data_ConstIncrease_100/out_tor3.npy", "/flowTable200/flowTable3")
    analysis.print_log()
    analysis.plot()