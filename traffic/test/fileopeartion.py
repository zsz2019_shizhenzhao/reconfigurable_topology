import os, shutil
import bz2

def rename():
    path="D:\\fb_data"
    files=os.listdir(path)

    count = 1
    for f in files:
        old_file=os.path.join(path, f)
        print("old_file is {}".format(old_file))

        new_file=os.path.join(path,"{}.bz2".format(count))
        print("File is renamed as:{}".format(new_file))
        os.rename(old_file, new_file)
        
        count += 1

def extract():
    path="D:\\Lab\\reconfigurable_topology\\data\\fb_data"
    
    for (dirpath, dirnames, files) in os.walk(path):
        for name in dirnames:
            srcfile = os.path.join(dirpath, name)
            srcfile = os.path.join(srcfile, name)
            dstfile = os.path.join(path, "fb{}".format(name))
            try:
                shutil.move(srcfile, dstfile)
            except:
                continue

def unzip(dst):
    path="D:\\fb_data"
    files=os.listdir(path)
    un_path = dst

    for (dirpath, dirnames, files) in os.walk(path):
        for filename in files:
            filepath = os.path.join(dirpath, filename)
            newfilepath = os.path.join(un_path, filename + '.ppm')
            with open(newfilepath, 'wb') as new_file, bz2.BZ2File(filepath, 'rb') as f:
                for data in iter(lambda : f.read(100 * 1024), b''):
                    new_file.write(data)

if __name__ == "__main__":
    extract()