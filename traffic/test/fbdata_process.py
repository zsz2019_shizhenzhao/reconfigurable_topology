import numpy as np
import os

class UnionFind:

    def __init__(self, n):
        self.count = n
        self.parent = [-1 for i in range(n)]
        
    def get_size(self):

        return self.count

    def find(self, p):
        if self.parent[p] < 0:
            return p
        self.parent[p] = self.find(self.parent[p])
        return self.parent[p]

    def union(self, p, q):
        if p >= len(self.parent) or q >= len(self.parent):
            self.count += len(self.parent)
            self.parent.extend([-1 for i in range(len(self.parent))])
        p_root = self.find(p)
        q_root = self.find(q)
        if p_root == q_root:
            return
        if self.parent[p_root] > self.parent[q_root]:
            self.parent[p_root] += self.parent[q_root]
            self.parent[q_root] = p_root
        else:
            self.parent[q_root] += self.parent[p_root]
            self.parent[p_root] = q_root

        self.count -= 1


class Process:
    _columnName = ["timestamp", "packet length", "src IP", "dst IP", 
    "src L4 Port", "dst L4 Port", "IP protocol", "src hostprefix", "dst hostprefix", 
    "src Rack", "dst Rack", "src Pod", "dst Pod", "intercluster", "interdatacenter"]
    _podidx = {}
    _podname = {}
    _podcount = 0
    _flowtable = {}    

    def __init__(self, path):
        self._unionfindset = UnionFind(50)
        self._intracluster_pods(path)

    def _intracluster_pods(self, path):
        files=os.listdir(path)

        count = 0
        filecount = 0
        clusters = []
        intercluster = {}

        for f in files:
            if filecount == 200:
                break
            with open(os.path.join(path, f), "r", encoding="ASCII") as target:
                for line in target:
                    flag = True
                    if line == '\n' or line == '':
                        break
                    line = line.strip()
                    columns = line.split('\t')
                    if len(columns) != len(self._columnName):
                        continue
                    tmp_dict = {self._columnName[j]:columns[j] for j in range(len(self._columnName))}
                    tmp_dict["timestamp"] = int(tmp_dict["timestamp"])
                    tmp_dict["interdatacenter"] = int(tmp_dict["interdatacenter"])
                    tmp_dict["intercluster"] = int(tmp_dict["intercluster"])
                    
                    line = target.readline()

                    if tmp_dict["interdatacenter"] != 0:
                        # Only consider the datacenter flows
                        continue
                    
                    pod = []
                    for i in range(11, 13):
                        if columns[i] == '\\N':
                            flag = False
                            break
                        if columns[i] not in self._podidx.keys():
                            self._podname[self._podcount] = columns[i]
                            self._podidx[columns[i]] = self._podcount
                            pod.append(self._podcount)
                            self._podcount += 1 
                        else:
                            pod.append(self._podidx[columns[i]])
                    if not flag:
                        continue
                    
                    if tmp_dict["intercluster"] == 0:
                        self._unionfindset.union(pod[0], pod[1])
                    
                    if (pod[0], pod[1]) in self._flowtable.keys():
                        self._flowtable[(pod[0], pod[1])].append((tmp_dict["timestamp"], tmp_dict["packet length"]))
                    else:
                        self._flowtable[(pod[0], pod[1])] = []
                    
                    count+=1
            filecount += 1
            print("Processed files: {}".format(filecount), end='\r')
        
        print()
        print(self._podcount)

        for key, value in intercluster.items():
            if value == 0:
                self._unionfindset.union(key[0], key[1])
                
        print(self._unionfindset.get_size())
        print(self._unionfindset.parent)
        
        self._save()
    
    def _read_from_tsv(self, target):
        print("Reading Data:")
        line = target.readline()
        count = 0
        while (line != '\n' and line != '' and count < self._MAX_LINES):
            flag = True
            # if count+1 == self._MAX_LINES:
            #     print('Data processed: [{}/{}]'.format(count+1, self._MAX_LINES),end='\n')
            # else:
            #     print('Data processed: [{}/{}]'.format(count, self._MAX_LINES),end='\r')
            
            line = line.strip()
            columns = line.split('\t')
            tmp_dict = {self._columnName[j]:columns[j] for j in range(len(self._columnName))}
            tmp_dict["timestamp"] = int(tmp_dict["timestamp"])
            tmp_dict["interdatacenter"] = int(tmp_dict["interdatacenter"])
            tmp_dict["intercluster"] = int(tmp_dict["intercluster"])

            line = target.readline()

            if tmp_dict["interdatacenter"] != 0:
                # Only consider the datacenter flows
                continue
            if tmp_dict["intercluster"] != 0:
                # Only consider the flows across clusters
                continue
            # print("{}, {}".format(tmp_dict["src hostprefix"], tmp_dict["dst hostprefix"]))
            
            for i in range(len(self._columnName)):
                # if columns[i] == '\\N' or columns[2] == columns[3] or columns[9] == columns[10] or columns[11] == columns[12]:
                if columns[i] == '\\N':
                    # print(columns)
                    flag = False
                    continue
                if i==2 or i==3:
                    if columns[i] not in self._ipdict.keys():
                        self._ipdict[columns[i]] = self._hostcount
                        self._hostcount += 1
                elif i==9 or i==10:
                    if columns[i] not in self._rackdict.keys():
                        self._rackdict[columns[i]] = self._rackcount
                        self._rackcount +=1 
                elif i==11 or i==12:
                    if columns[i] not in self._poddict.keys():
                        self._poddict[columns[i]] = self._podcount
                        self._podcount +=1 
            # if self._podcount == 50:
            #     break
            count+=1
            if flag: 
                self._flowtable.append(tmp_dict)
        self._flowtable.sort(key=lambda flow: flow["timestamp"])

    def _save(self):
        max_set = min(self._unionfindset.parent)
        p = self._unionfindset.parent.index(max_set)
        podlist = []
        for i in range(len(self._unionfindset.parent)):
            if self._unionfindset.parent[i] == p:
                podlist.append(i)
        podlist.append(p)

        content = []
        for (pi, pj), data in self._flowtable.items():
            if pi in podlist and pj in podlist:
                for flow in data:
                    content.append((int(flow[0]), int(flow[1]), pi, pj))
        content.sort(key=lambda flow: flow[0])

        content = np.array(content)
        np.save("traffic/test/podtraffic_{}.npy".format(abs(max_set)), content)

if __name__ == "__main__":
    fb_process = Process("D:\\Lab\\reconfigurable_topology\\data\\fb_data")
