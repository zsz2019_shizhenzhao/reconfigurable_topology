import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import distance
from traffic.test.multi_image import multi_image
import os
import shutil

def check_path(**path):
    for name in path.values():
        if os.path.isdir(name):
            shutil.rmtree(name)
        os.mkdir(name)

class Analysis:

    _NSLOT = 9
    _poddict = {}

    def __init__(self, filename):
        '''
        Read Pod-level traffic data extracted from fb_data
        The columns of 'podtraffic_(pod number)' are
        1. Timestamp
        2. Package length
        3. Source pod number
        4. Destination pod number
        '''
        self._flowtable = np.load(filename)
        self._podcount = int(filename.rstrip(".npy").split("_")[1])
        check_path(path = "data/fb_podtraffic")
        self._import_traffic()
        self.show_matrix()
        self.traffic_hist()

    def show_matrix(self):
        tvector = [self._podtraffic[i].reshape(1, -1) for i in range(self._NSLOT)]
        
        for i in range(len(tvector) - 1):
            print("Consine similarity between pod traffic {} {}".format(i, i + 1), end=": ")
            print(distance.cosine(tvector[i], tvector[i + 1]))

        multi_image(self._podtraffic)

    def traffic_hist(self):
        x = []
        num_bins = 50

        fig, ax = plt.subplots()

        for flow in self._flowtable:
            flowsize = flow[1]
            x.append(flowsize)

        # the histogram of the data
        n, bins, patches = ax.hist(x, num_bins, density = True)
        # print(n)
        # ax.set_xlim(0,1000)
        plt.show()

    def _import_traffic(self):
        total_time = self._flowtable[-1][0] - self._flowtable[0][0]
        time_slice = int(total_time / self._NSLOT)
        print("Total time:", total_time)
        self._podtraffic = np.zeros((self._NSLOT, self._podcount, self._podcount))
        count = 0

        for flow in self._flowtable:
            t = flow[0] - self._flowtable[0][0]
            i = min(int(t / time_slice), self._NSLOT - 1)
            flowsize = flow[1]
            srcpod, dstpod = flow[2], flow[3]
            if srcpod not in self._poddict.keys():
                self._poddict[srcpod] = count
                count += 1
            if dstpod not in self._poddict.keys():
                self._poddict[dstpod] = count
                count += 1
            
            if srcpod != dstpod:
                self._podtraffic[i][self._poddict[srcpod], self._poddict[dstpod]] += flowsize

        for i in range(self._NSLOT):
            np.save("data/fb_podtraffic/podtraffic_{}.npy".format(i + 1), self._podtraffic[i])

if __name__ == "__main__":
    ana = Analysis("traffic/test/podtraffic_8.npy")