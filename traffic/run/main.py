from traffic.run.TrafficPlanner import PoissonArrivalPlanner, saveTrafficMatrix
import pandas as pd
import numpy as np
import shutil
import os

def check_path(**path):
    for name in path.values():
        if os.path.isdir(name):
            shutil.rmtree(name)
        os.mkdir(name)

if __name__ == '__main__':
    # Path of property file
    propertyFile = "config/BasicPoisson.csv"
    
    properties = pd.read_csv(propertyFile, index_col=0)

    # Duration time(s)
    duration_s = int(properties.loc['duration_s', 'value'])

    # Number of output traffic matrix
    NtrafficMtx = int(properties.loc['output_num', 'value'])

    # Create Traffic Planner
    trafficPlanner = PoissonArrivalPlanner.PoissonArrivalPlanner(propertyFile)

    # label for filename
    label = trafficPlanner.getSimLabel()
    MATRIX_PATH = "data/data{}".format(label)
    LOG_PATH = "data/flowTable{}".format(label)
    
    # Clear directory
    check_path(path1=MATRIX_PATH, path2=LOG_PATH)

    for i in range(NtrafficMtx):
        # Save traffic flow log
        log_file = open(LOG_PATH+"/flowTable{}".format(i), "wb")
        
        trafficPlanner.createPlan(duration_s*1e9, logger=log_file)

        log_file.close()

        # Save traffic matrix
        saveTrafficMatrix.save_matrix(MATRIX_PATH+"/out_tor{}.npy".format(i), trafficPlanner.get_tor_traffic())
        saveTrafficMatrix.save_matrix(MATRIX_PATH+"/out_pod{}.npy".format(i), trafficPlanner.get_pod_traffic())

    exit(0)
