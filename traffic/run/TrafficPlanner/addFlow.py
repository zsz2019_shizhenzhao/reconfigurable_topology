from traffic.run.TrafficPlanner import FlowTable_pb2


def addFlow(time, srcId, dstId, flowSizeByte, logger):
    flow = FlowTable_pb2.Flow()
    flow.src = srcId
    flow.dst = dstId
    flow.time = time
    flow.flowSize = flowSizeByte

    # serialize
    serializeToString = flow.SerializeToString()

    # save data
    logger.write(serializeToString)
    logger.write(b"endl")
