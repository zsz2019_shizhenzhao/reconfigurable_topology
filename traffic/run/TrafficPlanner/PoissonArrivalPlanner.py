from traffic.run.FlowsizeDistribution.ParetoDistribution import ParetoDistribution
from utils.TrafficPlannerTemplate import TrafficPlanner
from math import log
import random


class PoissonArrivalPlanner(TrafficPlanner):
    _lambda = 0

    def __init__(self, propertyFile):
        TrafficPlanner.__init__(self, propertyFile)

        self._lambda = float(self._properties.loc['traffic_lambda_flow_starts_per_s', 'value'])
        self._pairDistribution = int(self._properties.loc['pair_probability', 'value'])

        # Set parameters for pareto 
        mean = float(self._properties.loc['traffic_flow_size_dist_pareto_mean_kilobytes', 'value'])
        shape = float(self._properties.loc['traffic_flow_size_dist_pareto_shape', 'value'])
        self._flowSizeDistribution = ParetoDistribution(shape=shape, mode=mean)

        # Set traffic pari probabilities
        if self._pairDistribution == 1:
            self.setPairProbAllToAll()

        if self._pairDistribution == 2:
            self.setPairProbAllToAllFraction()
            
    def choosePair(self):
        rng = random.random()
        return self.ceiling_item(rng)

    def createPlan(self, durationNs, logger):
        print("Creating arrival plan...")

        time, x = 0, 0

        while time <= durationNs:
            interArrivalTime = -log(random.random()) / (self._lambda / 1e9)

            # Register flow
            [src, dst] = self.choosePair()
            self.registerFlow(time, src, dst, self._podid[src], self._podid[dst], self._flowSizeDistribution.generateFlowSizeByte(), logger)

            # Advance time to next arrival
            time += interArrivalTime
            x += 1

        print("Done.")
        print("Poisson Arrival plan created.")
        print("Number of flows created: " + str(x) + ".\n")
