"""
    Traffic planner for constant rate Increasing active ToRs
"""
from traffic.run.FlowsizeDistribution.ParetoDistribution import ParetoDistribution
from utils.TrafficPlannerTemplate import TrafficPlanner
from math import log
import random


class ConstIncreasePlanner(TrafficPlanner):
    _lambda = 0
    _active_frac = .0
    _increrate = .0
    # TODO: 初始化一个乱序的ToR序号列表
    #       按初始比例顺序选取上表中的ToR设为Active
    #       按恒定比例增加active的ToR数量（顺序）
    #       PairCollection的内容需要根据Active ToR数量变化而变化
    #       随机数的生成范围也应与PairCollection长度匹配

    def __init__(self, propertyFile):
        TrafficPlanner.__init__(self, propertyFile)

        self._lambda = float(self._properties.loc['traffic_lambda_flow_starts_per_s', 'value'])
        self._pairDistribution = int(self._properties.loc['pair_probability', 'value'])

        # Set parameters for pareto 
        mean = float(self._properties.loc['traffic_flow_size_dist_pareto_mean_kilobytes', 'value'])
        shape = float(self._properties.loc['traffic_flow_size_dist_pareto_shape', 'value'])
        self._flowSizeDistribution = ParetoDistribution(shape=shape, mode=mean)

        # Set traffic pari probabilities
        # Change pair collection to a list here
        self._pairCollection = [] 
        if self._pairDistribution == 1:
            self.setPairProbAllToAll()

        if self._pairDistribution == 2:
            self.setPairProbAllToAllFraction()

        # Create shuffle ToR list
        self._shuffle_torlist = random.shuffle([i for i in range(self.torcount)])

        # Assign intial fration value
        self._active_frac = float(self._properties.loc['initial_fraction', 'value'])

        # Constant increasing rate
        self._increrate = float(self._properties.loc['increase_rate', 'value'])

    def setPairProbAllToAllFraction(self):
        print("Generating fractional pair probabilities between nodes...")
        fractionX = 0.19
        prob_idx = 0
        numofChosen = int(round(self._torcount * fractionX))

        pairProb = 1 / (numofChosen * (numofChosen-1))
        for src in range(numofChosen):
            for dst in range(numofChosen):
                if src != dst:
                    prob_idx += pairProb
                    src_tor, dst_tor = self._shuffle_torlist[src], self._shuffle_torlistd[dst]
                    self._pairCollection.append([[src_tor, dst_tor], prob_idx])

        print("done.")
        return 0

    def setPairProbAllToAll(self):
        print("Generating all-to-all pair probabilities between all nodes...")
        prob_idx = 0
        # Uniform probability for every server pair
        pdfNumBytes = 1.0 / (self._torcount * (self._torcount - 1))

        # Add uniform probability for every pair
        for src in range(self._torcount):
            for dst in range(self._torcount):
                if src != dst:
                    prob_idx += pdfNumBytes
                    src_tor, dst_tor = self._shuffle_torlist[src],self._shuffle_torlist[dst]
                    self._pairCollection.append([[src_tor, dst_tor], prob_idx])
        print("done.")

    def choosePair(self):
        rng = random.uniform(0, self._active_frac)
        return self.list_ceiling_item(rng)
    
    def list_ceiling_item(self, key):
        for i in range(len(self._pairCollection)):
            target = self._pairCollection[i][1]
            pair = self._pairCollection[i][0]
            if i == len(self._pairCollection) - 1:
                return pair
            if target <= key < self._pairCollection[i + 1][1]:
                return pair

    def createPlan(self, durationNs, logger):
        print("Creating arrival plan...")

        time, x = 0, 0

        tail = int(self._active_frac * self._torcount)

        while time <= durationNs:
            interArrivalTime = -log(random.random()) / (self._lambda / 1e9)
            
            if self._active_frac < 1:
                self._active_frac += self._increrate * interArrivalTime * 1e-9
                self._active_frac = min(self._active_frac, 1)

            # Register flow
            [src, dst] = self.choosePair()
            self.registerFlow(time, src, dst, self._podid[src], self._podid[dst], self._flowSizeDistribution.generateFlowSizeByte(), logger)

            # Advance time to next arrival
            time += interArrivalTime
            x += 1

        print("Done.")
        print("Poisson Arrival plan created.")
        print("Number of flows created: " + str(x) + ".\n")
