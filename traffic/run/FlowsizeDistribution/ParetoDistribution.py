"""
Pareto Distribution
"""
from numpy.random import pareto
import numpy as np
import matplotlib.pyplot as plt


class ParetoDistribution:
    __shape = 0
    __mode = 0

    def __init__(self, shape=1.05, mode=100.):
        self.__shape = shape
        self.__mode = mode

    def generateFlowSizeByte(self):
        return (np.random.default_rng().pareto(self.__shape) + 1) * self.__mode

    def set_para(self, shape, mode):
        self.__shape = shape
        self.__mode = mode
