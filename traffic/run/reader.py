from run.TrafficPlanner import FlowTable_pb2
import numpy as np

file = open("flowTable", 'rb')
flowTable = FlowTable_pb2.Flow()
data = file.read().rstrip(b"endl")
flowList = data.split(b"endl")
bins = []
trafficMatrix = np.zeros((100, 100), dtype=float)

for flow in flowList:
    flowTable.ParseFromString(flow)
    # print("Source: ", flowTable.src)
    # print("Destination: ", flowTable.dst)
    # print("Start Time: ", flowTable.time)
    # print("Flow Size: ", flowTable.flowSize)
    # print()

    bins.append(flowTable.flowSize)
    trafficMatrix[flowTable.src, flowTable.dst] += flowTable.flowSize

np.save("out2.npy", trafficMatrix)

# plt.hist(bins, bins=50, density=0, edgecolor="black")
# plt.matshow(trafficMatrix)
# plt.show()
